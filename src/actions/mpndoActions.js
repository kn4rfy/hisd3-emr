import * as Types from '../constants/ERActionTypes'
import {get, post, put, patch, _delete} from '../utils/RestClient'
import Promise from 'bluebird'

import * as Config from '../config/Config'
import _ from 'lodash'
import moment from 'moment';

export let getMPNDOByPMR = (pmrId) => {
  return dispatch => {
    Promise.coroutine(function *() {
      let erpmr = yield get(`${Config.getHost()}/restapi/erPmrs/${pmrId}`);
      let MPNDO = erpmr.data.mpnDoList;

      MPNDO.map((item, i)=>{
        if (_.get(item, 'progress_note')) {
          MPNDO[i].progress_note = JSON.parse(item.progress_note);
        }
        if (_.get(item, 'doctors_order')) {
          MPNDO[i].doctors_order = JSON.parse(item.doctors_order);
        }
      });

      return MPNDO || [];
    })().then((response) => {
      if(!_.isEmpty(response)){
        dispatch(updateMpndoRecord(response));
      }
      return response;
    }).catch((error) =>{
      if(__DEV__) {
        console.log('error getMPNDOByPMR', error)
      }
    })
  }
}

export let saveMPNDO = (data, resultfunc=()=>{}) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield post(Config.getHost() + '/restapi/mpnDoes', data);

      var mpnDoes = response.data;

      return mpnDoes;

    })().then((mpnDoes)=>{
      resultfunc(true, "Multi disciplinary progress notes and Doctors order successfully added");
      if(__DEV__) {
        console.log('success saveMPNDOrder', mpnDoes);
      }
      return mpnDoes;
    }).catch(function (error) {
      resultfunc(false, "Multi disciplinary progress notes and Doctors order failed to add");
      if(__DEV__) {
        console.log('error saveMPNDOrder', error);
      }
    });
  }
}

export let updateMPNDOInput = (data) => {
  return dispatch => {
    dispatch({ type: 'UPDATE_MPNDO_INPUT', data })
  }
}

export let updateMpndoRecord = (data) => {
  return {
    type: 'UPDATE_MPNDO',
    data
  }
}

export let saveNPN = (data, resultfunc=()=>{}) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield post(Config.getHost() + '/restapi/npns', data);

      var npns = response.data;

      return npns;

    })().then((npns)=>{
      resultfunc(true, "Nurse progress notes successfully added");
      if(__DEV__) {
        console.log('success saveNPN', npns);
      }
      return npns;
    }).catch(function (error) {
      resultfunc(false, "Nurse progress notes failed to add");
      if(__DEV__) {
        console.log('error saveNPN', error);
      }
    });
  }
}


export let updateNPNInput = (data) => {
  return dispatch => {
    dispatch({ type: 'UPDATE_NPN_INPUT', data })
  }
}
