/**
 * Created by albertoclarit on 10/7/16.
 */
import * as types from '../constants/HttpStatusActionTypes'

export let httpStatusLoading=()=>{

    return (dispatch,getState)=>{

        var {httpstatus} = getState();
        if(!httpstatus.isLoading)
            dispatch(httpStatusLoadingSuccess());

    };

};

export let httpStatusLoadingSuccess = ()=>{

    return {
        type: types.HTTPSTATUS_LOADING
    }
};
export let httpStatusIdle=()=>{

    return (dispatch,getState)=>{

        var {httpstatus} = getState();
        if(httpstatus.isLoading)
            dispatch(httpStatusIdleSuccess());

    };
};

export let httpStatusIdleSuccess = ()=>{
    return {
        type: types.HTTPSTATUS_IDLE
    }
};

