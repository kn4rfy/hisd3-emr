import * as Types from '../constants/ERActionTypes'
import {get, post, put, patch, _delete} from '../utils/RestClient'
import Promise from 'bluebird'

import * as Config from '../config/Config'

//=========== TRIAGE ACTION ============///

export let getTriageByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patient = response.data;
      try{
        var pdsc = yield get(Config.getHost() + `/restapi/pdscs/${patient.activePdsc.id}`);
        patient.triagelevel =  pdsc.data.triagelevel;
      }catch(e){}
      return  patient;
    })().then((patient)=>{
      dispatch(setTriage(patient.triagelevel));
      return patient;
    }).then((patient)=>{
      if (patient.triagelevel < 4) {
        dispatch(setActiveTab(1));
      } else {
        dispatch(setActiveTab(2));
      }
      return patient;
    }).catch((error)=>{
      dispatch(setTriage(null));
    });
  }
}

export let setTriage = (data) => {
  return {
    type: Types.SET_TRIAGE,
    data
  }
}

export let setActiveTab = (data) => {
  return {
    type: Types.SET_ACTIVE_TAB,
    data
  }
}

//=========== PMR ACTION ============///

export let updatePMR = (id, data, resultfunc=()=>{}) => {
  delete data.activeMpndoNew;

  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield patch(Config.getHost() + '/restapi/erPmrs/'+id, data);

      var pmr = response.data;
      try{
        var pmr = yield get(Config.getHost() + `/restapi/erPmrs/${id}`);
        pmr.pmrInfo =  pmr.data;
        if(pmr.pmrInfo.allergies != null && pmr.pmrInfo.allergies != 'null' && pmr.pmrInfo.allergies != "" && pmr.pmrInfo.allergies != {}){
          pmr.pmrInfo.allergies = JSON.parse(pmr.pmrInfo.allergies);
        } else {
          pmr.pmrInfo.allergies = {};
        }
      }catch(e){
        pmr.pmrInfo = 'clear';
        pmr.activeRecord = 'clear';
      }
      return pmr;

    })().then((pmr)=>{
      dispatch(updatePMRSuccess(pmr.pmrInfo));
      resultfunc(true, "Patient medical record successfully updated");
      if(__DEV__) {
        console.log('success updatePMR', pmr);
      }
      return pmr;
    }).catch(function (error) {
      resultfunc(false, "Patient medical record failed to update");
      if(__DEV__) {
        console.log('error updatePMR', error);
      }
    });
  }
}

export let getPMRByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var pmr = yield get(Config.getHost() + `/restapi/erPmrs/${patients.activePdsc.activePmr.id}`);
        patients.pmrInfo =  pmr.data;
        if(patients.pmrInfo.allergies != null && patients.pmrInfo.allergies != 'null' && patients.pmrInfo.allergies != "" && patients.pmrInfo.allergies != {}){
          patients.pmrInfo.allergies = JSON.parse(patients.pmrInfo.allergies);
        } else {
          patients.pmrInfo.allergies = {};
        }
      }catch(e){
        patients.pmrInfo = 'clear';
        patients.activeRecord = 'clear';
      }
      return patients;

    })().then((patients)=>{
      dispatch(loadPMRInfoSuccess(patients.pmrInfo));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPMRByPatientId', error);
      }
    });
  }
}

// export let addPMRSuccess = (data) => {
//     return {
//         type: Types.PMR_ADD_SUCCESS,
//         data
//     }
// }

export let updatePMRSuccess = (data) => {
  return {
    type: Types.PMR_UPDATE_SUCCESS,
    data
  }
}

export let updatePMRInput = (data) => {
  return {
    type: Types.PMR_UPDATE_INPUT,
    data
  }
}

export let updatePMRAllergyInput = (id, data) => {
  return {
    type: Types.PMR_ALLERGY_UPDATE_INPUT,
    id,
    data
  }
}

export let loadPMRInfoSuccess = (data) => {
  return {
    type: Types.PMR_LOAD_SUCCESS,
    data
  }
}

//=========== FOLIO ACTION ============///

export let updateFolio = (id, data) => {
  return dispatch => {

    patch(Config.getHost() + '/restapi/erFolios/'+id, data).then((response)=>{
      if(response.data)
      dispatch(updateFolioSuccess(response.data));

      if(__DEV__) {
        console.log('success updateFolio', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updateFolio', error);
      }
    });
  }
}

export let getFolioByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var folio = yield get(Config.getHost() + `/restapi/erFolios/${patients.activePdsc.activeFolio.id}`);
        patients.folio =  folio.data;
      }catch(e){}
      return  patients;

    })().then((patients)=>{
      dispatch(loadFolioInfoSuccess(patients.folio));
    }).catch((error)=>{
    });
  }
}

// export let addFolioSuccess = (data) => {
//     return {
//         type: Types.FOLIO_ADD_SUCCESS,
//         data
//     }
// }

export let updateFolioSuccess = (data) => {
  return {
    type: Types.FOLIO_UPDATE_SUCCESS,
    data
  }
}

export let updateFolioInput = (data) => {
  return {
    type: Types.FOLIO_UPDATE_INPUT,
    data
  }
}

export let loadFolioInfoSuccess = (data) => {
  return {
    type: Types.FOLIO_LOAD_SUCCESS,
    data
  }
}

//=========== Vital Signs ACTION ============///

export let addVitalSigns = (data, patientId, resultfunc=()=>{}) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield post(Config.getHost() + '/restapi/vitalSigns', data);

      var vitalSigns = response.data;

      return vitalSigns;

    })().then((vitalSigns)=>{
      dispatch(getVitalSignsByPatientId(patientId));
      resultfunc(true, "Vital signs successfully added");
      if(__DEV__) {
        console.log('success addVitalSigns', vitalSigns);
      }
      return vitalSigns;
    }).catch(function (error) {
      resultfunc(false, "Vital signs failed to add");
      if(__DEV__) {
        console.log('error addVitalSigns', error);
      }
    });
  }
}

export let updateVitalSigns = (id, data, patientId, resultfunc=()=>{}) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield patch(Config.getHost() + '/restapi/vitalSigns/'+id, data);

      var vitalSigns = response.data;

      return  vitalSigns;

    })().then((vitalSigns)=>{
      dispatch(getVitalSignsByPatientId(patientId));
      resultfunc(true, "Vital signs successfully updated");
      if(__DEV__) {
        console.log('success updateVitalSigns', vitalSigns);
      }
      return vitalSigns;
    }).catch(function (error) {
      resultfunc(false, "Vital signs failed to update");
      if(__DEV__) {
        console.log('error updateVitalSigns', error);
      }
    });
  }
}

export let getVitalSignsByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var vitalSigns = yield get(Config.getHost() + `/restapi/vitalSigns/search/findByErpmrId?id=${patients.activePdsc.activePmr.id}&sort=createdDate,asc`);

        Object.keys(vitalSigns.data._embedded.vitalSigns).map((item, i)=>{
          if(typeof vitalSigns.data._embedded.vitalSigns[item].records == 'string'){
            vitalSigns.data._embedded.vitalSigns[item].records = JSON.parse(vitalSigns.data._embedded.vitalSigns[item].records)
          }
        });
        patients.vitalSignsRecords =  vitalSigns.data;
        patients.vitalSignsActiveRecord = {}
      }catch(e){
        patients.vitalSignsRecords = 'clear';
        patients.vitalSignsActiveRecord = 'clear';
      }
      return  patients;

    })().then((patients)=>{
      dispatch(loadVitalSignsInfoSuccess(patients.vitalSignsRecords, patients.vitalSignsActiveRecord));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getVitalSignsByPatientId', error);
      }
    });
  }
}

export let addVitalSignsSuccess = (data) => {
  return {
    type: Types.VITAL_SIGNS_ADD_SUCCESS,
    data
  }
}

export let updateVitalSignsSuccess = (data) => {
  return {
    type: Types.VITAL_SIGNS_UPDATE_SUCCESS,
    data
  }
}

export let updateVitalSignsInput = (id, data) => {
  return {
    type: Types.VITAL_SIGNS_UPDATE_INPUT,
    id,
    data
  }
}

export let loadVitalSignsInfoSuccess = (records, activeRecord) => {
  return {
    type: Types.VITAL_SIGNS_LOAD_SUCCESS,
    records,
    activeRecord
  }
}

//=========== Past Medical History ACTION ============///

export let addPastMedicalHistory = (data, patientId) => {
  return dispatch => {

    post(Config.getHost() + '/restapi/pastMedicalHistories', data).then((response)=>{
      if(response.data)
      dispatch(getPastMedicalHistoryByPatientId(patientId));

      if(__DEV__) {
        console.log('success addPastMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error addPastMedicalHistory', error);
      }
    });
  }
}

export let updatePastMedicalHistory = (id, data) => {
  return dispatch => {

    patch(Config.getHost() + '/restapi/pastMedicalHistories/'+id, data).then((response)=>{
      if(response.data)
      dispatch(updatePastMedicalHistorySuccess(response.data));

      if(__DEV__) {
        console.log('success updatePastMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updatePastMedicalHistory', error);
      }
    });
  }
}

export let getPastMedicalHistoryByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var pastMedicalHistories = yield get(Config.getHost() + `/restapi/erPmrs/${patients.activePdsc.activePmr.id}/pastMedicalHistories`);
        patients.pastMedicalHistoriesRecords = pastMedicalHistories.data
        Object.keys(pastMedicalHistories.data._embedded.pastMedicalHistories).map((item, i)=>{
          patients.pastMedicalHistoriesActiveRecord = pastMedicalHistories.data._embedded.pastMedicalHistories[item];
        });
        patients.pastMedicalHistoriesActiveRecord.records = JSON.parse(patients.pastMedicalHistoriesActiveRecord.records);
      }catch(e){
        patients.pastMedicalHistoriesRecords = 'clear';
        patients.pastMedicalHistoriesActiveRecord = 'clear';
      }
      return  patients;

    })().then((patients)=>{
      dispatch(loadPastMedicalHistoryInfoSuccess(patients.pastMedicalHistoriesRecords, patients.pastMedicalHistoriesActiveRecord));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPastMedicalHistoryByPatientId', error);
      }
    });
  }
}

export let deletePastMedicalHistory = (id, patientId)=> {
  return (dispatch)=> {

    _delete(Config.getHost() + "/restapi/pastMedicalHistories/" + id).then((response)=> {
      if(response.data)
      dispatch(getPastMedicalHistoryByPatientId(patientId));

      if(__DEV__) {
        console.log('success deletePastMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deletePastMedicalHistory', error);
      }
    });
  }
};

export let addPastMedicalHistorySuccess = (data) => {
  return {
    type: Types.PAST_MEDICAL_HISTORY_ADD_SUCCESS,
    data
  }
}

export let updatePastMedicalHistorySuccess = (data) => {
  return {
    type: Types.PAST_MEDICAL_HISTORY_UPDATE_SUCCESS,
    data
  }
}

export let updatePastMedicalHistoryInput = (data) => {
  return {
    type: Types.PAST_MEDICAL_HISTORY_UPDATE_INPUT,
    data
  }
}

export let loadPastMedicalHistoryInfoSuccess = (records, activeRecord) => {
  return {
    type: Types.PAST_MEDICAL_HISTORY_LOAD_SUCCESS,
    records,
    activeRecord
  }
}

//=========== Present Medical History ACTION ============///

export let addPresentMedicalHistory = (data, patientId) => {
  return dispatch => {

    post(Config.getHost() + '/restapi/presentMedicalHistories', data).then((response)=>{
      if(response.data)
      dispatch(getPresentMedicalHistoryByPatientId(patientId));

      if(__DEV__) {
        console.log('success addPresentMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error addPresentMedicalHistory', error);
      }
    });
  }
}

export let updatePresentMedicalHistory = (id, data) => {
  return dispatch => {

    patch(Config.getHost() + '/restapi/presentMedicalHistories/'+id, data).then((response)=>{
      if(response.data)
      dispatch(updatePresentMedicalHistorySuccess(response.data));

      if(__DEV__) {
        console.log('success updatePresentMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updatePresentMedicalHistory', error);
      }
    });
  }
}

export let getPresentMedicalHistoryByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var presentMedicalHistories = yield get(Config.getHost() + `/restapi/erPmrs/${patients.activePdsc.activePmr.id}/presentMedicalHistories`);
        patients.presentMedicalHistoriesRecords = presentMedicalHistories.data
        Object.keys(presentMedicalHistories.data._embedded.presentMedicalHistories).map((item, i)=>{
          patients.presentMedicalHistoriesActiveRecord = presentMedicalHistories.data._embedded.presentMedicalHistories[item];
        });
        patients.presentMedicalHistoriesActiveRecord.records = JSON.parse(patients.presentMedicalHistoriesActiveRecord.records);
      }catch(e){
        patients.presentMedicalHistoriesRecords = 'clear';
        patients.presentMedicalHistoriesActiveRecord = 'clear';
      }
      return  patients;

    })().then((patients)=>{
      dispatch(loadPresentMedicalHistoryInfoSuccess(patients.presentMedicalHistoriesRecords, patients.presentMedicalHistoriesActiveRecord));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPresentMedicalHistoryByPatientId', error);
      }
    });
  }
}

export let deletePresentMedicalHistory = (id, patientId)=> {
  return (dispatch)=> {

    _delete(Config.getHost() + "/restapi/pastMedicalHistories/" + id).then((response)=> {
      if(response.data)
      dispatch(getPresentMedicalHistoryByPatientId(patientId));

      if(__DEV__) {
        console.log('success deletePresentMedicalHistory', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deletePresentMedicalHistory', error);
      }
    });
  }
};

export let addPresentMedicalHistorySuccess = (data) => {
  return {
    type: Types.PRESENT_MEDICAL_HISTORY_ADD_SUCCESS,
    data
  }
}

export let updatePresentMedicalHistorySuccess = (data) => {
  return {
    type: Types.PRESENT_MEDICAL_HISTORY_UPDATE_SUCCESS,
    data
  }
}

export let updatePresentMedicalHistoryInput = (data) => {
  return {
    type: Types.PRESENT_MEDICAL_HISTORY_UPDATE_INPUT,
    data
  }
}

export let loadPresentMedicalHistoryInfoSuccess = (records, activeRecord) => {
  return {
    type: Types.PRESENT_MEDICAL_HISTORY_LOAD_SUCCESS,
    records,
    activeRecord
  }
}

//=========== REVIEW SYSTEM ACTION ============///

export let addReviewSystem = (data, patientId) => {
  return dispatch => {

    post(Config.getHost() + '/restapi/reviewSystems', data).then((response)=>{
      if(response.data)
      dispatch(getReviewSystemByPatientId(patientId));

      if(__DEV__) {
        console.log('success addReviewSystem', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error addReviewSystem', error);
      }
    });
  }
}

export let updateReviewSystem = (id, data) => {
  return dispatch => {

    patch(Config.getHost() + '/restapi/reviewSystems/'+id, data).then((response)=>{
      if(response.data)
      dispatch(updateReviewSystemSuccess(response.data));

      if(__DEV__) {
        console.log('success updateReviewSystem', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updateReviewSystem', error);
      }
    });
  }
}

export let getReviewSystemByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var reviewSystems = yield get(Config.getHost() + `/restapi/erPmrs/${patients.activePdsc.activePmr.id}/reviewSystems`);
        patients.reviewSystemsRecords = reviewSystems.data
        Object.keys(reviewSystems.data._embedded.reviewSystems).map((item, i)=>{
          patients.reviewSystemsActiveRecord = reviewSystems.data._embedded.reviewSystems[item];
        });
        patients.reviewSystemsActiveRecord.records = JSON.parse(patients.reviewSystemsActiveRecord.records);
      }catch(e){
        patients.reviewSystemsRecords = 'clear';
        patients.reviewSystemsActiveRecord = 'clear';
      }
      return  patients;

    })().then((patients)=>{
      dispatch(loadReviewSystemInfoSuccess(patients.reviewSystemsRecords, patients.reviewSystemsActiveRecord));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getReviewSystemByPatientId', error);
      }
    });
  }
}

export let deleteReviewSystem = (id, patientId)=> {
  return (dispatch)=> {

    _delete(Config.getHost() + "/restapi/reviewSystems/" + id).then((response)=> {
      if(response.data)
      dispatch(getReviewSystemByPatientId(patientId));

      if(__DEV__) {
        console.log('success deleteReviewSystem', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deleteReviewSystem', error);
      }
    });
  }
};

export let addReviewSystemSuccess = (data) => {
  return {
    type: Types.REVIEW_SYSTEM_ADD_SUCCESS,
    data
  }
}

export let updateReviewSystemSuccess = (data) => {
  return {
    type: Types.REVIEW_SYSTEM_UPDATE_SUCCESS,
    data
  }
}

export let updateReviewSystemInput = (id, data) => {
  return {
    type: Types.REVIEW_SYSTEM_UPDATE_INPUT,
    id,
    data
  }
}

export let loadReviewSystemInfoSuccess = (records, activeRecord) => {
  return {
    type: Types.REVIEW_SYSTEM_LOAD_SUCCESS,
    records,
    activeRecord
  }
}

//=========== PHYSICAL EXAMINATION ACTION ============///

export let addPhysicalExamination = (data, patientId) => {
  return dispatch => {

    post(Config.getHost() + '/restapi/physicalExaminations', data).then((response)=>{
      if(response.data)
      dispatch(getPhysicalExaminationByPatientId(patientId));

      if(__DEV__) {
        console.log('success addPhysicalExamination', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error addPhysicalExamination', error);
      }
    });
  }
}

export let updatePhysicalExamination = (id, data) => {
  return dispatch => {

    patch(Config.getHost() + '/restapi/physicalExaminations/'+id, data).then((response)=>{
      if(response.data)
      dispatch(updatePhysicalExaminationSuccess(response.data));

      if(__DEV__) {
        console.log('success updatePhysicalExamination', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updatePhysicalExamination', error);
      }
    });
  }
}

export let getPhysicalExaminationByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + "/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var physicalExaminations = yield get(Config.getHost() + `/restapi/erPmrs/${patients.activePdsc.activePmr.id}/physicalExaminations`);
        patients.physicalExaminationsRecords = physicalExaminations.data
        Object.keys(physicalExaminations.data._embedded.physicalExaminations).map((item, i)=>{
          patients.physicalExaminationsActiveRecord = physicalExaminations.data._embedded.physicalExaminations[item];
        });
        patients.physicalExaminationsActiveRecord.records = JSON.parse(patients.physicalExaminationsActiveRecord.records);
      }catch(e){
        patients.physicalExaminationsRecords = 'clear';
        patients.physicalExaminationsActiveRecord = 'clear';
      }
      return  patients;

    })().then((patients)=>{
      dispatch(loadPhysicalExaminationInfoSuccess(patients.physicalExaminationsRecords, patients.physicalExaminationsActiveRecord));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPhysicalExaminationByPatientId', error);
      }
    });
  }
}

export let deletePhysicalExamination = (id, patientId)=> {
  return (dispatch)=> {
    _delete(Config.getHost() + "/restapi/physicalExaminations/" + id).then((response)=> {
      if(response.data)
      dispatch(getPhysicalExaminationByPatientId(patientId));

      if(__DEV__) {
        console.log('success deletePhysicalExamination', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deletePhysicalExamination', error);
      }
    });
  }
};

export let addPhysicalExaminationSuccess = (data) => {
  return {
    type: Types.PHYSICAL_EXAMINATION_ADD_SUCCESS,
    data
  }
}

export let updatePhysicalExaminationSuccess = (data) => {
  return {
    type: Types.PHYSICAL_EXAMINATION_UPDATE_SUCCESS,
    data
  }
}

export let updatePhysicalExaminationInput = (id, data) => {
  return {
    type: Types.PHYSICAL_EXAMINATION_UPDATE_INPUT,
    id,
    data
  }
}

export let loadPhysicalExaminationInfoSuccess = (records, activeRecord) => {
  return {
    type: Types.PHYSICAL_EXAMINATION_LOAD_SUCCESS,
    records,
    activeRecord
  }
}

//=========== IAP ACTION AND IAP DETIALS ACTION ============///

export let getIapList = (id)=> {

  return (dispatch)=> {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() + id);
      var iaps = response.data;
      return  iaps;

    })().then((iaps)=>{
      if (iaps.activePdsc) {
        if (iaps.activePdsc.iaps.length != 0) {
          dispatch(LoadDetailSuccess(iaps.activePdsc.iaps[0]));
          if (iaps.activePdsc.iaps[0].iapDetails) {
            dispatch(SetIapDetailSuccess(iaps.activePdsc.iaps[0].iapDetails));

          }
        }else{
          dispatch(ResetIapSuccess(id));
          dispatch(SetIapDetailSuccess('clear'));
        }
      }else{
        // if activePdsc is null
      }

      dispatch(LoadPatientIapSuccess(iaps));
    }).catch((error)=>{
    });
  }
};

export let upsert = (Iap,IapDetails,callback) =>{
  return dispatch=>{

    var patientID = Iap.patient;

    if (Iap.id) {
      Iap.oldStatus = IapDetails;
      if (Iap.oldStatus == 'PENDING') {
        Iap.status = 'STORED';
      }else{
        Iap.status = 'CLAIMED';
      }

      Promise.coroutine(function *(){
        var response = yield  patch(Config.getHost() + "/restapi/iaps/" + Iap.id, Iap);
        var iap = response.data;

        return iap;

      })().then((iap)=>{
        dispatch(getIapList(patientID));
        dispatch(callback);
        return iap;
      }).catch((error)=>{
      });

    }else{
      post(Config.getHost() + '/restapi/iaps',Iap).then(response => {

        if(response.data){

          var iapId = '/restapi/iaps/'+response.data.id;
          var IapDataArray = {description:IapDetails.description,iap:iapId,quantity_amount:IapDetails.quantity_amount,condition:IapDetails.condition};

          post(Config.getHost() + '/restapi/iapDetails',IapDataArray).then(response => {
            if(response.data){
              dispatch(getIapList(patientID));
              dispatch(callback)

              return null;
            }
          }).catch(function(error){
          });

          dispatch(updateIap(response.data));
        }

        return null;

      }).catch(function(error){
        if(__DEV__) {
          console.log('error upsert', error);
        }
      });
    }
  }
}
export let upsertIapDetails = (IapDataDetails,patientID,callback) => {
  return dispatch => {

    if (IapDataDetails.id) {
      delete IapDataDetails.iap;
      Promise.coroutine(function *(){
        var response = yield  patch(Config.getHost() + "/restapi/iapDetails/" + IapDataDetails.id, IapDataDetails);
        var Iapdetails = response.data;

        return Iapdetails;


      })().then((Iapdetails)=>{
        dispatch(getIapList(patientID));
        dispatch(callback);

        return null;
      }).catch((error)=>{
      });

    }else{

      IapDataDetails.iap = '/restapi/iaps/'+IapDataDetails.iap;
      post(Config.getHost() + '/restapi/iapDetails',IapDataDetails).then(response => {

        if(response.data)
        dispatch(callback);

        var arrayList = [response.data];
        dispatch(LoadIapDetailSuccess(arrayList));
      }).catch(function(error){
        if(__DEV__) {
          console.log('error upsertIapDetails', error);
        }
      });
    }
  }
}

export let getIapDetials = (IapDetailsID) =>{
  return dispatch =>{
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost() +"/restapi/iapDetails/" + IapDetailsID);
      var iapsDetails = response.data;
      return  iapsDetails;

    })().then((iapsDetails)=>{
      dispatch(getIapDetailsActive(iapsDetails));
    }).catch((error)=>{
    });
  }
}

export let deleteIapDetails = (id,patientID)=> {
  return (dispatch)=> {
    _delete(Config.getHost() +"/restapi/iapDetails/" + id).
    then((response)=> {

      dispatch(getIapList(patientID));

      return null;

    }).catch((error)=> {
    });
  }
};

export let getIapListSuccess = (records)=> {
  return {
    type: types.IAP_LOAD,
    records,
    page
  }
};

export let updateIap = (data)=> {
  for(var prop in data ){
    if (typeof data[prop]== 'string' && prop.toLowerCase().indexOf('signature') == -1) {
      data[prop] = data[prop].toUpperCase().trimLeft();

    }
  }
  return {
    type: Types.IAP_UPDATE,
    data
  };
};

export let updateIapDetails = (data)=> {
  return {
    type: Types.UPDATE_IAP_DETAILS,
    data
  };
};

export let getIapDetailsActive = (data,PatientId)=> {
  return {
    type: Types.GET_ACTIVE_IAP_DETAILS,
    data,
    PatientId
  };
};

export let AddIapDetailsSuccess = (data) => {
  return {
    type: Types.ADD_IAP_DETAILS,
    data
  }
};

export let LoadDetailSuccess = (data) => {
  return {
    type: Types.IAP_LOAD,
    data
  }
};

export let LoadPatientIapSuccess = (data) => {
  return {
    type: Types.IAP_LOAD_PATIENT,
    data
  }
};

export let ResetIapSuccess = (data) => {
  return {
    type: Types.IAP_RESET,
    data
  }
};

export let LoadIapDetailSuccess = (data)=> {
  return{
    type: Types.LOAD_IAP_DETAILS,
    data
  }
};

export let SetIapDetailSuccess = (data)=> {
  return{
    type: Types.SET_IAP_DETAILS,
    data
  }
};

export let SaveIapDetails = (data)=> {
  return{
    type: Types.SAVE_IAP_DETAILS,
    data
  }
};

export let RemoveKeyIapDetails = (key)=> {
  return{
    type: Types.REMOVE_KEY_IAP_DETAILS,
    selected:key
  }
};
