import * as pdscTypes from '../constants/PDSCActionTypes'
import {get, post, put, patch, _delete} from '../utils/RestClient'
import Promise from 'bluebird'
import * as Config from '../config/Config'
import _ from 'lodash';

import {loadPatientInfoSuccess, getPatientList} from '../actions/patientactions'


export let getPDSCList = (page, filter, patientId, size) => {
  return dispatch => {
    get(Config.getHost()+'/restapi/pdscs/search/findByFilter',{
      params: {
        page: page - 1,
        filter,
        patientId,
        size
      }
    }).then((response)=>{
      if(response.data._embedded && response.data._embedded.pdscs)
      dispatch(loadPDSCListSuccess(response.data));
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error getPDSCList', error);
      }
    });
  }
}

export let getPDSCInfo = (pdscId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(`${Config.getHost()}/restapi/pdscs/${pdscId}`);

      var pdsc = response.data;

      try{
        pdsc.erConsultant = `${Config.getHost()}/restapi/employees/${response.data.erConsultantData.id}`;
      }catch(e){}

      try{
        pdsc.nurse = `${Config.getHost()}/restapi/employees/${response.data.nurseData.id}`;
      }catch(e){}

      try{
        pdsc.armoOnDuty = `${Config.getHost()}/restapi/employees/${response.data.armoOnDutyData.id}`;
      }catch(e){}

      try{
        pdsc.billingDeptHead = `${Config.getHost()}/restapi/employees/${response.data.billingDeptHeadData.id}`;
      }catch(e){}

      try{
        var refResponseConsents = yield get(pdsc._links.pdscConsentRecords.href);
        pdsc._consents = refResponseConsents.data._links.self.href;
        var refToEntityResponseConsents = yield get(refResponseConsents.data._links.self.href);
        pdsc.consents = refToEntityResponseConsents.data._embedded.pdscConsentRecords;
      }catch(e){}

      return  pdsc;

      })().then((pdsc)=>{
        dispatch(loadPDSCInfoSuccess(pdsc));
        dispatch(updatePDSCInput(pdsc));

        return null;
      }).catch((error)=>{
        if(__DEV__) {
          console.log('error getPDSCInfo', error);
        }
    });
  }
}

export let getPatientAndPDSCInfo = (patientId, pdscId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield get(Config.getHost()+"/restapi/patients/" + patientId);
      var allData = {};
      var patient = response.data;
      allData.patient = patient;
      var currPdscId = false;
      if(pdscId){
        currPdscId = pdscId;
      }else{
        if(patient.activePdsc){
          currPdscId = patient.activePdsc.id;
        }
      }

      try{
        var refResponse = yield get(patient._links.PatientInsuranceList.href);
        allData.patient._insurances = refResponse.data._links.self.href;
        var refToEntityResponse = yield get(refResponse.data._links.self.href);
        allData.patient.insurances = refToEntityResponse.data._embedded.patientInsuranceLists;
      }catch(e){}

      try{
        if(currPdscId){
          var pdsc = yield get(`${Config.getHost()}/restapi/pdscs/${currPdscId}`);
          allData.pdsc =  pdsc.data;
          try{
            allData.pdsc.erConsultant = `${Config.getHost()}/restapi/employees/${pdsc.data.erConsultantData.id}`;
          }catch(e){}

          try{
            allData.pdsc.nurse = `${Config.getHost()}/restapi/employees/${pdsc.data.nurseData.id}`;
          }catch(e){}

          try{
            allData.pdsc.armoOnDuty = `${Config.getHost()}/restapi/employees/${pdsc.data.armoOnDutyData.id}`;
          }catch(e){}

          try{
            allData.pdsc.billingDeptHead = `${Config.getHost()}/restapi/employees/${pdsc.data.billingDeptHeadData.id}`;
          }catch(e){}

          // for consent
          var refResponseConsents = yield get(allData.pdsc._links.pdscConsentRecords.href);
          allData.pdsc._consents = refResponseConsents.data._links.self.href;
          var refToEntityResponseConsents = yield get(refResponseConsents.data._links.self.href);
          allData.pdsc.consents = refToEntityResponseConsents.data._embedded.pdscConsentRecords;
        }
      }catch(e){}

      try{
        if(currPdscId){
          var pdscInsurances = yield get(`${Config.getHost()}/restapi/pdscInsurances/search/findByPdscId?pdscId=${currPdscId}`);
          allData.pdsc.insurances =  pdscInsurances.data._embedded.pdscInsurances;
          var pdscAttendingPhysicians = yield get(`${Config.getHost()}/restapi/pdscAttendingPhysicians/search/findByPdscId?pdscId=${currPdscId}`);
          allData.pdsc.attendingPhysicians =  pdscAttendingPhysicians.data._embedded.pdscAttendingPhysicians;
          var pdscErDoctors = yield get(`${Config.getHost()}/restapi/pdscErDoctors/search/findByPdscId?pdscId=${currPdscId}`);
          allData.pdsc.erDoctors =  pdscErDoctors.data._embedded.pdscErDoctors;
        }
      }catch(e){}

      return  allData;
    })().then((allData)=>{
      dispatch(loadPatientInfoSuccess(allData.patient));
      if(allData.pdsc) {
        dispatch(loadPDSCData(allData.pdsc));
      }
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPatientAndPDSCInfo', error);
      }
    });
  }
}

export let getPDSCInfoByPatientId = (patientId) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(Config.getHost()+"/restapi/patients/" + patientId);

      var patients = response.data;
      try{
        var pdsc = yield get(`${Config.getHost()}/restapi/pdscs/${patients.activePdsc.id}`);
        patients.pdsc =  pdsc.data;

        try{
          pdsc.erConsultant = `${Config.getHost()}/restapi/employees/${response.data.erConsultantData.id}`;
        }catch(e){}

        try{
          pdsc.nurse = `${Config.getHost()}/restapi/employees/${response.data.nurseData.id}`;
        }catch(e){}

        try{
          pdsc.armoOnDuty = `${Config.getHost()}/restapi/employees/${response.data.armoOnDutyData.id}`;
        }catch(e){}

        try{
          pdsc.billingDeptHead = `${Config.getHost()}/restapi/employees/${response.data.billingDeptHeadData.id}`;
        }catch(e){}

        var refResponseConsents = yield get(patients.pdsc._links.pdscConsentRecords.href);
        patients.pdsc._consents = refResponseConsents.data._links.self.href;
        var refToEntityResponseConsents = yield get(refResponseConsents.data._links.self.href);
        patients.pdsc.consents = refToEntityResponseConsents.data._embedded.pdscConsentRecords;

      }catch(e){}
      return  patients;

    })().then((patients)=>{
      dispatch(updatePDSCInput(patients.pdsc));
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getPDSCInfoByPatientId', error);
      }
    });
  }
}

export let dischargePdscById = (pdscId, callback=null) => {
  return dispatch => {
    post(Config.getHost()+"/restapi/pdsc/discharge/"+pdscId).then((response)=>{
      if(callback != null) dispatch(callback);
      return null;
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error dischargePdscById', error);
      }
    });
  }
}

export let unDischargePdscById = (pdscId, callback=null) => {
  return dispatch => {
    post(Config.getHost()+"/restapi/pdsc/undischarge/"+pdscId).then((response)=>{
      if(callback != null) dispatch(callback);
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error unDischargePdscById', error);
      }
    });
  }
}

export let upsertPDSC = (pdscData, callback) => {
  return (dispatch, getState) => {
    _.map(pdscData.insuranceUpdate.toDelete, (pdscInsuranceId) => {
      dispatch(deletePdscInsurance(physicianId));
    });

    _.map(pdscData.attendingPhysicianUpdate.toDelete, (physicianId) => {
      dispatch(deletePdscAttendingPhysician(physicianId));
    });

    _.map(pdscData.erDoctorUpdate.toDelete, (physicianId) => {
      dispatch(deletePdscErDoctor(physicianId));
    });

    if(pdscData.consentsToDelete){
      pdscData.consentsToDelete.map((consentData) => {
        dispatch(deleteConsent(consentData));
      });
    }

    if(!pdscData.id){
      dispatch(addPDSC(pdscData, callback));
    }else{
      dispatch(updatePDSC(pdscData, callback));
    }
  }
}

export let addPDSC = (pdscDataOrig, callback=null) => {
  return dispatch => {
    let pdscData = _.clone(pdscDataOrig);
    delete pdscData.attendingPhysicians;
    post(Config.getHost()+'/restapi/pdscs', pdscData).then((response)=>{
      if(response.data/*._embedded && response.data._embedded.pdscs*/){
        _.map(pdscData.insuranceUpdate.toAdd, (patientInsuranceId) => {
          dispatch(addPdscInsurance(response.data.id, patientInsuranceId));
        });

        _.map(pdscData.attendingPhysicianUpdate.toAdd, (physicianId) => {
          dispatch(addPdscAttendingPhysician(response.data.id, physicianId));
        });

        _.map(pdscData.erDoctorUpdate.signatureData, (pedData, doctorId) => {
          dispatch(upsertPdscErDoctor(response.data.id, doctorId, pedData));
        })

        _.map(pdscData.consents, (consentData) => {
          consentData.pdsc = `${Config.getHost()}/restapi/pdscs/${response.data.id}`;
          dispatch(upsertConsent(consentData));
        });

        _.merge(response.data, pdscDataOrig);
        dispatch(loadPDSCData(response.data));

        if(callback != null) dispatch(callback);
        return null;
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error addPDSC', error);
      }
    });
  }
}

export let generatePMR = (pdscId, pdsc) => {
  return dispatch => {
    post(Config.getHost()+'/restapi/erPmrs', {pdsc: pdsc}).then((response)=>{
      if(response.data){
        dispatch(updatePDSC({id: pdscId, erPmr: response.data._links.self.href}, generateFolio(pdscId, pdsc)));
      }
    }).catch(function (error) {
    });
  }
}

export let addPdscInsurance = (pdscId, patientInsuranceId) =>{
  return dispatch => {
    post(Config.getHost()+'/restapi/pdscInsurances', {
      pdsc: `${Config.getHost()}/restapi/pdscs/${pdscId}`,
      patientInsurance: `${Config.getHost()}/restapi/patientInsuranceLists/${patientInsuranceId}`
    }).then((response) => {

    }).catch(function (error) {
    });
  }
}

export let addPdscAttendingPhysician = (pdscId, employeeId) =>{
  return dispatch => {
    post(Config.getHost()+'/restapi/pdscAttendingPhysicians', {
      pdsc: `${Config.getHost()}/restapi/pdscs/${pdscId}`,
      doctor: `${Config.getHost()}/restapi/employees/${employeeId}`
    }).then((response) => {

    }).catch(function (error) {
    });
  }
}

export let addPdscErDoctor = (pdscId, employeeId) =>{
  return dispatch => {
    post('/restapi/pdscErDoctors', {
      pdsc: `/restapi/pdscs/${pdscId}`,
      doctor: `/restapi/employees/${employeeId}`
    }).then((response) => {

    }).catch(function (error) {
    });
  }
}

export let upsertPdscErDoctor = (pdscId, doctorId, pedData) =>{
  return dispatch => {
    if(pedData.signature != 'skip'){
      if (pedData.recordId) {
        patch(Config.getHost()+'/restapi/pdscErDoctors/'+pedData.recordId,{
          pdsc: `/restapi/pdscs/${pdscId}`,
          doctor: `/restapi/employees/${doctorId}`,
          signature: pedData.signature
        }).then(response => {
          // dispatch(callback);
        }).catch(function (error) {
          if(__DEV__) {
            console.log('error upsertPdscErDoctor patch', error);
          }
        });

      }else{
        post(Config.getHost()+'/restapi/pdscErDoctors',{
          pdsc: `/restapi/pdscs/${pdscId}`,
          doctor: `/restapi/employees/${doctorId}`,
          signature: pedData.signature
        }).then(response => {
          // dispatch(callback);
        }).catch(function (error) {
          if(__DEV__) {
            console.log('error upsertPdscErDoctor post', error);
          }
        });
      }
    }
  }
}

export let deletePdscInsurance = (pdscInsuranceId) => {
  return dispatch => {
    _delete(Config.getHost()+'/restapi/pdscInsurances/'+pdscInsuranceId).then((response) => {
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deletePdscInsurance', error);
      }
    })
  }
}

export let deletePdscAttendingPhysician = (attendingPhysicianId) => {
  return dispatch => {
    _delete(Config.getHost()+'/restapi/pdscAttendingPhysicians/'+attendingPhysicianId).then((response) => {
    }).catch(function(error){
      if(__DEV__) {
        console.log('error deletePdscAttendingPhysician', error);
      }
    })
  }
}

export let deletePdscErDoctor = (erDoctorId) => {
  return dispatch => {
    _delete(Config.getHost()+'/restapi/pdscErDoctors/'+erDoctorId).then((response) => {
    }).catch(function(error){
      if(__DEV__) {
        console.log('error deletePdscAttendingPhysician', error);
      }
    })
  }
}

export let generateFolio = (pdscId, pdsc) => {
  return dispatch => {
    post(Config.getHost()+'/restapi/erFolios', {pdsc: pdsc}).then((response)=>{
      if(response.data){
        dispatch(updatePDSC({id: pdscId, erFolio: response.data._links.self.href}, ()=>{}));
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error generateFolio', error);
      }
    });
  }
}

export let updatePDSC = (pdscDataOrig, callback=null) => {
  return dispatch => {
    let pdscData = _.clone(pdscDataOrig);
    delete pdscData.activePmr;
    delete pdscData.activeFolio;
    delete pdscData.admissionData;
    delete pdscData.dischargeNoticeData;
    delete pdscData.erConsultantData;
    delete pdscData.nurseData;
    delete pdscData.armoOnDutyData;
    delete pdscData.billingDeptHeadData;
    delete pdscData.attendingPhysicians;
    delete pdscData.erDoctors;

    _.map(pdscData.insuranceUpdate.toAdd, (patientInsuranceId) => {
      dispatch(addPdscInsurance(pdscData.id, patientInsuranceId));
    });

    _.map(pdscData.attendingPhysicianUpdate.toAdd, (physicianId) => {
      dispatch(addPdscAttendingPhysician(pdscData.id, physicianId));
    })

    _.map(pdscData.erDoctorUpdate.signatureData, (pedData, doctorId) => {
      dispatch(upsertPdscErDoctor(pdscData.id, doctorId, pedData));
    })

    _.map(pdscData.consents, (consentData) => {
      consentData.pdsc = `${Config.getHost()}/restapi/pdscs/${pdscData.id}`;
      dispatch(upsertConsent(consentData));
    });

    patch(Config.getHost()+'/restapi/pdscs/'+pdscData.id, pdscData).then((response)=>{
      if(response.data/*._embedded && response.data._embedded.pdscs*/){
        _.merge(response.data, pdscDataOrig);
        dispatch(loadPDSCData(response.data));
        if(callback != null) dispatch(callback);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updatePDSC', error);
      }
    });
  }
}

export let deletePDSC = (pdscId, callback=null) => {
  return dispatch => {
    _delete(`${Config.getHost()}/restapi/pdscs/${pdscId}`).then((response) => {
      dispatch(updatePDSCInput('clear'));

      if(callback != null) dispatch(callback);
      return null;
    }).catch(function (error){
      if(__DEV__) {
        console.log('error deletePDSC', error);
      }
    });
  }
}

export let updatePDSCInput = (data)=> {
  if(data != 'clear'){
    for(var prop in data){
      if(typeof data[prop] == 'string' && data[prop].indexOf('restapi/') == -1 && prop.toLowerCase().indexOf('signature') == -1){
        data[prop] = data[prop].toUpperCase().trimLeft();
      }
    }
  }

  return {
    type: pdscTypes.UPDATE_PDSC_INPUT,
    data
  };
}

export let updateSearch = (data) => {
  return {
    type: pdscTypes.UPDATE_SEARCH_INPUT,
    data
  }
}

export let loadPDSCListSuccess = (data) => {
  return {
    type: pdscTypes.GET_PDSC_LIST,
    data
  }
}

export let loadPDSCInfoSuccess = (data) => {
  return {
    type: pdscTypes.GET_PDSC_INFO,
    data
  }
}

export let loadPDSCData = (data) => {
  return {
    type: pdscTypes.GET_PDSC_DATA,
    data
  }
}

export let updateDischargeInput = (data)=> {
  if(data != 'clear'){
    for(var prop in data){
      if(typeof data[prop] == 'string' && data[prop].indexOf('restapi/') == -1 && prop.toLowerCase().indexOf('signature') == -1){
        data[prop] = data[prop].toUpperCase().trimLeft();
      }
    }
  }

  return {
    type: pdscTypes.UPDATE_DISCHARGE_INPUT,
    data
  };
}

export let upsertDischargeForm = (dischargeData, callback=null) => {
  return dispatch => {
    if (dischargeData.id) {
      patch(Config.getHost()+'/restapi/dischargeNotices/'+dischargeData.id,dischargeData).then(response => {
        if(callback != null) dispatch(callback);
        return null;
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertDischargeForm patch', error);
        }
      });

    }else{
      post(Config.getHost()+'/restapi/dischargeNotices',dischargeData).then(response => {
        if(callback != null) dispatch(callback);
        return null;
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertDischargeForm post', error);
        }
      });
    }
  }
}

export let deleteDischarge = (dischargeData, callback=null) => {
  return dispatch => {
    _delete(Config.getHost()+'/restapi/dischargeNotices/'+dischargeData.id).then((response) => {
      if(callback != null) dispatch(callback);
    }).catch(function (error) {
    })
  }
}

export let updateConsentInput = (data) => {
  if(data != 'clear'){
    for(var prop in data){
      if(typeof data[prop] == 'string' && data[prop].indexOf('restapi/') == -1 && prop.toLowerCase().indexOf('signature') == -1){
        data[prop] = data[prop].toUpperCase().trimLeft();
      }
    }
  }

  return {
    type: pdscTypes.UPDATE_CONSENT_INPUT,
    data
  };
}

export let upsertConsent = (consentData) => {
  return dispatch => {
    if (consentData.id) {
      patch(Config.getHost()+'/restapi/pdscConsentRecords/'+consentData.id,consentData).then(response => {
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertConsent patch', error);
        }
      });

    }else{
      post(Config.getHost()+'/restapi/pdscConsentRecords',consentData).then(response => {
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertConsent post', error);
        }
      });
    }
  }
}

export let deleteConsent = (consentData) => {
  return dispatch => {
    _delete(Config.getHost()+'/restapi/pdscConsentRecords/'+consentData.id).then((response) => {
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error deleteConsent', error);
      }
    })
  }
}
