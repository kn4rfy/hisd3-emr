import * as pTypes from '../constants/PatientActionTypes';
import {get, post, put, patch, _delete} from '../utils/RestClient';
import * as pdscActions from './pdscactions';
import * as erActions from './erActions';
import moment from 'moment';
import Promise from 'bluebird'
import * as Config from '../config/Config'
import _ from 'lodash';

export let getPatientList = (page, filter, filterDob, size, getActivePatient = true) => {
  return (dispatch, getState) => {
    var formats = [
      moment.ISO_8601,
      "MM/DD/YYYY  :)  HH*mm*ss",
      "YYYY/MM/DD"
    ];

    get(Config.getHost() + '/restapi/patients/search/findByFilterDob',{
      params: {
        page: page - 1,
        filter,
        filterDob: (moment(filterDob, formats, true).isValid() ? moment(filterDob).format('MMMM DD, YYYY') : filterDob) || '',
        size
      }
    }).then((response)=>{
      if(response.data._embedded && response.data._embedded.patients){
        dispatch(loadPatientListSuccess(response.data));
        if(getActivePatient == true){
          if(!_.isEmpty(response.data._embedded.patients) && _.isEmpty(getState().patients.patientInfo)){
            dispatch(loadPatientInfoSuccess(response.data._embedded.patients[0]));
            dispatch(loadPatientActiveSuccess(response.data._embedded.patients[0]));
            dispatch(setSelectedPatient(response.data._embedded.patients[0].id))
            var patient = response.data._embedded.patients[0];
            if(patient.activePdsc != null){
              dispatch(pdscActions.loadPDSCInfoSuccess(patient.activePdsc));
              dispatch(pdscActions.updatePDSCInput(patient.activePdsc));
            }
          }
        }
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error getPatientList', error);
      }
    });
  }
};

export let setSelectedPatient = (patientId) => {
  return dispatch => {
    dispatch(setSelectedPatientOnVirtualize(patientId));
  }
};

export let getPatientInfo = (patientRest) => {
  return dispatch => {
    get(patientRest).then((response)=>{
      if(response.data)
      dispatch(loadPatientInfoSuccess(response.data));

    }).catch(function (error) {

    });
  }
};

export let getPatientInfoByFilter = (page, filter, filterDob, size) => {
  return (dispatch, getState) => {
    var formats = [
      moment.ISO_8601,
      "MM/DD/YYYY  :)  HH*mm*ss",
      "YYYY/MM/DD"
    ];

    get(Config.getHost() + '/restapi/patients/search/findByFilterDob',{
      params: {
        page: page - 1,
        filter,
        filterDob: (moment(filterDob, formats, true).isValid() ? moment(filterDob).format('MMMM DD, YYYY') : filterDob) || '',
        size
      }
    }).then((response)=>{
      if(response.data._embedded && response.data._embedded.patients){
        if(!_.isEmpty(response.data._embedded.patients)){
          dispatch(loadPatientInfoSuccess(response.data._embedded.patients[0]));
          dispatch(loadPatientActiveSuccess(response.data._embedded.patients[0]));
          dispatch(setSelectedPatient(response.data._embedded.patients[0].id))
          var patient = response.data._embedded.patients[0];
          if(patient.activePdsc != null){
            dispatch(pdscActions.loadPDSCInfoSuccess(patient.activePdsc));
            dispatch(pdscActions.updatePDSCInput(patient.activePdsc));
          }
        }
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error getPatientList', error);
      }
    });
  }
};

export let getPatientInfoById = (patientId, callback = null, progressShow = false) => {
  return dispatch => {
    Promise.coroutine(function *(){
      var response = yield  get(`${Config.getHost()}/restapi/patients/${patientId}`);
      var patient = response.data;
      //get patient insurance list
      try{
        var refResponse = yield get(patient._links.PatientInsuranceList.href);
        patient._insurances = refResponse.data._links.self.href;
        var refToEntityResponse = yield get(refResponse.data._links.self.href);
        patient.insurances = refToEntityResponse.data._embedded.patientInsuranceLists;
      }catch(e){}

      return patient;

    })().then((patient)=>{
      dispatch(loadPatientInfoSuccess(patient));
      dispatch(loadPatientActiveSuccess(patient));
      dispatch(callback(true));
      return null;

      if(__DEV__) {
        console.log('success getPatientInfoById', response);
      }
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error getPatientInfoById', error);
      }
    });
  }
};

export let addPatient = (patientData, resultfunc=()=>{}) => {
  return dispatch => {
    if (patientData.id) {
      delete patientData.activePdsc;

      patch(Config.getHost() + "/restapi/patients/" + patientData.id, patientData).then(response => {
        if(response.data)
        dispatch(getPatientList(1,"","",99999,false));
        dispatch(addPatientsSuccess(response.data));
        dispatch(loadPatientInfoSuccess(response.data));
        dispatch(loadPatientActiveSuccess(response.data));
        dispatch(pdscActions.getPDSCInfoByPatientId(response.data.id));
      }).then((response) =>{
        resultfunc(true, "Patient successfully updated");
        if(__DEV__) {
          console.log('success addPatient update', response);
        }
        return response;
      }).catch(function (error) {
        resultfunc(false, "Patient failed to update");
        if(__DEV__) {
          console.log('error addPatient update', error);
        }
      });
    }else{
      post(Config.getHost() + '/restapi/patients',patientData).then(response => {
        if(response.data)
        dispatch(getPatientList(1,"","",99999,false));
        dispatch(addPatientsSuccess(response.data));
        dispatch(loadPatientInfoSuccess(response.data));
        dispatch(loadPatientActiveSuccess(response.data));
        dispatch(pdscActions.getPDSCInfoByPatientId(response.data.id));
        return response;
      }).then((response) =>{
        resultfunc(true, "Patient successfully created");
        if(__DEV__) {
          console.log('success addPatient add', response);
        }
        return response;
      }).catch(function (error) {
        resultfunc(false, "Patient failed to create");
        if(__DEV__) {
          console.log('error addPatient add', error);
        }
      });
    }
  }
};

export let upsertPatientInsurance = (insuranceData, callback) => {
  return dispatch => {
    if (insuranceData.id) {
      patch(Config.getHost() + '/restapi/patientInsuranceLists/'+insuranceData.id,insuranceData).then(response => {
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertPatientInsurance patch', error);
        }
      });
    }else{
      post(Config.getHost() + '/restapi/patientInsuranceLists',insuranceData).then(response => {
      }).catch(function (error) {
        if(__DEV__) {
          console.log('error upsertPatientInsurance post', error);
        }
      });
    }
  }
};

export let deletePatientInsurance = (insuranceId, callback) => {
  return dispatch => {
    _delete(`${Config.getHost()}/restapi/patientInsuranceLists/${insuranceId}`).then((response) => {
      dispatch(callback);
    }).catch(function (error){
      if(__DEV__) {
        console.log('error deletePatientInsurance', error);
      }
    });
  }
};

export let updatePatient = (patient) => {
  return dispatch => {
    patch(patient._links.self.href, patient).then((response)=>{
      if(response.data)
      dispatch(loadPatientInfoSuccess(response.data));
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error updatePatient', error);
      }
    });
  }
};

export let updateSearch = (data) => {
  data = data.toUpperCase().trimLeft();

  return {
    type: pTypes.UPDATE_SEARCH_INPUT,
    data
  }
};

export let updateSearchDob = (data) => {
  return {
    type: pTypes.UPDATE_SEARCH_DOB,
    data
  }
};

export let loadPatientListSuccess = (data) => {
  return {
    type: pTypes.GET_PATIENT_LIST,
    data
  }
};

export let addPatientsSuccess = (data) => {
  return {
    type: pTypes.ADD_PATIENT,
    data
  }
};

export let loadPatientInfoSuccess = (data) => {
  return {
    type: pTypes.GET_PATIENT_INFO_BY_ID,
    data
  }
};

export let loadPatientActiveSuccess = (data) => {
  return {
    type: pTypes.GET_PATIENT_ACTIVE,
    data
  }
};

export let setSelectedPatientOnVirtualize = (patientId) => {
  return {
    type: pTypes.SET_SELECTED_PATIENT_ON_VIRTUALIZE,
    data: patientId
  }
};

export let updatePatientActive = (data)=> {
  if(data != 'clear'){
    for(var prop in data){
      if(typeof data[prop] == 'string' && data[prop].indexOf('restapi/') == -1 && prop.toLowerCase().indexOf('signature') == -1){
        data[prop] = data[prop].toUpperCase().trimLeft();
      }
    }
  }

  return {
    type: pTypes.UPDATE_PATIENTACTIVE,
    data
  };
};
