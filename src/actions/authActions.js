/**
* Created by albertoclarit on 1/13/16.
*/
import * as types from '../constants/AuthActionTypes';
import {post,get} from '../utils/RestClient';
import * as  healthchecks from './healthchecks';
import {AsyncStorage} from 'react-native'
import * as Config from '../config/Config'
export let login= (loginstate,resultfunc=()=>{})=>{
  return dispatch => {
    post(Config.getHost() + '/api/authentication',{},{
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      params:loginstate
    }).then((response)=>{
      setTimeout(()=>{
        dispatch(loginSuccess(resultfunc));
        dispatch(healthchecks.ping());

      },300);


    }).catch((error)=>{
      resultfunc(false,null);
      dispatch(loginFailed());
      dispatch(healthchecks.ping());
    });

  }
};

export let loginSuccess= (resultfunc)=>{


  return dispatch => {

    get(Config.getHost() + '/api/account').then((response)=>{

      let account = response.data;

      dispatch(accountReceived(account,false));

      AsyncStorage.setItem('login', (response.data.login)).then(()=>{

        if(!account.active){
          resultfunc(false,'Account is inactive. Please login to HISD3 site with your temporary password to activate.');

        }
        else {
          resultfunc(true);
        }


      });




    }).catch((error)=>{
      if(__DEV__) {
        console.log(error);
      }
      dispatch(healthchecks.ping());
      resultfunc(false,null);

      AsyncStorage.removeItem("login").then(()=>{

      });


    });

  };

};

export let accountReceived = (account,fromRefresh)=>{
  return {
    type: types.AUTH_ACCOUNTRECIEVE,
    account:account,
    fromRefresh:fromRefresh
  }
};

export let loginFailed = ()=>{
  return {
    type: types.AUTH_LOGIN_FAILED
  }
};



export let logout= (callBack)=>{

  return dispatch => {
    post(Config.getHost() + '/api/logout',{},{
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then((response)=>{
      AsyncStorage.removeItem("login").then(()=>{
        dispatch(logoutSuccess());
        dispatch(healthchecks.ping());
        callBack(true);
      });


    }).catch((error)=>{
      AsyncStorage.removeItem("login").then(()=>{
        dispatch(logoutSuccess());
        dispatch(healthchecks.ping());
        callBack(false);
      });

    });

  }
};



export let logoutSuccess= ()=>{

  return {
    type: types.AUTH_LOGOUT_SUCCESS
  }

};


/*

export let changePassword=(oldpassword,password)=>{
return dispatch=>{
RestClient.open({
method: 'POST',
path: '/api/account/change_password',
params:{
oldpassword,
password
}
}
).then((response)=>{
dispatch(dialogactions.openAlert("Password changed successfully",'Success','success'));
},(errorResponse)=>{

dispatch(dialogactions.openAlert(errorResponse.entity ||
"Failed to update password",'Failure','warning'));
});

}
};
*/
