/**
* Created by albertoclarit on 1/10/16.
*/
import * as types from '../constants/HealthCheckTypes';
import {get} from '../utils/RestClient';
import * as Config from '../config/Config'
export let ping=(callback)=>{

  return dispatch=>{

    get(Config.getHost() + '/api/public/ping').then((response)=>{
      if(response.data.success){
        if(__DEV__) {
          console.log('success ping');
        }
        dispatch(pingSuccess());

        setTimeout(()=>{
          if(callback){
            callback();
          }
        },300);

      }
      else
      dispatch(pingError());
    }).catch(function (error) {
      if(__DEV__) {
        console.log('error ping ', error);
      }
      dispatch(pingError());
    });

  }
}

export let pingSuccess= ()=> {
  return { type: types.HC_PINGSUCCESS }
}

export let pingError=()=> {
  return { type: types.HC_PINGERROR }
}

export let getFrontendVersion=()=> {
  return { type: types.HC_GETFRONTENDVER }
}
