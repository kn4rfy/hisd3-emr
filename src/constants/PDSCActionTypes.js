export const GET_PDSC_LIST = 'GET_PDSC_LIST';
export const GET_PDSC_INFO = 'GET_PDSC_INFO';
export const UPDATE_PDSC_INPUT = 'UPDATE_PDSC_INPUT';
export const UPDATE_SEARCH_INPUT = 'UPDATE_SEARCH_INPUT';
export const UPDATE_DISCHARGE_INPUT = 'UPDATE_DISCHARGE_INPUT';
export const UPDATE_CONSENT_INPUT = 'UPDATE_CONSENT_INPUT';
export const GET_PDSC_DATA = 'GET_PDSC_DATA';
