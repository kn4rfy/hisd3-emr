import React, { Component } from 'react';
import { Platform, Settings } from 'react-native';
import { Provider } from 'react-redux';

import RouterWithRedux from './HISD3Router';
import configureStore from '../stores/configureStore';

import * as Config from '../config/Config';
import * as TempConfig from '../utils/Config';

const store = configureStore();

export default class App extends Component {
  componentWillMount(){
    if(Platform.OS === 'ios'){
     Config.setHost(Settings.get("hisd3_host"));
    }
    //Config.setHost(TempConfig.url);
  }

  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux />
      </Provider>
    );
  }
}
