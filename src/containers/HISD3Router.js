import React, { Component } from 'react'
import { Alert } from 'react-native';

import {Scene, Router, ActionConst, Actions} from 'react-native-router-flux'
import { connect } from 'react-redux';
import {bindActionCreators} from "redux";
import * as authActions from '../actions/authActions';
import * as healthchecks from '../actions/healthchecks';
import * as Permissions  from '../permissions/Index';
import {requireAuthentication} from '../utils/AuthUtils';

import Auth        from '../components/Auth';
import PatientList from '../components/PatientList';
import PatientMenu from '../components/PatientMenu';
import NewCase from '../components/NewCase';
import PDSContainer from '../components/emergency/pdsc/PDSContainer';
import PMRContainer from '../components/emergency/pmr/PMRContainer'
// import MPNDOContainer from '../components/emergency/mpndo/MPNDOContainer'
//
// //iap components
// import IAPContainer from '../components/emergency/iap/IAPContainer'
// import IapDetails from '../components/emergency/iap/IapDetails'
// import IapClaimed from '../components/emergency/iap/IapClaimed'

// style
import * as HISstyle from '../styles/HISstyle';

const RouterWithRedux = connect()(Router);

class HISD3_EMR extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.healthchecks.ping();
  }

  logout=()=>{
    Alert.alert(
      'Confirm',
      'Do you really want to logout?',
      [{
        text: 'Yes',
        onPress: () => {
          this.props.authActions.logout((result)=>{
            Actions.auth();
          });
        },
      },
      {
        text: 'No',
        onPress: () => {},
      }]
    );
  }


  render() {
    return (
      <RouterWithRedux
        hideNavBar={true}
        showNavigationBar={false}
        duration={0}
        animation={"fade"}>

        <Scene key="root">
          <Scene key="auth" type={ActionConst.RESET}  component={Auth} />
          <Scene key="patients" initial={true} type={ActionConst.RESET}
            component={requireAuthentication(PatientList, Permissions.ER_PERMISSIONS)} title="Patient List"
            onRight={this.logout}
            rightTitle="Logout"
            rightButtonTextStyle={{color: 'white'}}
            />
          <Scene key="newcase" component={NewCase} title="New Case"/>
          <Scene key="patientmenu" component={PatientMenu} title="Patient Information" />
          <Scene key="pdsc" component={PDSContainer} title="Data Sheet Form"/>
          <Scene key="pmr" component={PMRContainer} title="Medical Record"/>

          {/*
          <Scene key="mpndo" component={MPNDOContainer} title="Doctors Orders and Progress Notes"/>
          <Scene key="iap" component={IAPContainer} title="Personal Effects"/>
          <Scene key="iapdetails" component={IapDetails} title="New Item"/>
          <Scene key="iapclaimed" component={IapClaimed} title="Claimed"/>*/}
        </Scene>

      </RouterWithRedux>
    );
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    healthchecks: bindActionCreators(healthchecks, dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(HISD3_EMR);
