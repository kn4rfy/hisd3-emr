const darkColor = '#2d2d2d';
const lightDarkColor ='#9a9a9a'
const darkBlueColor = '#2b6480';
const borderColor = '#89d4f9';
const lightBlueColor = '#fafafa';

const subTitleBg = '#57889c';

const formLabel = { color: '#2d2d2d', fontSize: 15, marginBottom: 7, fontWeight: 'bold' };
const switchFormLabel = { color: darkColor, fontSize: 15, marginBottom: 7, marginTop: 5, marginLeft: 10 };

const containerMargins = { marginTop: 64 };
const formGroup = { marginBottom: 10 };
const formGroupWell = { marginBottom: 10, padding: 10, borderRadius: 4, backgroundColor: '#ebf4fe' };
const formWell = { padding: 10, borderRadius: 4, backgroundColor: '#ebf4fe' };
const formInput = { color: '#464949', borderWidth: 1, borderColor: borderColor };

const titleBar = { padding: 10, marginBottom: 5 };
const titleBarText = { color: '#2d2d2d', fontSize: 25, fontWeight: 'bold', fontFamily: 'Helvetica' };

const subTitleBar = { marginBottom: 15 };
const subTitleBarText = { color: '#2d2d2d', fontSize: 20, fontWeight: 'bold' };
const well = { padding: 10, borderRadius: 4, backgroundColor: 'white', marginBottom: 10 };
const textInputStyle = { marginRight: 5, borderWidth:1, borderRadius: 2, borderColor: borderColor, flex: 0.7, padding:10, height:44, borderColor: borderColor };
const signButtonStyle = {backgroundColor: '#DB4A67', borderRadius: 3, borderColor: '#DB4A67' };
const saveButtonStyle = {backgroundColor: '#84A83E', borderRadius: 3, borderColor: '#96bf48' };
const primaryButtonStyle = {backgroundColor: '#3E90D4', borderRadius: 3, borderColor: '#2F7DBE' };
const deleteButtonStyle = {backgroundColor: '#CA4862', borderRadius: 3, borderColor: '#DB4A67' };
const warningButtonStyle = {backgroundColor: '#E2B14A', borderRadius: 3, borderColor: '#C99D42' };
const signButtonText = { fontSize: 18, color: 'white' };

const putShadow = {shadowColor: "#000000", shadowOpacity: 0.1, shadowRadius:2, shadowOffset: { height: 2, width: 0 }};
const patientListItem = { flexDirection: 'row', paddingTop: 10, paddingBottom: 20, paddingLeft: 10, backgroundColor: 'white', marginBottom: 10, marginLeft: 3, marginRight: 3, borderRadius: 2 };
const boxType = { paddingTop: 10, paddingBottom: 20, paddingLeft: 10, backgroundColor: 'white', marginBottom: 10, marginLeft: 3, marginRight: 3, borderRadius: 2 };
export {
  darkColor,
  lightDarkColor,
  darkBlueColor,
  lightBlueColor,
  patientListItem,
  putShadow,
  formLabel,
  formGroup,
  formGroupWell,
  formWell,
  formInput,
  containerMargins,
  boxType,
  titleBar,
  titleBarText,
  subTitleBar,
  subTitleBarText,
  switchFormLabel,
  well,
  textInputStyle,
  signButtonStyle,
  signButtonText,
  saveButtonStyle,
  primaryButtonStyle,
  deleteButtonStyle,
  warningButtonStyle
}
