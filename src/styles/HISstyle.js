//colors
const mainColor = '#0E5EA8';
const mainColorBack = '#e2f1ff';

const headColor = '#1681E6';
const ligthdarktextcolor = '#A3A3A3';
const darktextColor = '#333333'
const dangerColor = '#E57373';
const backColor = '#EEEEEE';
const borderlineColor = '#F6F7FE';
const buttonColor = '#1681E6'
const lightGreen = '#16C2AF';
const darkMainColor = '#4D7FB3';

const redColor = '#FF6E72';
const redColorBack = '#ffddde';

const labelColor = '#2d2d2d';
const borderRightColor = '#E2E2E2';
const lightyellow = '#FFC751';
const loginBackColor = '#0E5EA8';


//font family
const fnFamilyThin = 'Roboto-Thin';
const fnFamilyRegular = 'Roboto-Regular';
const fnFamilyLight = 'Roboto-Light';

// TextView Style
const headTitle = {fontFamily:fnFamilyRegular,fontSize: 19, color: '#232424',marginBottom:5}
const bodyText = {fontFamily:fnFamilyThin,fontSize: 18,fontWeight:'400',color: ligthdarktextcolor}
const formLabel = { color:labelColor, marginBottom: 12,fontFamily:fnFamilyLight,fontSize: 16};
const formLabelHead = { color:labelColor, marginBottom: 7,fontFamily:fnFamilyLight,fontSize: 15,fontWeight:'400'};
const titleHead = {fontFamily:fnFamilyRegular,fontSize: 15, color: '#A3A3A3',marginBottom:5};

//switch style
const switchFormLabel = { color: labelColor, fontSize: 15, marginBottom: 7, marginTop: 5, marginLeft: 10,fontFamily:fnFamilyLight};


//input style
const formInput = { color: labelColor, borderWidth: 1, borderColor: mainColor , fontFamily:fnFamilyLight, fontSize:15 };
const textInputStyle = { marginRight: 5, borderWidth:1, borderRadius: 2, borderColor: labelColor, flex: 0.7, padding:10, height:44, borderColor: mainColor };

//button style
const saveButtonStyle = { height:50, borderRadius: 1,marginBottom:-10, borderColor: mainColor,backgroundColor:mainColor}
const saveText = {fontFamily:fnFamilyRegular,color:'white'}
const deleteButtonStyle =  { height:50, borderRadius: 1,marginBottom:-10, borderColor: redColor,backgroundColor:redColor}
const deleteText = {fontFamily:fnFamilyRegular,color:'white'}
const warningButton =  { height:50, borderRadius: 1,marginBottom:-10, borderColor: lightyellow,backgroundColor:lightyellow}
const warningText = {fontFamily:fnFamilyRegular,color:'white'}
const addButton =  { height:50, borderRadius: 1, borderColor: lightGreen,backgroundColor:lightGreen}
const addText = {fontFamily:fnFamilyRegular,color:'white'}

// patientList
const patientListItem = { flexDirection: 'row', padding:10, backgroundColor: 'white', borderRadius: 2, borderRightWidth: 1, borderRightColor:backColor };
const lineBottom = { borderBottomWidth: 2, borderBottomColor: backColor}
const putShadow = {shadowColor: "#000000", shadowOpacity: 0.1, shadowRadius:2, shadowOffset: { height: 2, width: 0 }};

//continer
const putContainer = [{marginRight:10,marginLeft:10,marginTop:10,backgroundColor:'white'},putShadow];
const putContainerPadding = [{marginRight:10,marginLeft:10,marginTop:10,padding:10,marginBottom:1,backgroundColor:'white'},putShadow];
const putPadding = {padding:10};
const formGroup = { marginBottom: 15 };

//well
const wellContainer = { flexDirection: 'row' };
const wellHeadTitle = {fontFamily: fnFamilyRegular, fontSize: 15, marginLeft:15,marginTop:10,marginBottom:10,color: '#757575'};

//header
const headerContainer = [{backgroundColor:mainColor},putShadow]
const headerHeadTitle = {fontFamily:fnFamilyRegular,fontSize: 23, color: borderRightColor,marginBottom:5}
const headerBodyText = {fontFamily:fnFamilyThin,fontSize: 18,fontWeight:'400',color: borderRightColor}
const headerTitleHead = {fontFamily:fnFamilyRegular,fontSize: 15, color: borderRightColor,marginBottom:5};

//Modal

const HISstyleModal = {justifyContent: 'center',alignItems: 'center',height: 400,width:400};

const orderLabel = {fontSize: 11, color: '#515151'};

export {
  orderLabel,
  patientListItem,
  putShadow,
  headTitle,
  bodyText,
  darkMainColor,
  mainColor,
  mainColorBack,
  headColor,
  dangerColor,
  backColor,
  borderlineColor,
  buttonColor,
  lightGreen,
  ligthdarktextcolor,
  darktextColor,
  lineBottom,
  redColor,
  redColorBack,
  saveButtonStyle,
  saveText,
  deleteButtonStyle,
  deleteText,
  putContainer,
  putContainerPadding,
  formLabel,
  formInput,
  formGroup,
  switchFormLabel,
  formLabelHead,
  titleHead,
  borderRightColor,
  wellContainer,
  wellHeadTitle,
  lightyellow,
  loginBackColor,
  putPadding,
  warningButton,
  warningText,
  headerContainer,
  headerHeadTitle,
  headerBodyText,
  headerTitleHead,
  textInputStyle,
  addButton,
  addText,
  HISstyleModal
}
 
