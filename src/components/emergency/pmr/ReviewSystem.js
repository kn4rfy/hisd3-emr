import React, { Component } from 'react';
import ReactNative, {
  Dimensions,
  PixelRatio,
  View,
  Text,
  TextInput,
  Picker,
  ScrollView,
  Switch,
  ListView
} from 'react-native';

import Icon from 'react-native-vector-icons/EvilIcons';
import { Hideo } from 'react-native-textinput-effects';
import ModalPicker from 'react-native-modal-picker';
import Button from 'apsl-react-native-button';
import Slider from 'react-native-slider';
import _ from 'lodash';

import {requireAuthentication} from '../../../utils/RoleComponent';

import * as style from '../../../styles/styles';
import TitleBar from '../../general/TitleBar'

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

const generalOptions = [
    { value: 'Body weakness', label: 'Body weakness' },
    { value: 'Fever', label: 'Fever' },
    { value: 'Weight loss', label: 'Weight loss' }
];

const skinMusculoSkeletalOptions = [
    { value: 'Back pain', label: 'Back pain' },
    { value: 'Joint pain', label: 'Fever pain' },
    { value: 'Muscle pain', label: 'Muscle pain' },
    { value: 'Rashes', label: 'Rashes' }
];

const eentOptions = [
    { value: 'Blurring of vision', label: 'Blurring of vision' },
    { value: 'Cough/Colds', label: 'Cough/Colds' },
    { value: 'Tinitus', label: 'Tinitus' },
    { value: 'Hoarseness', label: 'Hoarseness' }
];

const cardioPulmonaryOptions = [
    { value: 'Chest pain', label: 'Chest pain' },
    { value: 'Palpitations', label: 'Palpitations' },
    { value: 'Dyspnea', label: 'Dyspnea' },
    { value: 'Orthopnea', label: 'Orthopnea' }
];

const gastroIntestinalOptions = [
    { value: 'Abdominal pain', label: 'Abdominal pain' },
    { value: 'Nausea/Vomiting', label: 'Nausea/Vomiting' },
    { value: 'Black/Bloody stool', label: 'Black/Bloody stool' },
    { value: 'Diarrhea', label: 'Diarrhea' }
];

const urinaryOptions = [
    { value: 'Dysurin', label: 'Dysurin' },
    { value: 'Frequency in urination', label: 'Frequency in urination' },
    { value: 'Hematuria', label: 'Hematuria' }
];

const femaleGenitalOptions = [
    { value: 'Abnormal vaginal bleeding/discharge', label: 'Abnormal vaginal bleeding/discharge' },
    { value: 'LMP', label: 'LMP' },
    { value: 'Postmenopausal', label: 'Postmenopausal' },
    { value: 'Hysteroctomy', label: 'Hysteroctomy' }
];

const neurologicOptions = [
    { value: 'Confusion', label: 'Confusion' },
    { value: 'Double vision', label: 'Double vision' },
    { value: 'Loss of sensation', label: 'Loss of sensation' },
    { value: 'Difficulty with speech', label: 'Difficulty with speech' },
    { value: 'Headache', label: 'Headache' },
    { value: 'Difficulty with walking', label: 'Difficulty with walking' },
    { value: 'Loss of consciousness', label: 'Loss of consciousness' }
];

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class ReviewSystem extends Component {
  constructor(props) {
    super(props);
  }

  updateInputField=(id, data, mode)=>{
      const payload = [];
      if(_.get(this.props.er.reviewSystems.activeRecord.records, id)){
        let recordsArray = this.props.er.reviewSystems.activeRecord.records[id].split(',')
        recordsArray.map((item, i)=>{
          payload.push(item);
        });
      }
      if(mode == false){
        var index = payload.indexOf(data);
        payload.splice(index, 1);
      }else {
        payload.push(data);
      }
      this.props.erActions.updateReviewSystemInput(id, String(payload));
  }

  renderRow = (id, rowData) => {
    return (
      <View style={{ width: (50*vw) -60, height: 80, margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
        <Text style={style.formLabel}>{rowData.value}</Text>
        <Switch
          onValueChange={(value)=>{this.updateInputField(id, rowData.value, value)}}
          value={_.get(this.props.er.reviewSystems.activeRecord.records, id) ?
            (this.props.er.reviewSystems.activeRecord.records[id].includes(rowData.value) == true ? true : false) : false}
          />
      </View>
    )
  }

  onPressSave=()=>{
    var reviewSystemsPayload = {};

    if(_.get(this.props.er.reviewSystems.activeRecord, 'id')) {
      reviewSystemsPayload.records = JSON.stringify(this.props.er.reviewSystems.activeRecord.records);
      this.props.erActions.updateReviewSystem(this.props.er.reviewSystems.activeRecord.id ,reviewSystemsPayload);
    } else {
      reviewSystemsPayload.erPmr = this.props.er.pmr.activeRecord._links.self.href;
      reviewSystemsPayload.records = JSON.stringify(this.props.er.reviewSystems.activeRecord.records);
      this.props.erActions.addReviewSystem(reviewSystemsPayload, this.props.patientId);
    }
  }

  render() {
    return (
      <ScrollView>
        <View>
        <TitleBar title={'Review Systems Form'} />

          <View style={style.well}>
            <View style={{flex: 1}}>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>GENERAL</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(generalOptions)}
                  renderRow={(rowData)=> this.renderRow('general', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>SKIN MUSCULO-SKELETAL</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(skinMusculoSkeletalOptions)}
                  renderRow={(rowData)=> this.renderRow('skinMusculoSkeletal', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>EENT</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(eentOptions)}
                  renderRow={(rowData)=> this.renderRow('eent', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>CARDIO-PULMONARY</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(cardioPulmonaryOptions)}
                  renderRow={(rowData)=> this.renderRow('cardioPulmonary', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>GASTROINTESTINAL</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(gastroIntestinalOptions)}
                  renderRow={(rowData)=> this.renderRow('gastroIntestinal', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>URINARY</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(urinaryOptions)}
                  renderRow={(rowData)=> this.renderRow('urinary', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>FEMALE GENITAL</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(femaleGenitalOptions)}
                  renderRow={(rowData)=> this.renderRow('femaleGenital', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>NEUROLOGIC</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(neurologicOptions)}
                  renderRow={(rowData)=> this.renderRow('neurologic', rowData)}
                />
              </View>
            </View>
          </View>

          {
            requireAuthentication((
              <View style={style.well}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 0.5, paddingRight: 5 }}>
                    <Button className="primary" onPress={this.onPressSave} textStyle={style.signButtonText} style={[style.saveButtonStyle]} >
                      Save
                    </Button>
                  </View>
                </View>
              </View>
            ), this.props.auth.account, "ROLE_ER_NURSE")
          }

        </View>
      </ScrollView>
    );
  }
}
