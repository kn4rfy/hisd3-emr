import React, { Component } from 'react';
import { Dimensions, View, Alert, StyleSheet, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';
import moment from 'moment';
import _ from 'lodash';

import {connect} from 'react-redux';
import {Actions, Scene} from 'react-native-router-flux';
import {bindActionCreators} from 'redux';

import * as authActions from '../../../actions/authActions';
import * as healthchecks from '../../../actions/healthchecks';
import * as erActions from '../../../actions/erActions';

import PatientInfo from '../../PatientInfo';
import PatientMedicalRecord from './PatientMedicalRecord';
import VitalSigns from './VitalSigns';
import MedicalHistories from './MedicalHistories';
// import ReviewSystem from './ReviewSystem';
// import PhysicalExamination from './PhysicalExamination';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class PMRContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidMount() {
    this.props.erActions.getPMRByPatientId(this.props.patientId);
    this.props.erActions.getVitalSignsByPatientId(this.props.patientId);
    this.props.erActions.getPastMedicalHistoryByPatientId(this.props.patientId);
    this.props.erActions.getPresentMedicalHistoryByPatientId(this.props.patientId);
    this.props.erActions.getReviewSystemByPatientId(this.props.patientId);
    this.props.erActions.getPhysicalExaminationByPatientId(this.props.patientId);
  }

  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button transparent onPress={()=>Actions.pop()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Patient Medical Records</Title>
          </Body>
          <Right />
        </Header>
        {/*<Content scrollEnabled={false}
          onContentSizeChange={(contentWidth, contentHeight) => {
              this.setState({contentHeight: contentHeight});
          }}
          contentContainerStyle={{flex: 1, height: Math.max(this.state.contentHeight)}}>*/}
          <Content scrollEnabled={false} contentContainerStyle={{flex: 1}}>
          <PatientInfo {...this.props} />
          <Tabs>
            <Tab heading="Medical Records">
              <PatientMedicalRecord {...this.props}/>
            </Tab>
            <Tab heading="Vital Signs">
              <VitalSigns {...this.props} />
            </Tab>
            <Tab heading="Medical Histories">
              <MedicalHistories {...this.props} />
            </Tab>
            <Tab heading="Review Systems">
              {/*<ReviewSystem {...this.props} />*/}
            </Tab>
            <Tab heading="Physical Exams">
              {/*<PhysicalExamination {...this.props} />*/}
            </Tab>
          </Tabs>
        </Content>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth:state.auth,
    routes:state.routes,
    healthchecks:state.healthchecks,
    patients:state.patients,
    er:state.er
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    healthchecks: bindActionCreators(healthchecks, dispatch),
    erActions: bindActionCreators(erActions, dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PMRContainer);
