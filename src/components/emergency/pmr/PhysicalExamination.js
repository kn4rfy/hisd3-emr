import React, { Component } from 'react';
import ReactNative, {
  Dimensions,
  PixelRatio,
  View,
  Text,
  TextInput,
  Picker,
  ScrollView,
  Switch,
  ListView
} from 'react-native';

import Icon from 'react-native-vector-icons/EvilIcons';
import { Hideo } from 'react-native-textinput-effects';
import ModalPicker from 'react-native-modal-picker';
import Button from 'apsl-react-native-button';
import Slider from 'react-native-slider';
import _ from 'lodash';

import {requireAuthentication} from '../../../utils/RoleComponent';

import * as style from '../../../styles/styles';
import TitleBar from '../../general/TitleBar'

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

const generalSurveyOptions = [
    { value: 'Conscious', label: 'Conscious' },
    { value: 'Coherent', label: 'Coherent' },
    { value: 'No acute access', label: 'No acute access' },
    { value: 'Distress - Moderate', label: 'Distress - Moderate' },
    { value: 'Distress - Severe', label: 'Distress - Severe' }
];

const skin = [
    { value: 'Normal', label: 'Normal' },
    { value: 'Warm and Dry', label: 'Warm, Dry' },
    { value: 'Cyanosis/Diaphoresis/Pallor', label: 'Cyanosis/Diaphoresis/Pallor' },
    { value: 'Skin lesion', label: 'Skin lesion' },
    { value: 'Jaundice', label: 'Jaundice' }
];

const respiratoryOptions = [
    { value: 'Breath sounds normal', label: 'Breath sounds normal' },
    { value: 'Asymmetric chest expansion', label: 'Asymmetric chest expansion' },
    { value: 'Decreased breath sound', label: 'Decreased breath sound' },
    { value: 'Crackles', label: 'Crackles' },
    { value: 'Stridor', label: 'Stridor' },
    { value: 'Wheezing', label: 'Wheezing' }
];

const rectalOptions = [
    { value: 'Negative stool', label: 'Negative stool' },
    { value: 'Black/Bloody stool', label: 'Black/Bloody stool' },
    { value: 'Tenderness/Mass/Module', label: 'Tenderness/Mass/Module' }
];

const eentOptions = [
    { value: 'Normal', label: 'Normal' },
    { value: 'Pale conjunctivac', label: 'Pale conjunctivac' },
    { value: 'Purulent nasal discharge', label: 'Purulent nasal discharge' },
    { value: 'Pharyngeal erythema exudates', label: 'Pharyngeal erythema exudates' },
    { value: 'Leteric sclera', label: 'Leteric sclera' }
];

const cardiovascularSystemOptions = [
    { value: 'Regular rate & Rhythm', label: 'Regular rate & Rythm' },
    { value: 'No murmur', label: 'No murmur' },
    { value: 'Irregular rhythm', label: 'Irregular rhythm' },
    { value: 'Rextrastoles - Occasional', label: 'Rextrastoles - Occasional' },
    { value: 'Rextrastoles - Frequent', label: 'Rextrastoles - Frequent' },
    { value: 'Gallop - S3', label: 'Gallop - S3' },
    { value: 'Gallop - S4', label: 'Gallop - S4' },
    { value: 'Murmur', label: 'Murmur' },
    { value: 'PMI displaced laterally', label: 'PMI displaced laterally' },
    { value: 'Tachycardia/Bradycardia', label: 'Tachycardia/Bradycardia' }
];

const abdomenOptions = [
    { value: 'Flat', label: 'Flat' },
    { value: 'Soft', label: 'Soft' },
    { value: 'Globular', label: 'Globular' },
    { value: 'Distended', label: 'Distended' },
    { value: 'Rigid', label: 'Rigid' },
    { value: 'No organomegaly', label: 'No organomegaly' },
    { value: 'Non-tender', label: 'Non-tender' },
    { value: 'Abnormal bowel sound - Increased', label: 'Abnormal bowel sound - Increased' },
    { value: 'Abnormal bowel sound - Decreased', label: 'Abnormal bowel sound - Deccreased' },
    { value: 'Abnormal bowel sound - Absent', label: 'Abnormal bowel sound - Absent' },
    { value: 'Guarding', label: 'Guarding' },
    { value: 'Hepatomegaly/Splenomegaly/Mass', label: 'Hepatomegaly/Splenomegaly/Mass' },
    { value: 'Rebound tenderness', label: 'Rebound tenderness' }
];

const neckOptions = [
    { value: 'Inspection normal', label: 'Inspection normal' },
    { value: 'Thyroid normal', label: 'Thyroid normal' }
];

const backOptions = [
    { value: 'Inspection normal', label: 'Inspection normal' },
    { value: 'CVA tenderness - Right', label: 'CVA tenderness - Right' },
    { value: 'CVA tenderness - Left', label: 'CVA tenderness - Left' }
];

const extremitiesOptions = [
    { value: 'Full ROM', label: 'Full ROM' },
    { value: 'No pedal edema', label: 'No pedal edema' },
    { value: 'Calf tenderness', label: 'Calf tenderness' },
    { value: 'Pedal edema', label: 'Pedal edema' }
];

const neurologicPsychiatryOptions = [
    { value: 'Oriented to 3 spheres - No motor deficit', label: 'Oriented to 3 spheres - No motor deficit' },
    { value: 'Oriented to 3 spheres - CN normal', label: 'Oriented to 3 spheres - CN normal' },
    { value: 'Oriented to 3 spheres - Abnormal CN', label: 'Oriented to 3 spheres - Abnormal CN' },
    { value: 'Disoriented to person/place/time', label: 'Disoriented to person/place/time' },
    { value: 'Facial droop/EOM palsy/Anisocoria', label: 'Facial droop/EOM palsy/Anisocoria' },
    { value: 'Weakness/Sensory loss', label: 'Weakness/Sensory loss' },
    { value: 'Mood/Affect normal', label: 'Mood/Affect normal' },
    { value: 'Depressed affect', label: 'Depressed affect' }
];

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class PhysicalExamination extends Component {
  constructor(props) {
    super(props);
  }

  updateInputField=(id, data, mode)=>{
      const payload = [];
      if(_.get(this.props.er.physicalExaminations.activeRecord.records, id)){
        let recordsArray = this.props.er.physicalExaminations.activeRecord.records[id].split(',')
        recordsArray.map((item, i)=>{
          payload.push(item);
        });
      }
      if(mode == false){
        var index = payload.indexOf(data);
        payload.splice(index, 1);
      }else {
        payload.push(data);
      }
      this.props.erActions.updatePhysicalExaminationInput(id, String(payload));
  }

  renderRow = (id, rowData) => {
    return (
      <View style={{ width: (50*vw) -60, height: 80, margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
        <Text style={style.formLabel}>{rowData.value}</Text>
        <Switch
          onValueChange={(value)=>{this.updateInputField(id, rowData.value, value)}}
          value={_.get(this.props.er.physicalExaminations.activeRecord.records, id) ?
            (this.props.er.physicalExaminations.activeRecord.records[id].includes(rowData.value) == true ? true : false) : false}
          />
      </View>
    )
  }

  onPressSave=()=>{
    var physicalExaminationsPayload = {};

    if(_.get(this.props.er.physicalExaminations.activeRecord, 'id')) {
      physicalExaminationsPayload.records = JSON.stringify(this.props.er.physicalExaminations.activeRecord.records);
      this.props.erActions.updatePhysicalExamination(this.props.er.physicalExaminations.activeRecord.id ,physicalExaminationsPayload);
    } else {
      physicalExaminationsPayload.erPmr = this.props.er.pmr.activeRecord._links.self.href;
      physicalExaminationsPayload.records = JSON.stringify(this.props.er.physicalExaminations.activeRecord.records);
      this.props.erActions.addPhysicalExamination(physicalExaminationsPayload, this.props.patientId);
    }
  }

  render() {
    return (
      <ScrollView>
        <View>
        <TitleBar title={'Physical Examination Form'} />

          <View style={style.well}>
            <View style={{flex: 1}}>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>GENERAL SURVEY</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(generalSurveyOptions)}
                  renderRow={(rowData)=> this.renderRow('generalSurvey', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>SKIN</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(skin)}
                  renderRow={(rowData)=> this.renderRow('skin', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>RESPIRATORY</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(respiratoryOptions)}
                  renderRow={(rowData)=> this.renderRow('respiratory', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>RECTAL</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(rectalOptions)}
                  renderRow={(rowData)=> this.renderRow('rectal', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>EENT</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(eentOptions)}
                  renderRow={(rowData)=> this.renderRow('eent', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>CARDIOVASCULAR SYSTEM</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(cardiovascularSystemOptions)}
                  renderRow={(rowData)=> this.renderRow('cardiovascularSystem', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>ABDOMEN</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(abdomenOptions)}
                  renderRow={(rowData)=> this.renderRow('abdomen', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>NECK</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(neckOptions)}
                  renderRow={(rowData)=> this.renderRow('neck', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>BACK</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(backOptions)}
                  renderRow={(rowData)=> this.renderRow('back', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>EXTREMETIES</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(extremitiesOptions)}
                  renderRow={(rowData)=> this.renderRow('extremities', rowData)}
                />
              </View>

              <View style={style.subTitleBar} >
                <Text style={style.subTitleBarText}>NEUROLOGIC PSYCHIATRY</Text>
              </View>
              <View style={style.formGroupWell}>
                <ListView contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={ds.cloneWithRows(neurologicPsychiatryOptions)}
                  renderRow={(rowData)=> this.renderRow('neurologicPsychiatry', rowData)}
                />
              </View>

            </View>
          </View>

          {
            requireAuthentication((
              <View style={style.well}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 0.5, paddingRight: 5 }}>
                    <Button className="primary" onPress={this.onPressSave} textStyle={style.signButtonText} style={[style.saveButtonStyle]} >
                      Save
                    </Button>
                  </View>
                </View>
              </View>
            ), this.props.auth.account, "ROLE_ER_NURSE")
          }

        </View>
      </ScrollView>
    );
  }
}
