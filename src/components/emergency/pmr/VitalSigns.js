import React, { Component } from 'react';
import ReactNative, { Dimensions, View, Alert, Modal } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card, Footer, FooterTab } from 'native-base';
import moment from 'moment';
import _ from 'lodash';
import ModalPicker from 'react-native-modal-picker';
import Spinner from 'react-native-loading-spinner-overlay';

import {requireAuthentication} from '../../../utils/RoleComponent';

import VitalSignsChart from './VitalSignsChart';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class VitalSigns extends Component {
  constructor(props) {
    super(props);
    this.state ={
      showVitalSignsForm: false,
      spinVisible: false
    }
  }

  updateVitalSignsField=(id)=>{
    return(data)=>{
      this.props.erActions.updateVitalSignsInput(id, data);
    }
  }

  showSpinner=(boolVal) => {
    this.setState({ spinVisible: boolVal })
  }

  _scrollToInput (reactNode: any) {
    // Add a 'scroll' ref to your ScrollView
    this.refs.scroll.scrollToFocusedInput(reactNode)
  }

  showResponse=(result, message)=>{
    this.showSpinner(false)

    if(result){
      Alert.alert(
        'Success',
        message,
        [{
          text: 'OK',
          onPress: this.onPressShowVitalSignsForm(false),
        }]
      );
    }
    else {
      Alert.alert(
        'Error',
        message,
        [{
          text: 'OK',
          onPress: this.onPressShowVitalSignsForm(false),
        }]
      );
    }
  }

  onPressShowVitalSignsForm=(mode)=>{
    return()=>{
      this.setState({showVitalSignsForm: mode});
    }
  }

  onPressSaveConfirm=()=>{
    Alert.alert(
      'Confirm',
      'Do you really want to complete the input and save?',
      [{
        text: 'Yes',
        onPress: this.onPressSave,
      },
      {
        text: 'No',
        onPress: () => {},
      }]
    );
  }

  onPressSave=()=>{
    var vitalSignsPayload = {};

    if(_.get(this.props.er.vitalSigns.activeRecord, 'id')) {
      Alert.alert(
        'Info',
        'Vital signs has already been saved.',
        [{
          text: 'OK',
          onPress: () => {this.onPressShowVitalSignsForm(false)},
        }]
      );
    } else {
      vitalSignsPayload.erPmr = this.props.er.pmr.activeRecord._links.self.href;
      vitalSignsPayload.records = JSON.stringify(this.props.er.vitalSigns.activeRecord.records);
      this.props.erActions.addVitalSigns(vitalSignsPayload, this.props.patientId, (result, message)=>{
        this.showResponse(result, message);
      });
    }
  }

  render() {
    return (
      <Container>
        <Content padder>
          <VitalSignsChart {...this.props} />
        </Content>

        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.showVitalSignsForm}
          onRequestClose={()=>{}}
          >
          <Container>
            <Header>
              <Left>
                <Button transparent onPress={this.onPressShowVitalSignsForm(false)}>
                  <Icon name='arrow-back' />
                </Button>
              </Left>
              <Body>
                <Title>Add Vital Signs Form</Title>
              </Body>
              <Right />
            </Header>
            <Content padder>
              <Item style={{borderBottomWidth: 0}}>
                <Title>BLOOD PRESSURE (mmHg)</Title>
              </Item>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>SYSTOLIC (mmHg)</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('bloodPressureSystolic')}
                      value={this.props.er.vitalSigns.activeRecord.records.bloodPressureSystolic || ""}
                      placeholder={'Systolic'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>DIASTOLIC (mmHg)</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('bloodPressureDiastolic')}
                      value={this.props.er.vitalSigns.activeRecord.records.bloodPressureDiastolic || ""}
                      placeholder={'Diastolic'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>
              </View>

              <Item style={{borderBottomWidth: 0}}>
                <Title>HEART RATE (bpm)</Title>
              </Item>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>BEATS PER MINUTE</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('heartRate')}
                      value={this.props.er.vitalSigns.activeRecord.records.heartRate || ""}
                      placeholder={'Beats per minute'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>HEART RYTHM</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('heartRateRythm')}
                      value={this.props.er.vitalSigns.activeRecord.records.heartRateRythm || ""}
                      placeholder={'Rythm'}
                      />
                  </Item>
                </Item>
              </View>

              <Item style={{borderBottomWidth: 0}}>
                <Title>PULSE RATE (bpm)</Title>
              </Item>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>BEATS PER MINUTE</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('pulseRate')}
                      value={this.props.er.vitalSigns.activeRecord.records.pulseRate || ''}
                      placeholder={'Beats per minute'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>PULSE RYTHM</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('pulseRateRythm')}
                      value={this.props.er.vitalSigns.activeRecord.records.pulseRateRythm || ''}
                      placeholder={'Rythm'}
                      />
                  </Item>
                </Item>
              </View>

              <Item style={{borderBottomWidth: 0}}>
                <Title>RESPIRATORY RATE (bpm)</Title>
              </Item>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>BEATS PER MINUTE</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('respiratoryRate')}
                      value={this.props.er.vitalSigns.activeRecord.records.respiratoryRate || ''}
                      placeholder={'Beats per minute'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>RESPIRATORY RYTHM</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('respiratoryRateRythm')}
                      value={this.props.er.vitalSigns.activeRecord.records.respiratoryRateRythm || ''}
                      placeholder={'Rythm'}
                      />
                  </Item>
                </Item>
              </View>

              <Item style={{borderBottomWidth: 0}}>
                <Title>BODY TEMPERATURE (°C)</Title>
              </Item>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>TEMPERATURE IN CELCIUS</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('temperature')}
                      value={this.props.er.vitalSigns.activeRecord.records.temperature || ""}
                      placeholder={'Temperature'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>TEMPERATURE SOURCE</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('temperatureSource')}
                      value={this.props.er.vitalSigns.activeRecord.records.temperatureSource || ""}
                      placeholder={'Source'}
                      />
                  </Item>
                </Item>
              </View>

              <View style={{flexDirection: 'row'}}>
                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>OXYGEN SATURATION (%)</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('oxygenSaturation')}
                      value={this.props.er.vitalSigns.activeRecord.records.oxygenSaturation || ""}
                      placeholder={'Oxygen saturation'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>

                <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                  <Label>PAIN SCORE</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updateVitalSignsField('painScore')}
                      value={this.props.er.vitalSigns.activeRecord.records.painScore || ""}
                      placeholder={'Pain score'}
                      keyboardType={'numeric'}
                      />
                  </Item>
                </Item>
              </View>

              <Item stackedLabel style={{borderBottomWidth: 0}}>
                <Label>GLASGOW COMA SCALE</Label>
                <Item regular style={{flex: 0.5}}>
                  <Input
                    onChangeText={this.updateVitalSignsField('glasgowComaScale')}
                    value={this.props.er.vitalSigns.activeRecord.records.glasgowComaScale || ""}
                    placeholder={'Glasgow coma scale'}
                    />
                </Item>
              </Item>

            </Content>
          </Container>
          <Footer >
            <FooterTab>
              <Button danger full onPress={this.onPressShowVitalSignsForm(false)}
                style={{flex: 0.4}}>
                <Text style={{color: '#FFF'}}>Close</Text>
              </Button>
              <Button success full onPress={this.onPressSaveConfirm}
                style={{flex: 0.4}}>
                <Text style={{color: '#FFF'}}>Submit</Text>
              </Button>
            </FooterTab>
          </Footer>
        </Modal>

        <Footer>
          <FooterTab>
            {
              requireAuthentication((
                <Button primary full onPress={this.onPressShowVitalSignsForm(true)}
                  style={{flex: 1}}>
                  <Text style={{color: '#FFF'}}>Add Vital Signs</Text>
                </Button>
              ), this.props.auth.account, "ROLE_ER_NURSE")
            }
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
