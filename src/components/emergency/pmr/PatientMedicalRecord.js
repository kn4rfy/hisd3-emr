import React, { Component } from 'react';
import ReactNative, { Dimensions, View, Alert, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card } from 'native-base';
import moment from 'moment';
import _ from 'lodash';
import ModalPicker from 'react-native-modal-picker';
import Spinner from 'react-native-loading-spinner-overlay';

import {requireAuthentication} from '../../../utils/RoleComponent';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

const allergyTypeOptions = [
  { value: 'food', label: 'Food' },
  { value: 'drug', label: 'Drug' },
  { value: 'others', label: 'Others' }
];

const allergyFoodOptions = [
  { value: 'Peanuts', label: 'Peanuts' },
  { value: 'Shrimps', label: 'Shrimps' },
  { value: 'Crabs', label: 'Crabs' },
  { value: 'Seafoods', label: 'Seafoods' }
];

const allergyDrugOptions = [
  { value: 'Methapethamine', label: 'Methapethamine' },
  { value: 'Penicillin', label: 'Penicillin' },
  { value: 'Colistin', label: 'Colistin' }
];

export default class PatientMedicalRecord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      food: '',
      drug: '',
      others: '',
      allergyFoodOptions: allergyFoodOptions,
      allergyDrugOptions: allergyDrugOptions,
      allergyOthersOptions: [],
      spinVisible: false,
      isVisible: false
    }
  }

  componentDidMount(){
    this.syncPropsToState(this.props);
  }

  syncPropsToState=(props)=>{
    this.setState({
      height: props.er.pmr.activeRecord.height,
      weight: props.er.pmr.activeRecord.weight,
      broughtBy: props.er.pmr.activeRecord.broughtBy,
      howAdmitted: props.er.pmr.activeRecord.howAdmitted,
      historyObtainedFrom: props.er.pmr.activeRecord.historyObtainedFrom,
      chiefComplaints: props.er.pmr.activeRecord.chiefComplaints,
      clinicalImpressions: props.er.pmr.activeRecord.clinicalImpressions,
      allergyType: props.er.pmr.activeRecord.allergyType,
      allergies: props.er.pmr.activeRecord.allergies
    });

    if (_.get(props.er.pmr.activeRecord, 'allergies.food') && props.er.pmr.activeRecord.allergies.food != "") {

      const toDelete = new Set(props.er.pmr.activeRecord.allergies.food.split(','));
      const newArray = this.state.allergyFoodOptions.filter(obj => !toDelete.has(obj.value));

      props.er.pmr.activeRecord.allergies.food.split(',').map((item)=>{
        newArray.push({ value: item, label: item })
      })

      this.setState({allergyFoodOptions: newArray});
    }

    if (_.get(props.er.pmr.activeRecord, 'allergies.drug') && props.er.pmr.activeRecord.allergies.drug != "") {
      const toDelete = new Set(props.er.pmr.activeRecord.allergies.drug.split(','));
      const newArray = this.state.allergyDrugOptions.filter(obj => !toDelete.has(obj.value));

      props.er.pmr.activeRecord.allergies.drug.split(',').map((item)=>{
        newArray.push({ value: item, label: item })
      })

      this.setState({allergyDrugOptions: newArray})
    }

    if (_.get(props.er.pmr.activeRecord, 'allergies.others') && props.er.pmr.activeRecord.allergies.others != "") {
      props.er.pmr.activeRecord.allergies.others.split(',').map((item, i)=>{
        this.state.allergyOthersOptions.push({ value: item, label: item })
      })
    }
  }

  updatePMRField=(id)=>{
    return(data)=>{
      var payload = {};
      payload[id] = data;
      this.props.erActions.updatePMRInput(payload);
    }
  }

  updateAllergyTypeField=(id, data, mode)=>{
    const payload = [];
    if(_.get(this.props.er.pmr.activeRecord, id)){
      let recordsArray = this.props.er.pmr.activeRecord[id].split(',')
      recordsArray.map((item, i)=>{
        payload.push(item);
      });
    }
    if(mode == false){
      var index = payload.indexOf(data);
      payload.splice(index, 1);
    }else {
      payload.push(data);
    }
    this.props.erActions.updatePMRInput({[id]: String(payload)});
  }

  updateAllergyField=(id, data, mode)=>{
    const payload = [];
    if(_.get(this.props.er.pmr.activeRecord, 'allergies.'+id)){
      let recordsArray = this.props.er.pmr.activeRecord.allergies[id].split(',')
      recordsArray.map((item, i)=>{
        payload.push(item);
      });
    }
    if(mode == false){
      var index = payload.indexOf(data);
      payload.splice(index, 1);
    }else {
      payload.push(data);
    }
    this.props.erActions.updatePMRAllergyInput(id, String(payload));
  }

  renderRowAllergyType = (id, rowData) => {
    return (
      <View style={{ width: (30 * vw), height: 80}}>
        <Card>
          <ListItem style={{borderBottomWidth: 0}}>
            <Body>
              <Text>{rowData.label}</Text>
              <Switch
                onValueChange={(value)=>{this.updateAllergyTypeField(id, rowData.value, value)}}
                value={_.get(this.props.er.pmr.activeRecord, id) ? (
                  this.props.er.pmr.activeRecord.allergyType.includes(rowData.value) == true ? true : false) : false
                }
                />
            </Body>
          </ListItem>
        </Card>
      </View>
    )
  }

  renderRowAllergies = (id, rowData) => {
    return (
      <View style={{ width: (30 * vw), height: 80}}>
        <Card>
          <ListItem style={{borderBottomWidth: 0}}>
            <Body>
              <Text>{rowData.label}</Text>
              <Switch
                onValueChange={(value)=>{this.updateAllergyField(id, rowData.value, value)}}
                value={_.get(this.props.er.pmr.activeRecord, 'allergies.'+id) ? (
                  this.props.er.pmr.activeRecord.allergies[id].includes(rowData.value) == true ? true : false) : false
                }
                />
            </Body>
          </ListItem>
        </Card>
      </View>
    )
  }

  allergyOptionChange=(id, optionId)=>{
    return(value)=>{
      this.setState({ [id]: value });

      const lastTyped = value.charAt(value.length - 1);
      const parseWhen = [',', ';'];

      if (parseWhen.indexOf(lastTyped) > -1){
        const allergy = this.state[id];
        const allergyOptions = this.state[optionId];
        if(typeof _.find(allergyOptions, { value: allergy, label: allergy }) == 'undefined'){
          allergyOptions.push({ value: allergy, label: allergy });
          this.setState({ [id]: '', [optionId]: allergyOptions}, ()=>{
            this.updateAllergyField(id, allergy, true);
          });
        } else {
          this.setState({ [id]: ''});
        }
      }
    }
  }

  _scrollToInput (reactNode: any) {
    this.refs.content._root.scrollToFocusedInput(reactNode)
  }

  showSpinner=(boolVal) => {
    this.setState({ spinVisible: boolVal })
  }

  showResponse=(result, message)=>{
    this.showSpinner(false)

    if(result){
      Alert.alert(
        'Success',
        message,
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    }
    else {
      Alert.alert(
        'Error',
        message,
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    }
  }

  onPressSave=()=>{
    this.showSpinner(true);
    var pmrPayload = this.props.er.pmr.activeRecord;

    if(_.get(this.props.er.pmr.activeRecord, 'allergies.others')) {
      pmrPayload.allergies.others = pmrPayload.allergies.others.toString();
      pmrPayload.allergies = JSON.stringify(pmrPayload.allergies);
      this.props.erActions.updatePMR(this.props.er.pmr.activeRecord.id, pmrPayload, (result, message)=>{
        this.showResponse(result, message);
        if (result == true) {
          this.syncPropsToState(this.props);
        }
      });
    } else {
      pmrPayload.allergies = JSON.stringify(pmrPayload.allergies);
      this.props.erActions.updatePMR(this.props.er.pmr.activeRecord.id, pmrPayload, (result, message)=>{
        this.showResponse(result, message);
        if (result == true) {
          this.syncPropsToState(this.props);
        }
      });
    }
  }

  showPopover=(id)=>{
    return()=>{
      this.setState({[id]: true});
    }
  }

  closePopover=(id)=>{
    return()=>{
      this.setState({[id]: false});
    }
  }

  setSelectItem=(id)=>{
    return(data)=>{
      var payload = {};
      if (data.value.includes('Others')) {
        this.setState({ [id+'Others']: data.value });
        payload[id] = "";
        this.props.erActions.updatePMRInput(payload);
      } else {
        this.setState({ [id+'Others']: "" });
        payload[id] = data.value;
        this.props.erActions.updatePMRInput(payload);
      }
    }
  }

  render() {
    let bedOptions = _.map([{value: 'Sample Bed 1'}, {value: 'Sample Bed 2'}, {value: 'Sample Bed 3'}], (item, idx)=>{
      return {
        key: idx,
        label: item.value,
        value: item.value
      }
    });

    let broughtByOptions = _.map([{value: 'Private vehicle'}, {value: 'Ambulance'}, {value: 'Others specify'}], (item, idx)=>{
      return {
        key: idx,
        label: item.value,
        value: item.value
      }
    });

    let howAdmittedOptions = _.map([{value: 'Ambulant'}, {value: 'On wheelchair'}, {value: 'Stretcher'}], (item, idx)=>{
      return {
        key: idx,
        label: item.value,
        value: item.value
      }
    });

    let historyObtainedFromOptions = _.map([{value: 'Patient'}, {value: 'Others specify'}], (item, idx)=>{
      return {
        key: idx,
        label: item.value,
        value: item.value
      }
    });

    return (
      <Container>
        <Content padder>
          <View style={{flex: 1, flexDirection: 'row' }}>
            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>HEIGHT (centimeters)</Label>
              <Item regular>
                <Input
                  onChangeText={this.updatePMRField('height')}
                  value={this.props.er.pmr.activeRecord.height || ""}
                  placeholder={'Height'}
                  keyboardType={'numeric'}
                  editable={_.get(this.props.er.pmr.activeRecord, 'height') && _.get(this.state, 'height') ? false : true}
                  />
              </Item>
            </Item>
            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>WEIGHT (kilograms)</Label>
              <Item regular>
                <Input
                  onChangeText={this.updatePMRField('weight')}
                  value={this.props.er.pmr.activeRecord.weight || ""}
                  placeholder={'Weight'}
                  keyboardType={'numeric'}
                  editable={_.get(this.props.er.pmr.activeRecord, 'weight') && _.get(this.state, 'weight') ? false : true}
                  />
              </Item>
            </Item>
          </View>

          {_.get(this.state, 'height') && _.get(this.state, 'weight') ? (
            <View style={{flex: 1, flexDirection: 'row' }}>
              <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                <Label>BODY SURFACE AREA</Label>
                <Item regular>
                  <Input
                    value={(Math.sqrt((Number(this.props.er.pmr.activeRecord.height) * Number(this.props.er.pmr.activeRecord.weight)) / 3600, 2)).toFixed(2)}
                    editable={false}
                    />
                </Item>
              </Item>
              <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
                <Label>BODY MASS INDEX</Label>
                <Item regular>
                  <Input
                    value={(Number(this.props.er.pmr.activeRecord.weight) / Math.pow((Number(this.props.er.pmr.activeRecord.height) / 100), 2)).toFixed(2)}
                    editable={false}
                    />
                </Item>
              </Item>
            </View>
          ) : null }

          <View style={{flex: 1, flexDirection: 'row'}}>
            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>ER AREA BED #</Label>
              {_.get(this.props.er.pmr.activeRecord, 'bed') && _.get(this.state, 'bed') ? (
                <Item regular>
                  <Input
                    value={this.props.er.pmr.activeRecord.bed}
                    editable={false}
                    />
                </Item>
              ) : (
                <Item regular>
                  <ModalPicker
                    data={bedOptions}
                    initValue={'Bed #'}
                    onChange={this.setSelectItem('bed')}
                    style={{flex: 1}}>
                    <Input
                      onChangeText={this.updatePMRField('bed')}
                      value={this.props.er.pmr.activeRecord.bed || ""}
                      placeholder={'Bed #'}
                      editable={_.get(this.state, 'bedOthers') ? true : false}
                      />
                  </ModalPicker>
                </Item>
              )}
            </Item>

            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>BROUGHT BY</Label>
              {_.get(this.props.er.pmr.activeRecord, 'broughtBy') && _.get(this.state, 'broughtBy') ? (
                <Item regular>
                  <Input
                    value={this.props.er.pmr.activeRecord.broughtBy}
                    editable={false}
                    />
                </Item>
              ) : (
                <Item regular>
                  <ModalPicker
                    data={broughtByOptions}
                    initValue={'Brought by'}
                    onChange={this.setSelectItem('broughtBy')}
                    style={{flex: 1}}>
                    <Input
                      onChangeText={this.updatePMRField('broughtBy')}
                      value={this.props.er.pmr.activeRecord.broughtBy || ""}
                      placeholder={ _.get(this.state, 'broughtByOthers') ? 'Please specify others' : 'Brought by'}
                      editable={_.get(this.state, 'broughtByOthers') ? true : false}
                      />
                  </ModalPicker>
                </Item>
              )}
            </Item>
          </View>

          <View style={{flex: 1, flexDirection: 'row'}}>
            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>HOW ADMITTED</Label>
              {_.get(this.props.er.pmr.activeRecord, 'howAdmitted') && _.get(this.state, 'howAdmitted') ? (
                <Item regular>
                  <Input
                    value={this.props.er.pmr.activeRecord.howAdmitted}
                    editable={false}
                    />
                </Item>
              ) : (
                <Item regular>
                  <ModalPicker
                    data={howAdmittedOptions}
                    initValue={'How admitted'}
                    onChange={this.setSelectItem('howAdmitted')}
                    style={{flex: 1}}>
                    <Input
                      onChangeText={this.updatePMRField('howAdmitted')}
                      value={this.props.er.pmr.activeRecord.howAdmitted || ""}
                      placeholder={ _.get(this.state, 'howAdmittedOthers') ? 'Please specify others' : 'How admitted'}
                      editable={_.get(this.state, 'howAdmittedOthers') ? true : false}
                      />
                  </ModalPicker>
                </Item>
              )}
            </Item>

            <Item stackedLabel style={{flex: 0.5, borderBottomWidth: 0}}>
              <Label>HISTORY OBTAINED FROM</Label>
              {_.get(this.props.er.pmr.activeRecord, 'historyObtainedFrom') && _.get(this.state, 'historyObtainedFrom') ? (
                <Item regular>
                  <Input
                    value={this.props.er.pmr.activeRecord.historyObtainedFrom}
                    editable={false}
                    />
                </Item>
              ) : (
                <Item regular>
                  <ModalPicker
                    data={historyObtainedFromOptions}
                    initValue={'History obtained from'}
                    onChange={this.setSelectItem('historyObtainedFrom')}
                    style={{flex: 1}}>
                    <Input
                      onChangeText={this.updatePMRField('historyObtainedFrom')}
                      value={this.props.er.pmr.activeRecord.historyObtainedFrom || ""}
                      placeholder={ _.get(this.state, 'historyObtainedFromOthers') ? 'Please specify others' : 'History obtained from'}
                      editable={_.get(this.state, 'historyObtainedFromOthers') ? true : false}
                      />
                  </ModalPicker>
                </Item>
              )}
            </Item>
          </View>

          <Item stackedLabel style={{borderBottomWidth: 0}}>
            <Label>REASON(S) FOR ADMISSION</Label>
            <Item regular>
              <Input
                onChangeText={this.updatePMRField('chiefComplaints')}
                value={this.props.er.pmr.activeRecord.chiefComplaints || ""}
                placeholder={'Reason(s) for admission'}
                editable={_.get(this.props.er.pmr.activeRecord, 'chiefComplaints') && _.get(this.state, 'chiefComplaints') ? false : true}
                />
            </Item>
          </Item>

          <Item stackedLabel style={{borderBottomWidth: 0}}>
            <Label>CLINICAL IMPRESSIONS</Label>
            <Item regular>
              <Input
                onChangeText={this.updatePMRField('clinicalImpressions')}
                value={this.props.er.pmr.activeRecord.clinicalImpressions || ""}
                placeholder={'Clinical Impressions'}
                editable={_.get(this.props.er.pmr.activeRecord, 'clinicalImpressions') && _.get(this.state, 'clinicalImpressions') ? false : true}
                />
            </Item>
          </Item>

          <ListItem style={{borderBottomWidth: 0}}>
            <Title>ALLERIES</Title>
          </ListItem>

          <Item stackedLabel>
            <Label>TYPE OF ALLERGIES</Label>
            {_.get(this.props.er.pmr.activeRecord, 'allergyType') && _.get(this.state, 'allergyType') ? (
              <Text>{this.props.er.pmr.activeRecord.allergyType}</Text>
            ) : (
              <List contentContainerStyle={{justifyContent: 'space-around', flexDirection: 'row', flexWrap: 'wrap'}}
                dataArray={allergyTypeOptions}
                renderRow={(item) => this.renderRowAllergyType('allergyType', item)
                }>
              </List>
            )}
          </Item>

          {_.get(this.props.er.pmr.activeRecord, 'allergyType') ? (
            this.props.er.pmr.activeRecord.allergyType.includes('food') ? (
              <Item stackedLabel>
                <Label>FOOD</Label>
                {_.get(this.props.er.pmr.activeRecord, 'allergies.food') && _.get(this.state, 'allergies.food') ? (
                  <Text>{this.props.er.pmr.activeRecord.allergies.food}</Text>
                ) : (
                  <Item regular>
                    <Input
                      onChangeText={this.allergyOptionChange('food', 'allergyFoodOptions')}
                      value={this.state.food || ""}
                      placeholder={'Enter an allergy (comma separated)'}
                      />
                  </Item>
                )}
                <List contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
                  dataArray={this.state.allergyFoodOptions}
                  renderRow={(item) => this.renderRowAllergies('food', item)
                  }>
                </List>
              </Item>
            ) : null
          ) : null }

          {_.get(this.props.er.pmr.activeRecord, 'allergyType') ? (
            this.props.er.pmr.activeRecord.allergyType.includes('drug') ? (
              <Item stackedLabel>
                <Label>DRUG</Label>
                {_.get(this.props.er.pmr.activeRecord, 'allergies.drug') && _.get(this.state, 'allergies.drug') ? (
                  <Text>{this.props.er.pmr.activeRecord.allergies.drug}</Text>
                ) : (
                  <Item regular>
                    <Input
                      onChangeText={this.allergyOptionChange('drug', 'allergyDrugOptions')}
                      value={this.state.drug || ""}
                      placeholder={'Enter an allergy (comma separated)'}
                      />
                  </Item>
                )}
                <List contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
                  dataArray={this.state.allergyDrugOptions}
                  renderRow={(item) => this.renderRowAllergies('drug', item)
                  }>
                </List>
              </Item>
            ) : null
          ) : null }

          {_.get(this.props.er.pmr.activeRecord, 'allergyType') ? (
            this.props.er.pmr.activeRecord.allergyType.includes('others') ? (
              <Item stackedLabel>
                <Label>OTHERS</Label>
                {_.get(this.props.er.pmr.activeRecord, 'allergies.others') && _.get(this.state, 'allergies.others') ? (
                  <Text>{this.props.er.pmr.activeRecord.allergies.others}</Text>
                ) : (
                  <Item regular>
                    <Input
                      onChangeText={this.allergyOptionChange('others', 'allergyOthersOptions')}
                      value={this.state.others || ""}
                      placeholder={'Enter an allergy (comma separated)'}
                      />
                  </Item>
                )}
                <List contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
                  dataArray={this.state.allergyOthersOptions}
                  renderRow={(item) => this.renderRowAllergies('others', item)
                  }>
                </List>
              </Item>
            ) : null
          ) : null }

          {
            requireAuthentication((
              <View style={{flexDirection: 'row'}}>
                <Button success block onPress={this.onPressSave}
                  style={{flex: 1}}>
                  <Text>Save</Text>
                </Button>
              </View>
            ), this.props.auth.account, "ROLE_ER_NURSE")
          }
        </Content>
      </Container>
    );
  }
}
