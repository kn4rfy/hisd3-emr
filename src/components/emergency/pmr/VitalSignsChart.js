import React, { Component } from 'react';
import ReactNative, { Dimensions, View, Alert, Modal, ScrollView } from 'react-native';
import { Container, Header, Content, Body, Title, Item, List, ListItem, Button, Label, Text, Card, CardItem } from 'native-base';
import moment from 'moment';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class VitalSignsChart extends Component {
  constructor(props) {
    super(props);
  }

  renderRowAllergies = (rowData) => {
    return (
      <View>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{moment(rowData.dateTime).format('MMM DD, YYYY HHmm') + 'H'}</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.bloodPressureSystolic} / {rowData.records.bloodPressureDiastolic} mmHg</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.heartRate} bpm</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.pulseRate} bpm</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.respiratoryRate} bpm</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.temperature} °C</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.oxygenSaturation} %</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.painScore}</Text>
            </Body>
          </CardItem>
        </Card>
        <Card style={{height: 65}}>
          <CardItem>
            <Body>
              <Text>{rowData.records.glasgowComaScale}</Text>
            </Body>
          </CardItem>
        </Card>
      </View>
    )
  }

  render() {
    return (
      <View style={{flexDirection: 'row'}}>
        <View style={{flex:0.3}}>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>TIME & DATE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>BLOOD PRESSURE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>HEART RATE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>PULSE RATE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>RESPIRATORY RATE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>TEMPERATURE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>OXYGEN SATURATION</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>PAIN SCORE</Label>
              </Body>
            </CardItem>
          </Card>
          <Card style={{height: 65}}>
            <CardItem>
              <Body>
                <Label>GLASGOW COMA SCALE</Label>
              </Body>
            </CardItem>
          </Card>
        </View>
          <ScrollView horizontal={true} style={{flex: 0.7}}>
            {this.props.er.vitalSigns.records.length != 0 ? (
              <List
                dataArray={this.props.er.vitalSigns.records}
                renderRow={(item) => this.renderRowAllergies(item)}
                scrollEnabled={false}>
              </List>
            ) : (
              <View style={{flex:1,justifyContent: 'center', alignItems: 'center'}}>
                <Title>NO DATA</Title>
              </View>
            )
          }
        </ScrollView>
    </View>
  );
}
}
