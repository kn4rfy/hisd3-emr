import React, { Component } from 'react';
import ReactNative, { Dimensions, View, Alert, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card, Footer, FooterTab, Tab, Tabs } from 'native-base';
import _ from 'lodash';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {requireAuthentication} from '../../../utils/RoleComponent';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

const pastMedicalHistoryOptions = [
  { value: 'Neurological - CVD', label: 'Neurological - CVD' },
  { value: 'Neurological - Seizure disorder', label: 'Neurological - Seizure disorder' },
  { value: 'Pulmonary - Asthma', label: 'Pulmonary - Asthma' },
  { value: 'Pulmonary - COPD', label: 'Pulmonary - COPD' },
  { value: 'Hypertension', label: 'Hypertension' },
  { value: 'Cardiac disease - ACS', label: 'Cardiac disease - ACS' },
  { value: 'Cardiac disease - CHF', label: 'Cardiac disease - CHF' },
  { value: 'Cardiac disease - RHD', label: 'Cardiac disease - RHD' },
  { value: 'Diabetes - Insulin dependent', label: 'Diabetes - Insulin dependent' },
  { value: 'Diabetes - Oral hypoglycemic', label: 'Diabetes - Oral hypoglycemic' }
];

export default class MedicalHistories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      presentMedicalHistories: "",
      presentMedicalHistoryOptions: [],
      page1:1,
      page2:0,
      positionpage2:1000
    }
  }

  _scrollToInput (reactNode: any) {
    this.refs.scroll.scrollToFocusedInput(reactNode)
  }

  _scrollToInput2 (reactNode: any) {
    this.refs.scroll2.scrollToFocusedInput(reactNode)
  }
  _scrollToInput3 (reactNode: any) {
    this.refs.scroll3.scrollToFocusedInput(reactNode)
  }

  updatePastMedicalHistoryField=(rowID, data, mode)=>{
    const payload = this.props.er.pastMedicalHistories.activeRecord.records;
    if(mode == false){
      payload[rowID] = null;
    }else {
      payload[rowID] = {value: data, date: "", notes: ""};
    }
    this.props.erActions.updatePastMedicalHistoryInput({records: payload});
  }

  updatePastMedicalHistoryFieldInput=(rowID, id)=>{
    return(data)=>{
      const payload = this.props.er.pastMedicalHistories.activeRecord.records;
      payload[rowID][id] = data;
      this.props.erActions.updatePastMedicalHistoryInput({records: payload});
    }
  }

  renderRowPastMedicalHistory = (rowData, sectionID, rowID) => {
    return (
      <ListItem>
        {_.get(this.props.er.pastMedicalHistories.activeRecord, 'id') ? (
          rowData != null ? (
            <View style={{ margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
              <Text>{rowData.value}</Text>
              <Text>Year: {rowData.date}</Text>
              <View style={{ flex: 1 }}>
                <Text>Notes: {rowData.notes}</Text>
              </View>
            </View>
          ) : null
        ) : (
            <View style={{flexDirection: 'row'}}>
              <View style={{flex:0.8}}>
                <Text>{rowData.label}</Text>
              </View>
              <View style={{flex:0.2}}>
                <Switch
                  onValueChange={(value)=>{this.updatePastMedicalHistoryField(rowID, rowData.value, value)}}
                  value={_.get(this.props.er.pastMedicalHistories.activeRecord, 'records['+rowID+'].value') ?
                    (this.props.er.pastMedicalHistories.activeRecord.records[rowID].value.includes(rowData.value) == true ? true : false) : false}
                    />
                </View>
              </View>
          )
        }
      </ListItem>
    )
  }

  renderRowPastMedicalHistory2 = (rowData, sectionID, rowID) => {
    return (
      <View>
        {_.get(this.props.er.pastMedicalHistories.activeRecord, 'id') ? (
          rowData != null ? (
            <View style={{ margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
              <Text>{rowData.value}</Text>
              <Text>Year: {rowData.date}</Text>
              <View style={{ flex: 1 }}>
                <Text>Notes: {rowData.notes}</Text>
              </View>
            </View>
          ) : null
        ) : (
          <View>
            {_.get(this.props.er.pastMedicalHistories.activeRecord, 'records['+rowID+'].value') ? (
              this.props.er.pastMedicalHistories.activeRecord.records[rowID].value.includes(rowData.value) == true ? (
                <Item stackedLabel style={{borderWidth: 0}}>
                  <Label>{rowData.label}</Label>
                  <Item regular>
                    <Input
                      onChangeText={this.updatePastMedicalHistoryFieldInput(rowID, 'date')}
                      value={this.props.er.pastMedicalHistories.activeRecord.records[rowID].date || ""}
                      placeholder={'Year'}
                      />
                  </Item>
                  <Item regular>
                    <Input
                      onChangeText={this.updatePastMedicalHistoryFieldInput(rowID, 'notes')}
                      value={this.props.er.pastMedicalHistories.activeRecord.records[rowID].notes}
                      placeholder={'Other notes'}
                      />
                  </Item>
                </Item>
              ) : null) : null}
            </View>
          )
        }
      </View>
    )
  }

  presentMedicalHistoryOptionsChange=(id, optionId)=>{
    return(value)=>{
      this.setState({ [id]: value });

      const lastTyped = value.charAt(value.length - 1);
      const parseWhen = [',', ';'];

      if (parseWhen.indexOf(lastTyped) > -1){
        const medicalHistory = this.state[id];
        const medicalHistoryOptions = this.state[optionId];
        if(typeof _.find(medicalHistoryOptions, { value: medicalHistory, label: medicalHistory }) == 'undefined'){
          medicalHistoryOptions.push({ value: medicalHistory, label: medicalHistory });
          this.setState({ [id]: '', [optionId]: medicalHistoryOptions});
        } else {
          this.setState({ [id]: ''});
        }
      }
    }
  }

  updatePresentMedicalHistoryField=(rowID, data, mode)=>{
    const payload = this.props.er.presentMedicalHistories.activeRecord.records;
    if(mode == false){
      payload[rowID] = null;
    }else {
      payload[rowID] = {value: data, chiefComplaints: "", details: ""};
    }
    this.props.erActions.updatePresentMedicalHistoryInput({records: payload});
  }

  updatePresentMedicalHistoryFieldInput=(rowID, id)=>{
    return(data)=>{
      const payload = this.props.er.presentMedicalHistories.activeRecord.records;
      payload[rowID][id] = data;
      this.props.erActions.updatePresentMedicalHistoryInput({records: payload});
    }
  }

  renderRowPresentMedicalHistory = (rowData, sectionID, rowID) => {
    return (
      <ListItem>
        {_.get(this.props.er.presentMedicalHistories.activeRecord, 'id') ? (
          rowData != null ? (
            <View style={{ margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
              <Text>{rowData.value}</Text>
              <Text>Chief complaints: {rowData.chiefComplaints}</Text>
              <View style={{ flex: 1 }}>
                <Text>Details: {rowData.details}</Text>
              </View>
            </View>
          ) : null
        ) : (
            <View style={{flexDirection: 'row'}}>
              <View style={{flex:0.2}}>
                <Switch
                  onValueChange={(value)=>{this.updatePresentMedicalHistoryField(rowID, rowData.value, value)}}
                  value={_.get(this.props.er.presentMedicalHistories.activeRecord, 'records['+rowID+'].value') ?
                    (this.props.er.presentMedicalHistories.activeRecord.records[rowID].value.includes(rowData.value) == true ? true : false) : false}
                    />
                </View>
                <View style={{flex:0.8}}>
                  <Text>{rowData.label}</Text>
                </View>
              </View>
          )
        }
      </ListItem>
    )
  }

  renderRowPresentMedicalHistory2 = (rowData, sectionID, rowID) => {
    return (
      <View>
        {_.get(this.props.er.presentMedicalHistories.activeRecord, 'id') ? (
          rowData != null ? (
            <View style={{ margin: 10, padding: 10, backgroundColor: 'white', borderWidth: 1, borderRadius: 4, borderColor: 'lightgray' }}>
              <Text>{rowData.value}</Text>
              <Text>Chief complaints: {rowData.chiefComplaints}</Text>
              <View style={{ flex: 1 }}>
                <Text>Details: {rowData.details}</Text>
              </View>
            </View>
          ) : null
        ) : _.get(this.props.er.presentMedicalHistories.activeRecord, 'records['+rowID+'].value') ? (
          this.props.er.presentMedicalHistories.activeRecord.records[rowID].value.includes(rowData.value) == true ? (
            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>{rowData.label}</Label>
              <Item regular>
                <Input
                  onChangeText={this.updatePresentMedicalHistoryFieldInput(rowID, 'chiefComplaints')}
                  value={this.props.er.presentMedicalHistories.activeRecord.records[rowID].chiefComplaints || ""}
                  placeholder={'Chief complaints'}
                  />
              </Item>
              <Item regular>
                <Input
                  onChangeText={this.updatePresentMedicalHistoryFieldInput(rowID, 'details')}
                  value={this.props.er.presentMedicalHistories.activeRecord.records[rowID].details}
                  placeholder={'Details'}
                  />
              </Item>
            </Item>
          ) : null) : null
        }
      </View>
    )
  }

  onPressSaveConfirm=()=>{
    if(_.get(this.props.er.pmr.activeRecord, 'id')) {
      Alert.alert(
        'Confirm',
        'Do you really want to complete the input and save?',
        [{
          text: 'Yes',
          onPress: this.onPressSave,
        },
        {
          text: 'No',
          onPress: () => {},
        }]
      );

    } else {
      Alert.alert(
        'Error',
        'Patient Medical Record is not yet created! Please create one before proceeding.',
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    }
  }

  onPressSave=()=>{
    let pastMedicalHistoriesPayload = {};
    let presentMedicalHistoriesPayload = {};

    if(_.get(this.props.er.pastMedicalHistories.activeRecord, 'id')) {
      Alert.alert(
        'Warning',
        'Past Medical History has already been saved and cannot be updated',
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    } else {
      if (this.props.er.pastMedicalHistories.activeRecord.records.length != 0) {
        pastMedicalHistoriesPayload.erPmr = this.props.er.pmr.activeRecord._links.self.href;
        pastMedicalHistoriesPayload.records = JSON.stringify(this.props.er.pastMedicalHistories.activeRecord.records);
        this.props.erActions.addPastMedicalHistory(pastMedicalHistoriesPayload, this.props.patientId);
      } else {
        return;
      }
    }

    if(_.get(this.props.er.presentMedicalHistories.activeRecord, 'id')) {
      Alert.alert(
        'Warning',
        'Present Medical History has already been saved and cannot be updated',
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    } else {
      if (this.props.er.presentMedicalHistories.activeRecord.records.length != 0) {
        presentMedicalHistoriesPayload.erPmr = this.props.er.pmr.activeRecord._links.self.href;
        presentMedicalHistoriesPayload.records = JSON.stringify(this.props.er.presentMedicalHistories.activeRecord.records);
        this.props.erActions.addPresentMedicalHistory(presentMedicalHistoriesPayload, this.props.patientId);
      } else {
        return;
      }
    }
  }

  onClickPage = (page) =>{
    return () =>{
      if (page == 1 ) {
        this.setState({page1:1,page2:0,positionpage2:1000})
      }
      if (page == 2) {
        this.setState({page1:0,page2:1,positionpage2:50})
      }
    }
  }

  render() {
    return (
      <Container>
        <Tabs>
          <Tab heading="PAST MEDICAL HISTORY">
            <Content padder>
              <View style={{flexDirection: 'row'}}>
                <View style={{ flex:0.4 }}>
                  {_.get(this.props.er.pastMedicalHistories.activeRecord, 'id') ? (
                    <List
                      dataArray={this.props.er.pastMedicalHistories.activeRecord.records}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPastMedicalHistory(rowData, sectionID, rowID)
                      }>
                    </List>
                  ) : (
                    <List
                      dataArray={pastMedicalHistoryOptions}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPastMedicalHistory(rowData, sectionID, rowID)
                      }>
                    </List>
                  )}
                </View>

                <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

                <View style={{ flex: 0.6 }}>
                  {_.get(this.props.er.pastMedicalHistories.activeRecord, 'id') ? (
                    <List
                      dataArray={this.props.er.pastMedicalHistories.activeRecord.records}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPastMedicalHistory2(rowData, sectionID, rowID)}>
                    </List>
                  ) : (
                    <List
                      dataArray={pastMedicalHistoryOptions}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPastMedicalHistory2(rowData, sectionID, rowID)}>
                    </List>
                  )}
                </View>
              </View>
            </Content>
          </Tab>
          <Tab heading="PRESENT MEDICAL HISTORY">
            <Content padder>
              <View style={{flexDirection:'row'}}>
                <View style={{flex:0.4}}>
                  {_.get(this.props.er.presentMedicalHistories.activeRecord, 'id') ? (
                    <List
                      dataArray={this.props.er.presentMedicalHistories.activeRecord.records}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPresentMedicalHistory(rowData, sectionID, rowID)}>
                    </List>
                  ) : (
                    <View>
                      <Item stackedLabel style={{borderBottomWidth: 0}}>
                        <Label>PRESENT MEDICAL HISTORY</Label>
                        <Item regular>
                          <Input
                            onChangeText={this.presentMedicalHistoryOptionsChange('presentMedicalHistories', 'presentMedicalHistoryOptions')}
                            value={this.state.presentMedicalHistories || ""}
                            placeholder={'Enter a medical history (comma separated)'}
                            />
                        </Item>
                      </Item>
                      <List
                        dataArray={this.state.presentMedicalHistoryOptions}
                        renderRow={(rowData, sectionID, rowID) => this.renderRowPresentMedicalHistory(rowData, sectionID, rowID)}>
                      </List>
                    </View>
                  )}
                </View>

                <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

                <View style={{flex:0.6}}>
                  {_.get(this.props.er.presentMedicalHistories.activeRecord, 'id') ? (
                    <List
                      dataArray={this.props.er.presentMedicalHistories.activeRecord.records}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPresentMedicalHistory2(rowData, sectionID, rowID)}>
                    </List>
                  ) : (
                    <List
                      dataArray={this.state.presentMedicalHistoryOptions}
                      renderRow={(rowData, sectionID, rowID) => this.renderRowPresentMedicalHistory2(rowData, sectionID, rowID)}>
                    </List>
                  )}
                </View>
              </View>
            </Content>
          </Tab>
        </Tabs>
        <Footer>
          <FooterTab>
            {_.get(this.props.er.presentMedicalHistories.activeRecord, 'id') && _.get(this.props.er.presentMedicalHistories.activeRecord, 'id') ?
              null : requireAuthentication((
                <Button success full onPress={this.onPressSaveConfirm}
                  style={{flex: 1}}>
                  <Text>Save</Text>
                </Button>
              ), this.props.auth.account, "ROLE_ER_NURSE")
            }
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
