import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  TouchableOpacity,
  Alert
} from 'react-native'


import Button from 'apsl-react-native-button'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import * as style from '../../../styles/styles'
import * as HISstyle from '../../../styles/HISstyle'
import moment from 'moment'
import * as Config from '../../../config/Config';
import { Hideo } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/EvilIcons';


//actions
import * as erActions from '../../../actions/erActions';
import * as patientActions from '../../../actions/patientactions';

//redux
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Actions, Scene,ActionConst } from 'react-native-router-flux'

import TitleBar from '../../general/TitleBar'

 class IapDetails extends Component {
    constructor(props) {
        super(props);

        this.state={
          description:'',
          quantity_amount:'',
          condition:'',
          isValidDecs: "true",
          isValidQty: "true"
        }
    }

    componentDidMount(){
        if (this.props.iapdetailsID) {
            this.props.erActions.getIapDetials(this.props.iapdetailsID,this.props.patientId)
        }else{
            this.props.erActions.getIapDetailsActive('clear',this.props.patientId)
        }

        var payload = {};
            payload['iap'] = this.props.er.iap.activeRecord.id;
            this.props.erActions.updateIapDetails(payload);


    }


    onChangeDesc = (value) =>{


      var payload = {};
          payload['description'] = value.toUpperCase().trimLeft();

        this.props.erActions.updateIapDetails(payload);

        // const desclength = this.props.er.iapDetails.activeRecord.description;
        // desclength  ? this.setState({isValidDecs:"error"}) : this.setState({isValidDecs:"success"});


    }

    onChangeQty = (value) =>{

      var payload = {};
          payload['quantity_amount'] = value;

        this.props.erActions.updateIapDetails(payload);

        // if (value == 0) {
        //   this.setState({isValidQty:"error"})
        // }else{
        //   const qtyLength = this.props.er.iapDetails.activeRecord.quantity_amount;
        //   qtyLength  ? this.setState({isValidQty:"error"}) : this.setState({isValidQty:"success"});
        //
        // }
    }

    onChangeCondition = (value) =>{
      var payload = {};
          payload['condition'] = value.toUpperCase().trimLeft();

        this.props.erActions.updateIapDetails(payload);

    }

    errorSubmit = () =>{
      Alert.alert(
        'Error!',
        `Some input field is empty`,
        [
          {text: 'OK', onPress: ()=>{}}
        ]
      )
    }
    successSubmit = () =>{
      Alert.alert(
        'Success!',
        `Data Submit Successfully`,
        [
          {text: 'OK', onPress: ()=>{
             Actions.iap({patientId:this.props.patientId, type: ActionConst.POP_AND_REPLACE});
          }}
        ]
      )
    }

    addList = () =>{
      if (this.state.isValidQty == "error") {
          this.errorSubmit();
          //this.props.dialogActions.addNotification("Error", "Some input field is empty" , "error");
        }else{
                if(this.state.isValidDecs == "error"){
                  this.errorSubmit();
                  //this.onAddIap(this.state.descriptionSelect,this.state.quantity_amount,this.state.condition);
                }else{
                  this.onAddIap(this.props.er.iapDetails.activeRecord.description,this.props.er.iapDetails.activeRecord.quantity_amount,this.props.er.iapDetails.activeRecord.condition);
                  //this.onAddIap(this.state.description,this.state.quantity_amount,this.state.condition);
                }
        }

    }

    onAddIap = (description,quantity_amount,condition) =>{
      const id = this.props.er.iap.activeRecord.id;
      const iapdataArray = {iap:id,description:description,quantity_amount:quantity_amount,condition:condition};

      if(this.props.er.iap.activeRecord.id){
          //this.props.erActions.upsertIapDetails(iapdataArray,this.props.patientId,this.successSubmit);
          const getPatientIapApi = "/restapi/patients/" + this.props.patientId;


          this.props.erActions.upsertIapDetails(this.props.er.iapDetails.activeRecord, getPatientIapApi,this.successSubmit);
      }else{
          this.props.erActions.upsert(this.props.er.iap.activeRecord,iapdataArray,this.successSubmit);
      }

      // this.setState({
      //     description:'',
      //     quantity_amount:'',
      //     condition:'',
      //     descriptionSelect:'',
      //     isValidDecs:'error',
      //     isValidQty:'error',
      //     isValidDecsSelect:'error'
      //
      // })
    }




    render() {



        return (
          <ScrollView style={{ marginTop: 65 }}>
          <View>
              <View style={HISstyle.putContainerPadding}>
                    <View style={{ flex: 1 }}>
                      <Text style={HISstyle.formLabel}>ITEM DESCRIPTION</Text>
                      <Hideo
                        onChangeText={(value)=>this.onChangeDesc(value)}
                        value={this.props.er.iapDetails.activeRecord.description || ''}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        placeholder={'Item Description'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        style={{paddingRight: 5}}
                        />
                    </View>
                    <View style={{ flex: 1, marginTop:10 }}>
                      <Text style={HISstyle.formLabel}>QUANTITY/AMOUNT</Text>
                      <Hideo
                        onChangeText={(value)=>this.onChangeQty(value)}
                        value={this.props.er.iapDetails.activeRecord.quantity_amount ? String(this.props.er.iapDetails.activeRecord.quantity_amount) : ''}
                        keyboardType={'numeric'}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        placeholder={'Quantity/Amount'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        style={{paddingRight: 5}}
                        />
                    </View>
                    <View style={{ flex: 1, marginTop:10 }}>
                      <Text style={HISstyle.formLabel}>CONDITION</Text>
                      <Hideo
                        onChangeText={(value)=>this.onChangeCondition(value)}
                        value={this.props.er.iapDetails.activeRecord.condition || ''}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        placeholder={'Condtion'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        style={{paddingRight: 5}}
                        />
                    </View>
                    <View style={{ flexDirection: 'row',marginTop:10 ,marginBottom:10}}>
                      <View style={{flex:0.6}}></View>
                      <View style={{flex:0.4}}>
                        <Button textStyle={HISstyle.saveText} style={[HISstyle.saveButtonStyle]} onPress={this.addList} >
                          Save
                        </Button>
                      </View>

                    </View>
              </View>

            </View>
          </ScrollView>
        );
    }
}
function mapStateToProps(state){
    return {
        patients: state.patients,
        er: state.er
    }
}

function mapDispatchToProps(dispatch) {
    return {
        patientActions: bindActionCreators(patientActions, dispatch),
        erActions: bindActionCreators(erActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(IapDetails);
