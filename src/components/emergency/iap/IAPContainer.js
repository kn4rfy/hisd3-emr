import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  Switch
} from 'react-native'

//npm
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';

//add components
import PatientIapForm from './PatientIapForm';
import IapDetails from './IapDetails'
import PatientInfo from '../../PatientInfo'

//actions
import * as erActions from '../../../actions/erActions';
import * as patientActions from '../../../actions/patientactions';

//redux
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Actions, Scene } from 'react-native-router-flux'

 class IAPContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

          <View style={{flex:1}}>
            <View style={{marginTop:64}}>
              <PatientInfo {...this.props} />
            </View>

            <ScrollableTabView locked={true} tabBarPosition="bottom" renderTabBar={() => <DefaultTabBar />}>


            <PatientIapForm
                tabLabel="Personal Effects"
                key="PatientIapForm"
                {...this.props}  />
            </ScrollableTabView>
          </View>
        );
    }
}
function mapStateToProps(state){
    return {
        patients: state.patients,
        er: state.er
    }
}

function mapDispatchToProps(dispatch) {
    return {
        patientActions: bindActionCreators(patientActions, dispatch),
        erActions: bindActionCreators(erActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(IAPContainer);
