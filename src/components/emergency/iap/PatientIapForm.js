import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  TouchableOpacity,
  Alert
} from 'react-native'


import Button from 'apsl-react-native-button'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import { Actions } from 'react-native-router-flux';
import { Hideo } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/EvilIcons';
import moment from 'moment'


import * as style from '../../../styles/styles'
import * as HISstyle from '../../../styles/HISstyle'
import * as Config from '../../../config/Config';
import TitleBar from '../../general/TitleBar'

import SignaturePreviewModal from '../../general/SignaturePreviewModal'
import PatientInfo from '../../PatientInfo';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);
class PatientIapForm extends Component {
  constructor(props) {
    super(props);

    this.state={
      claiming_person_signature_preview:false
    }
  }

  openSignatureModalPreview = (name) => {

    this.setState({
      claiming_person_signature_preview: true
    })
  }

  closeSignatureModalPreview = (name) => {
    this.setState({
      claiming_person_signature_preview: false
    })
  }

  componentDidMount(){
    if(this.props.patientId)  // when called  /restapi/path/:id
    {
      const getPatientIapApi = "/restapi/patients/" + this.props.patientId;
      this.props.erActions.getIapList(getPatientIapApi);
    }else{
      this.props.erActions.getIapDetailsActive('clear',this.props.patientId);
    }
  }

  onFormPage = (id) =>{
    Actions.iapdetails({patientId:this.props.patientId});
  }

  editIap = (index) =>{
    return()=>{
      if (this.props.er.iap.activeRecord.status  == 'PENDING' ) {
        Actions.iapdetails({iapdetailsID:index,patientId:this.props.patientId});
      }
      if (this.props.er.iap.activeRecord.status  == 'STORED' ) {
        Actions.iapdetails({iapdetailsID:index,patientId:this.props.patientId});

      }
      if (this.props.er.iap.activeRecord.status == 'CLAIMED') {

      }
    }
  }

  onLongPressRemove = (id) =>{
    return ()=>{
      if (this.props.er.iap.activeRecord.status  == 'PENDING' ) {
        this.onDelete(id)
      }
      if (this.props.er.iap.activeRecord.status  == 'STORED' ) {
        Actions.iapdetails({iapdetailsID:index,patientId:this.props.patientId});
        this.onDelete(id)
      }
      if (this.props.er.iap.activeRecord.status == 'CLAIMED') {

      }
    }
  }

  onDelete = (id)=>{
    Alert.alert(
      'Confirmation!',
      `Please Confirm to Remove`,
      [
        {text: 'Cancel', onPress: ()=>{

        }},
        {text: 'Remove', onPress: ()=>{
          const getPatientIapApi = "/restapi/patients/" + this.props.patientId;
          this.props.erActions.deleteIapDetails(id,getPatientIapApi);
        }}
      ]
    )
  }

  successSubmit = () =>{
    Alert.alert(
      'Success!',
      `Stored Successfully`,
      [
        {text: 'OK', onPress: ()=>{

        }}
      ]
    )
  }

  onClaimed = () =>{
    Actions.iapclaimed({patientId:this.props.patientId});
  }

  onConfirm = () =>{
    this.props.erActions.upsert(this.props.er.iap.activeRecord,this.props.er.iap.activeRecord.status,this.successSubmit);
  }

  renderRow=(rowData,key)=>{
    return(
      <TouchableOpacity onPress={this.editIap(rowData.id)} onLongPress={this.onLongPressRemove(rowData.id)}>
        <View style={[HISstyle.lineBottom, HISstyle.patientListItem,HISstyle.putShadow]}>
          <View style={{ flex: 0.3  }}>
            <Text style={{ fontSize: 18, color: '#2d2d2d', marginBottom: 5 }}>{rowData.description}</Text>
          </View>
          <View style={{ flex: 0.3  }}>
            <Text style={{ fontSize: 18, color: '#2d2d2d', marginBottom: 5 }}>{rowData.quantity_amount}</Text>
          </View>
          <View style={{ flex: 0.3  }}>
            <Text style={{ fontSize: 18, color: '#2d2d2d', marginBottom: 5 }}>{rowData.condition}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    let inventory_status = (<Text style={[HISstyle.headTitle,{ color: '#2d2d2d', marginBottom: 5 }]}>N/A</Text>);
    let date_time_stored = "N/A";
    let date_time_claimed;
    let receive_and_stored = "N/A";
    let claimedby = "N/A";
    let fomrSubmitData;
    let formIapDetails;
    let previewbtn;

    if (this.props.er.iap.activePatient.activePdsc != null ) {
      formIapDetails =  (
        <View style={{ flex: 0.3 }}>
          <Button textStyle={HISstyle.saveText} style={[HISstyle.saveButtonStyle]} onPress={this.onFormPage} >
            Add New
          </Button>
        </View>
      );
    }else{
      formIapDetails = (  <View>
        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: 28, marginLeft: 10, color: style.darkBlueColor,textAlign:'center' }}>Create the Patient Data Sheet Consent First!</Text>
        </View>
      </View>
    );
  }

  if (this.props.er.iap.activeRecord.status == 'PENDING' ) {
    inventory_status = (<Text style={[HISstyle.headTitle,{ color: '#c79121', marginBottom: 5 }]}>PENDING</Text>);
    date_time_claimed = "N/A";
    fomrSubmitData = (

      <View style={{ flex: 0.3 }}>
        <Button textStyle={HISstyle.warningText} style={[HISstyle.warningButton]} onPress={this.onConfirm} >
          STORE INVENTORY
        </Button>
      </View>
    );
  }
  if ( this.props.er.iap.activeRecord.status == 'STORED' ) {
    inventory_status = (<Text style={[HISstyle.headTitle,{color: '#3b9ff3', marginBottom: 5 }]}>STORED</Text>);
    date_time_stored = moment(this.props.er.iap.activeRecord.date_stored).format("MM-DD-YYYY H:mm  ");
    date_time_claimed = "NOT YET CLAIMED";
    fomrSubmitData = (
      <View style={{ flex: 0.3 }}>
        <Button textStyle={HISstyle.saveText} style={[HISstyle.saveButtonStyle]} onPress={this.onClaimed} >
          CLAIM
        </Button>
      </View>
    );
  }
  if ( this.props.er.iap.activeRecord.status == 'CLAIMED' ) {
    inventory_status = (<Text style={[HISstyle.headTitle,{color: '#c79121', marginBottom: 5 }]}>CLAIMED</Text>);
    date_time_stored = moment(this.props.er.iap.activeRecord.date_stored).format("MM-DD-YYYY H:mm  ");
    date_time_claimed = moment(this.props.er.iap.activeRecord.date_claimed).format("MM-DD-YYYY H:mm  ");
    receive_and_stored = this.props.er.iap.activeRecord.nurseFullname;
    claimedby = this.props.er.iap.activeRecord.claiming_person_fullname;
    formIapDetails = null;
    fomrSubmitData = null;
    previewbtn = (
      <Button className="primary" textStyle={HISstyle.warningText} style={[HISstyle.warningButton]} onPress={this.openSignatureModalPreview} >
        Preview Signature
      </Button>);
    }

    let claimingSignature = null;

    if(this.props.er.iap.activeRecord.claiming_person_signature != null){
      claimingSignature = `data:image/png;base64,`+this.props.er.iap.activeRecord.claiming_person_signature ;
    }else{
      if(this.props.er.iap.activeRecord.hasClaiming){
        claimingSignature = Config.getHost()+`/restapi/iap/claiming_person_signature/${this.props.er.iap.activeRecord.id}`;
      }
    }

    const defImage = this.props.patients.patientInfo.gender == 'Male' ? (require('../../../assets/boy.png')) : (require('../../../assets/girl.png'));

    return (
      <ScrollView>

        <SignaturePreviewModal
          id='claiming_person_signature'
          imgSrc={claimingSignature}
          previewSignOpen={this.state.claiming_person_signature_preview}
          closeSignaturePreviewModal={this.closeSignatureModalPreview}
          openSignaturePreviewModal={this.openSignatureModalPreview}
          openSignatureModal={this.openSignatureModalPreview}
          hideUpdateBtn
          />
        <View  minHeight={100*vh-64-64}>

          <View style={HISstyle.putPadding}>
            <View style={[HISstyle.formGroup, {flex: 1, flexDirection: 'row' }]}>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{marginBottom: 5 }]}>INVENTORY STATUS</Text>
                {inventory_status}
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>BAG NUMBER</Text>
                <Text style={[HISstyle.headTitle,{ color: '#3b9ff3', marginBottom: 5 }]}>{this.props.er.iap.activeRecord.safetybagnumber ?  this.props.er.iap.activeRecord.safetybagnumber : "N/A"}</Text>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>DATE TIME STORED</Text>
                <Text style={[HISstyle.headTitle,{marginBottom: 5 }]}>{date_time_stored}</Text>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>DATE TIME CLAIMED</Text>
                <Text style={{ fontSize: 18, color: '#2d2d2d', marginBottom: 5 }}>{date_time_claimed}</Text>
              </View>
            </View>
            <View style={[HISstyle.putPadding, {flex: 1, flexDirection: 'row' }]}>
              <View style={{ flex: 0.3  }}>
              </View>
              <View style={{ flex: 0.3  }}>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>RECEIVED & STORED BY</Text>
                <Text style={[HISstyle.headTitle,{marginBottom: 5 }]}>{receive_and_stored}</Text>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>CLAIMED BY</Text>
                <Text style={[HISstyle.headTitle,{marginBottom: 5 }]}>{claimedby}</Text>
                {previewbtn}
              </View>
            </View>
          </View>

          <View style={HISstyle.putPadding}>
            <View style={{ flexDirection: 'row', paddingTop: 10, paddingBottom: 20, paddingLeft: 10, backgroundColor: '#fafafa', marginBottom: 10 }}>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>ITEM DESCRIPTION</Text>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>QUANTITY/AMOUNT</Text>
              </View>
              <View style={{ flex: 0.3  }}>
                <Text style={[HISstyle.titleHead,{ marginBottom: 5 }]}>CONDITION</Text>
              </View>
            </View>
            <ListView
              style={{
                flex: 1
              }}
              dataSource={ds.cloneWithRows(this.props.er.iapDetails.records)}
              renderRow={this.renderRow}
              enableEmptySections
              />

            <View style={{flexDirection:'row',marginTop:10}}>
              {formIapDetails}
              <View style={{flex:0.4}}></View>
              {fomrSubmitData}
            </View>

          </View>
        </View>
      </ScrollView>
    );
  }
}
export default PatientIapForm;
