import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  TouchableOpacity,
  Alert
} from 'react-native'


import Button from 'apsl-react-native-button'
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import * as style from '../../../styles/styles'
import * as HISstyle from '../../../styles/HISstyle'
import moment from 'moment'
import * as Config from '../../../config/Config';
import { Hideo } from 'react-native-textinput-effects';
import Icon from 'react-native-vector-icons/EvilIcons';

import TitleBar from '../../general/TitleBar'


//signature
import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal'


//actions
import * as erActions from '../../../actions/erActions';
import * as patientActions from '../../../actions/patientactions';

//redux
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Actions, Scene,ActionConst } from 'react-native-router-flux'


const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);


 class IapClaimed extends Component {
    constructor(props) {
        super(props);

        this.state={
          claiming_person_signature:false,
          claiming_person_signaturePreview:false
        }


    }

    componentDidMount(){
    }

    // ============  sign signaturemodal
    openSignatureModal = (name) => {

      this.setState({
        claiming_person_signature: true
      })
    }
    closeSignatureModal = (name) => {
      this.setState({
        claiming_person_signature: false
      })
    }

    //=============  preview SignatureModal
    openSignatureModalPreview = (name) => {

      this.setState({
        claiming_person_signaturePreview: true
      })
    }

    closeSignatureModalPreview = (name) => {
      this.setState({
        claiming_person_signaturePreview: false
      })
    }




    updateFieldSignature = (name,value) =>{
      var payload = {};
          payload[name] = value;

        this.props.erActions.updateIap(payload);


    }


    updateField = (value) =>{
      var payload = {};
          payload['claiming_person_fullname'] = value;

        this.props.erActions.updateIap(payload);

    }

    successSubmit = () =>{
      Alert.alert(
        'Success!',
        `Claimed Successfully`,
        [
          {text: 'OK', onPress: ()=>{
            Actions.iap({patientId:this.props.patientId, type: ActionConst.POP_AND_REPLACE});
          }}
        ]
      )
    }


    onClaimed = () =>{
       this.props.erActions.upsert(this.props.er.iap.activeRecord,this.props.er.iap.activeRecord.status,this.successSubmit);
    }



    render() {


      let claimingSignature = null;

      if(this.props.er.iap.activeRecord.claiming_person_signature != null){
        claimingSignature = `data:image/png;base64,`+this.props.er.iap.activeRecord.claiming_person_signature ;
      }else{
        if(this.props.er.iap.activeRecord.hasClaiming){
          claimingSignature = Config.getHost()+`/restapi/iap/claiming_person_signature/${this.props.er.iap.activeRecord.id}`;
        }
      }


        return (
          <ScrollView style={{ marginTop: 65 }}>

              <SignatureModal
                id={'claiming_person_signature'}
                isSignatureOpen={this.state.claiming_person_signature}
                closeSignatureModal={this.closeSignatureModal}
                openSignatureModal={this.openSignatureModal}
                updateField={this.updateFieldSignature}
              />
              <SignaturePreviewModal
                id='claiming_person_signature'
                imgSrc={claimingSignature}
                previewSignOpen={this.state.claiming_person_signaturePreview}
                closeSignaturePreviewModal={this.closeSignatureModalPreview}
                openSignaturePreviewModal={this.openSignatureModalPreview}
                openSignatureModal={this.openSignatureModal}
              />

          <View minHeight={100*vh-64-64}>
              <View style={[HISstyle.putContainerPadding,{marginTop:10}]}>
                    <View style={{ flex: 1 }}>
                      <Text style={HISstyle.formLabel}>CLAIMING PERSON FULLNAME</Text>
                      <Hideo
                        onChangeText={(value)=>this.updateField(value)}
                        value={this.props.er.iap.activeRecord.claiming_person_fullname || ""}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        placeholder={'Item Description'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        style={{paddingRight: 5}}
                        />
                    </View>

                    {
                      claimingSignature != null ?
                      (
                        <View style={{ flex: 1,marginTop:10 }}>
                          <Button className="primary" textStyle={HISstyle.warningText} style={HISstyle.warningButton} onPress={this.openSignatureModalPreview} >
                            Preview Signature
                          </Button>
                        </View>
                      ):
                      (
                        <View style={{ flex: 1,marginTop:10 }}>
                          <Button className="primary" textStyle={HISstyle.deleteText} style={HISstyle.deleteButtonStyle} onPress={this.openSignatureModal} >
                            Signature
                          </Button>
                        </View>
                      )
                    }
                    <View style={{ flexDirection: 'row',marginTop:20}}>
                      <View style={{ flex: 0.7}}></View>
                      <View style={{ flex: 0.3,marginBottom:10 }}>
                        <Button className="primary" textStyle={HISstyle.saveText} style={HISstyle.saveButtonStyle}  onPress={this.onClaimed}>
                          Save
                        </Button>
                      </View>
                    </View>
              </View>

            </View>
          </ScrollView>
        );
    }
}
function mapStateToProps(state){
    return {
        patients: state.patients,
        er: state.er
    }
}

function mapDispatchToProps(dispatch) {
    return {
        patientActions: bindActionCreators(patientActions, dispatch),
        erActions: bindActionCreators(erActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(IapClaimed);
