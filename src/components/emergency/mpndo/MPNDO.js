import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  Modal,
  TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/EvilIcons';
import { Fumi, Hoshi, Hideo } from 'react-native-textinput-effects';
import _ from 'lodash';
import moment from 'moment';

import * as style from '../../../styles/styles';
import * as HISstyle from '../../../styles/HISstyle';

import * as authActions from '../../../actions/authActions';
import * as healthchecks from '../../../actions/healthchecks';
import * as erActions from '../../../actions/erActions';

import SignatureModal from '../../general/SignatureModal'
import MultiSelectModal from '../../general/MultiSelectModal'
import DoctorsOrderForm from './DoctorsOrderForm'

const patientData = require('../../../assets/MOCK_DATA.json');

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});


const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);


export default class MPNDO extends Component {
    constructor(props){
      super(props);

      this.state = {
        count: 1
      }
    }

    openModal = (orderId) => {
      return()=>{
        this.props.updateField('doctor_order', orderId);
        this.props.openModal();
      }
    }

    renderOrderRow = (rowData, sectionID, rowID) => {
      let orderLabel = 'ORDER';
      let orderColor = HISstyle.redColor;

      switch(rowData.doctype)
      {
        case 'labOrder': orderLabel = 'LABORATORY'; break;
        case 'imaging': orderLabel = 'IMAGING'; break;
        default: break;
      }

      if(rowData.approved == true || rowData.approved == 'true')
        orderColor = HISstyle.mainColor;

      return (
        <View style={[HISstyle.lineBottom, HISstyle.putShadow], { borderLeftWidth: 5, borderLeftColor: orderColor, backgroundColor: 'white', justifyContent: 'center', paddingLeft: 7, paddingTop: 7 }}>
          <TouchableOpacity onPress={this.openModal(rowData.order_id)}>
            <Text style={[HISstyle.orderLabel,{ justifyContent: 'center', color: orderColor }]}>{orderLabel}</Text>
            <Text style={[HISstyle.headTitle,{marginBottom:10}]}>{rowData.order ? rowData.order : 'No specific note specified'}</Text>
          </TouchableOpacity>
        </View>
      )
    }

    renderNoteRow = (rowData, sectionID, rowID) => {
      return (
        <View style={[HISstyle.lineBottom, HISstyle.putShadow], { borderLeftWidth: 5, borderLeftColor: HISstyle.lightyellow, backgroundColor: 'white', justifyContent: 'center', paddingLeft: 7, paddingTop: 7 }}>
          <Text style={[HISstyle.headTitle,{marginBottom:10}]}>{rowData.note ? rowData.note : 'No specific note specified'}</Text>
        </View>
      )
    }

    renderRow = (rowData, sectionID, rowID) => {

      let orderColor = HISstyle.redColor;
      let orderColorBack = HISstyle.redColorBack;

      let newOrdersList = _.map(rowData.doctors_order, (data)=> {
        return {order: data.order, order_id: data.order_id, doctype: data.doctype, approved: rowData.approved}
      })

      if(rowData.approved == true || rowData.approved == 'true'){
        orderColorBack = HISstyle.mainColorBack;
        orderColor = HISstyle.mainColor;
      }


      return (
        <View style={{flexDirection: 'row', marginBottom: 5 }}>
          <View style={{ flex: 0.5, paddingRight: 10 }}>
            {_.get(rowData, 'progress_note') ?
              rowData.progress_note.length > 0 ?  (
              <View style={{ flexDirection: 'column'}}>

                  <View style={{ borderLeftWidth: 5, borderLeftColor: HISstyle.lightyellow, backgroundColor: 'white',paddingLeft: 7 }}>
                    <Text>
                      {_.get(rowData, 'pn_datetime') ? moment(_.get(rowData, 'pn_datetime', '')).format('MMM DD, YYYY HHmm') + 'H' : moment().format('MMM DD, YYYY HHmm') + 'H'}
                    </Text>
                  </View>

                <ListView
                  enableEmptySections={true}
                  dataSource={ds.cloneWithRows(rowData.progress_note)}
                  renderRow={this.renderNoteRow}
                  containerStyle={{ flex: 1 }}
                />
                <View style={{ borderLeftWidth: 5, borderLeftColor: HISstyle.lightyellow, backgroundColor: 'white',paddingLeft: 7 }}><Text>{rowData.doctorFullname}</Text></View>
              </View>
            ) : null : null}
          </View>

          <View style={{ flex: 0.5 }}>
            {_.get(rowData, 'doctors_order')?
              rowData.doctors_order.length > 0 ?  (
              <View style={{ flexDirection: 'column' }}>

                <View style={{ borderLeftWidth: 5, borderLeftColor: orderColor, backgroundColor: 'white',paddingLeft: 7 }}>
                  <Text>
                    {_.get(rowData, 'do_datetime') ? moment(_.get(rowData, 'do_datetime', '')).format('MMM DD, YYYY HHmm') + 'H' : moment().format('MMM DD, YYYY HHmm') + 'H'}
                  </Text>
                </View>

                <ListView
                  enableEmptySections={true}
                  dataSource={ds.cloneWithRows(newOrdersList)}
                  renderRow={this.renderOrderRow}
                  containerStyle={{ flex: 1 }}
                />

                <View style={{ borderLeftWidth: 5, borderLeftColor: orderColor, backgroundColor: 'white',paddingLeft: 7 }}><Text>{rowData.doctorFullname}</Text></View>
              </View>
            ) : null : null}
          </View>
        </View>
      )
    }

    render() {
      return (
          <View style={{ flex: 1,backgroundColor:HISstyle.backColor }}>

            <View
              style={{position:'absolute',
                          justifyContent:'center',
                          top:0,
                          bottom:0,
                          right:0,
                          left:0,
                          width:100*vw-395,
                          borderRightWidth: 1,
                          borderRightColor: '#e2e2e2'
                        }}></View>

            <View style={{marginTop:10}}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 0.5,alignItems:'center'}}>
                  <Text style={HISstyle.headTitle}>PROGRESS NOTES</Text>
                </View>
                <View style={{flex: 0.5,alignItems:'center'}}>
                  <Text style={HISstyle.headTitle}>DOCTORS ORDER</Text>
                </View>
              </View>


            </View>
            <ListView
              enableEmptySections={true}
              dataSource={ds.cloneWithRows(this.props.mpndoList)}
              renderRow={this.renderRow}
              containerStyle={{ flex: 1 }}
            />
          </View>
      )
    }
}
