import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  Modal,
  TouchableOpacity,
  Alert
} from 'react-native'

import Icon from 'react-native-vector-icons/EvilIcons';
import { Fumi, Hoshi, Hideo } from 'react-native-textinput-effects'
import Button from 'apsl-react-native-button'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import moment from 'moment';

import * as style from '../../../styles/styles';
import * as HISstyle from '../../../styles/HISstyle';
import * as Config from '../../../config/Config';

import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal';

const patientData = require('../../../assets/MOCK_DATA.json');
const { width, height, scale } = Dimensions.get('window')
import TitleBar from '../../general/TitleBar'
export default class NurseProgressNote extends Component {
  constructor(props) {
    super(props);

    this.state = {
      NPNDate: false
    }
  }

  showDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: true});
    }
  }

  hideDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: false});
    }
  }

  setDateTime=(value, name)=>{
    return(date)=>{
      this.props.updateField([name], date);
      this.setState({[value]: false});
    }
  }

  openSignatureModal = (name) => {
    this.setState({
      [name]: true
    })
  }

  closeSignatureModal = (name) => {
    this.setState({
      [name]: false
    })
  }

  saveSignature = (name, value)=>{
    this.props.updateField(name, value);
  }

  openSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: true
    })
  }

  closeSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: false
    })
  }

  closeModal=()=>{
    return()=>{
      this.setState({
        NPNDate: false,
      }, this.props.closeModal('showNPNModal'));
    }
  }

  submitForm=()=>{
    return()=>{
      if(this.props.npn.activeRecord.npn_signature == null){
        Alert.alert(
          'Error',
          'Please sign before saving!',
          [{
            text: 'OK',
            onPress: ()=>{},
          }]
        );
      }else {
        this.setState({
          NPNDate: false,
        }, this.props.submitForm());
      }
    }
  }

  render() {
    activeRecord = this.props.npn.activeRecord;

    let npn_signatureImg = null;
    if(activeRecord.npn_signature != null){
      npn_signatureImg = `data:image/png;base64,`+activeRecord.npn_signature;
    }else{
      if(activeRecord.npn_signature){
        npn_signatureImg = Config.getHost()+`/restapi/npns/npn_signature/${activeRecord.id}`;
      }
    }

    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.showModal}
        onRequestClose={()=>{}}
        >
        <KeyboardAwareScrollView ref='scroll'>
          <View style={{backgroundColor:'#ffffff'}}>
            <TitleBar title={'NURSE PROGRESS NOTES'} />

            <View style={HISstyle.putContainerPadding}>
              <View style={{ flex: 1, paddingRight: 5,marginTop:10 }}>
                <Text style={HISstyle.formLabel}>NURSE PROGRESS NOTES</Text>
                <TouchableOpacity onPress={()=>{} /*this.showDatePicker('NPNDate')*/}>
                  <Hideo
                    value={_.get(activeRecord, 'input_datetime') ? moment(_.get(activeRecord, 'input_datetime', '')).format('MMM DD, YYYY HHmm') + 'H' : moment().format('MMM DD, YYYY HHmm') + 'H'}
                    iconClass={Icon}
                    iconName={'pencil'}
                    iconColor={'white'}
                    iconBackgroundColor={HISstyle.mainColor}
                    inputStyle={HISstyle.formInput}
                    editable={false}
                    />
                </TouchableOpacity>
                <DateTimePicker
                  isVisible={this.state.NPNDate}
                  onConfirm={this.setDateTime('NPNDate', 'input_datetime')}
                  onCancel={this.hideDatePicker('NPNDate')}
                  mode={'datetime'}
                  />
              </View>
              <View style={{ flex: 1, paddingRight: 5,marginTop:10 }}>
                <TextInput
                  editable={true}
                  maxLength={1000}
                  multiline={true}
                  placeholder={'What is your order?'}
                  style={[HISstyle.formInput,{height: 150,padding:10}]}
                  onChangeText={(txt) => this.props.updateField('note', txt)}
                  />
              </View>
            </View>

            <View style={{flexDirection: 'row',marginTop:20}}>
              <View style={{flex:0.3  , flexDirection: 'row'}}>
                {npn_signatureImg!=null?
                  (
                    <Button
                      onPress={()=>this.openSignaturePreviewModal(`npn_signature`)}
                      textStyle={HISstyle.warningText}
                      style={[HISstyle.warningButton,HISstyle.putShadow,{flex:0.3,marginBottom:10  }]} >
                      Preview
                    </Button>
                  ):(
                    <Button
                      onPress={()=>this.openSignatureModal(`npn_signature`)}
                      textStyle={HISstyle.deleteText}
                      style={[HISstyle.deleteButtonStyle,HISstyle.putShadow,{flex:0.3,marginBottom:10  }]} >
                      Signature
                    </Button>
                  )
                }
              </View>
              <View style={{ flex: 0.3, flexDirection: 'row',marginLeft:10,marginRight:10 }}>
                <Button  onPress={this.closeModal()}
                  style={[HISstyle.deleteButtonStyle,HISstyle.putShadow,{flex:0.3,  justifyContent: 'center', alignItems: 'center',marginBottom:10  }]} >
                  <Text style={HISstyle.deleteText}>Close</Text>
                </Button>
              </View>
              <View style={{ flex: 0.3 ,flexDirection:'row'}}>
                <Button onPress={this.submitForm()}
                  style={[HISstyle.saveButtonStyle,HISstyle.putShadow,{flex:0.3,  justifyContent: 'center', alignItems: 'center',marginBottom:10  }]} >
                  <Text style={HISstyle.saveText}>Save</Text>
                </Button>
              </View>
            </View>

          </View>
        </KeyboardAwareScrollView>

        <SignatureModal
          id={"npn_signature"}
          isSignatureOpen={this.state.npn_signature}
          closeSignatureModal={this.closeSignatureModal}
          openSignatureModal={this.openSignatureModal}
          updateField={this.saveSignature}
          />

        <SignaturePreviewModal
          id='npn_signature'
          imgSrc={npn_signatureImg}
          previewSignOpen={this.state.npn_signaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.openSignatureModal}
          />
      </Modal>
    )
  }
}
