import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  Alert,
  TouchableOpacity
} from 'react-native'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import Promise from 'bluebird'

import { Fumi, Hoshi, Hideo } from 'react-native-textinput-effects'
import {Actions} from 'react-native-router-flux'
import {post,get} from '../../../utils/RestClient'
import * as style from '../../../styles/styles'
import * as HISstyle from '../../../styles/HISstyle'
import Icon from 'react-native-vector-icons/EvilIcons'
import ModalPicker from 'react-native-modal-picker'
import Button from 'apsl-react-native-button'
import Modal from 'react-native-simple-modal'

import Segment from '../../general/Segment'
import * as Config from '../../../config/Config'
import _ from 'lodash';
import moment from 'moment';

import * as authActions from '../../../actions/authActions';
import * as healthchecks from '../../../actions/healthchecks';
import * as erActions from '../../../actions/erActions';
import * as mpndoActions from '../../../actions/mpndoActions';

import SignatureModal from '../../general/SignatureModal'
import MultiSelectModal from '../../general/MultiSelectModal'

import DoctorsOrderForm from './DoctorsOrderForm'
import NurseProgressNote from './NurseProgressNote'

import PatientInfo from '../../PatientInfo'

import MPNDO from './MPNDO';
import TitleBar from '../../general/TitleBar'
const patientData = require('../../../assets/MOCK_DATA.json');

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class MPNDOContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNPNModal: false,
      showMPNDOModal: false,
      selectedRowDetails: [],
      orderNote: '',
      mpndoID: '',
      mpndoList: []
    }
  }

  componentDidMount () {
    this.props.mpndoActions.getMPNDOByPMR(this.props.er.pmr.activeRecord.id);
    this.getAttendingPhysByFilter('');
  }

  closeModal = (modalName) => {
    return()=>{
      this.setState({ [modalName]: false },
        ()=>{
          this.props.mpndoActions.updateMPNDOInput('clear')
          this.props.mpndoActions.updateNPNInput('clear')
        }
      );
    }
  }

  openModal = (modalName) => {
    return()=>{
      this.setState({ [modalName]: true });
    }
  }

  viewDetails = (rowData) => {
    return () => {
      this.setState({ selectedRowDetails: rowData });
    }
  }

  getAttendingPhysByFilter = (filter) => {
    get(Config.getHost()+'/restapi/employees/search/findAllByFilterAndTypeAndDoc',{
      params:{
        filter,
        careProviderType: "doctor",
        attendingPhysician: true
      }
    }).then((response) => {
      if(response.data._embedded && response.data._embedded.employees){

        var attendingPhysicians = response.data._embedded.employees.map((item) => {
          return {
            label: item.fullname,
            value: item._links.self.href,
            employeeId: item.id
          }
        });

        this.setState({
          attendingPhysicians
        });
      }

    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getAttendingPhysByFilter', error);
      }
    });
  }

  onDoctorSearch = (term) => {
    if(this.timeoutHandle)
    clearTimeout(this.timeoutHandle);

    this.timeoutHandle = setTimeout(()=>{
      this.getAttendingPhysByFilter(term);
      this.timeoutHandle = undefined;
    }, 500);
  }

  updateNPNField = (name, value) => {
    var payload = {};
    payload[name] = value;
    this.props.mpndoActions.updateNPNInput(payload);
  }

  updateMPNDOField = (name, value) => {
    var payload = {};
    payload[name] = value;
    this.props.mpndoActions.updateMPNDOInput(payload);
  }

  showResponse=(result, message, modalName)=>{
    if(result){
      Alert.alert(
        'Success',
        message,
        [{
          text: 'OK',
          onPress: this.closeModal(modalName),
        }]
      );
    }
    else {
      Alert.alert(
        'Error',
        message,
        [{
          text: 'OK',
          onPress: this.closeModal(modalName),
        }]
      );
    }
  }

  submitNPNForm = () => {
    return()=>{
      let activeRecord = this.props.npn.activeRecord;

      activeRecord.nurse = "/restapi/employees/" + this.props.auth.account.employeeid;

      if (!_.get(activeRecord, 'input_datetime')) {
        activeRecord.input_datetime = moment();
      }

      this.props.mpndoActions.saveNPN(activeRecord, (result, message)=>{
        this.props.mpndoActions.updateNPNInput('clear');
        this.props.mpndoActions.getMPNDOByPMR(this.props.er.pmr.activeRecord.id);
        this.showResponse(result, message, 'showNPNModal');
      });
    }
  }

  submitMPNDOForm = () => {
    return()=>{
      let activeRecord = this.props.mpndo.activeRecord;

      activeRecord.erPmr = this.props.er.pmr.activeRecord.id;

      activeRecord.progress_note = JSON.stringify(activeRecord.progress_note);
      activeRecord.doctors_order = JSON.stringify(activeRecord.doctors_order);
      if (!_.get(activeRecord, 'pn_datetime')) {
        activeRecord.pn_datetime = moment();
      }

      if (!_.get(activeRecord, 'do_datetime')) {
        activeRecord.do_datetime = moment();
      }

      this.props.mpndoActions.saveMPNDO(activeRecord, (result, message)=>{
        this.props.mpndoActions.updateMPNDOInput('clear');
        this.props.mpndoActions.getMPNDOByPMR(this.props.er.pmr.activeRecord.id);
        this.showResponse(result, message, 'showMPNDOModal');
      });
    }
  }

  render() {
    return (
      <View style={{ flex: 1, marginTop: 64,backgroundColor:HISstyle.backColor }}>
        <View
          style={{position:'absolute',
                      justifyContent:'center',
                      top:0,
                      bottom:0,
                      right:0,
                      left:10,
                      width:100*vw-395,
                      borderRightWidth: 1,
                      borderRightColor: '#e2e2e2'
                    }}></View>
        <PatientInfo {...this.props} />

        <View style={[{flex:1,padding:10}]}>

          <MPNDO
            mpndoList={this.props.mpndo.records}
            openModal={this.openModal('showNPNModal')}
            updateField={this.updateNPNField}
            />

            <View style={{ flexDirection: 'row', alignItems: 'center'}}>
              <Button onPress={this.openModal('showMPNDOModal')} textStyle={{ color: 'white'}}
                style={[HISstyle.addButton,{  width: 200, marginHorizontal: 5 }]}>
                NEW ORDER
              </Button>
              <Button onPress={this.openModal('showNPNModal')} textStyle={{ color: 'white'}}
                style={[HISstyle.addButton,{  width: 200, marginHorizontal: 5 }]}>
                NEW NURSE PROGRESS NOTE
              </Button>
            </View>

          <NurseProgressNote
            showModal={this.state.showNPNModal}
            openModal={this.openModal}
            closeModal={this.closeModal}
            updateField={this.updateNPNField}
            submitForm={this.submitNPNForm}
            {...this.props}
            />

          <DoctorsOrderForm
            showModal={this.state.showMPNDOModal}
            openModal={this.openModal}
            closeModal={this.closeModal}
            updateField={this.updateMPNDOField}
            submitForm={this.submitMPNDOForm}
            attendingPhysicians={this.state.attendingPhysicians}
            onDoctorSearch={this.onDoctorSearch}
            {...this.props}
            />
        </View>

      </View>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    er: state.er,
    mpndo: state.er.mpndo,
    npn: state.er.npn,
    pmr: state.er.pmr,
    patients: state.patients,
    pdscs: state.pdscs
  }
}

function mapDispatchToProps(dispatch) {
  return {
    erActions: bindActionCreators(erActions, dispatch),
    mpndoActions: bindActionCreators(mpndoActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MPNDOContainer);
