import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  Modal,
  TouchableOpacity,
  Alert
} from 'react-native'

import Icon from 'react-native-vector-icons/EvilIcons';
import { Fumi, Hoshi, Hideo } from 'react-native-textinput-effects'
import Button from 'apsl-react-native-button'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import moment from 'moment';
import uuid from 'uuid';

import ModalBox from 'react-native-modalbox';

import * as style from '../../../styles/styles';
import * as HISstyle from '../../../styles/HISstyle';
import * as Config from '../../../config/Config';

import ListViewSelect from '../../general/ListViewSelect';
import MultiSelectModal from '../../general/MultiSelectModal';
import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal';

const patientData = require('../../../assets/MOCK_DATA.json');
const { width, height, scale } = Dimensions.get('window')
import TitleBar from '../../general/TitleBar'

import ModalPicker from 'react-native-modal-picker'
import ListViewSelectModal from '../../general/ListViewSelectModal';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class DoctorsOrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      doctor: null,
      isSelectibleOpen: false,
      progress_note: '',
      doctors_order: '',
      orders: [],
      notes: [],
      PNDate: false,
      DODate: false,
      isOpen:false,
      isOpen2:false,
      dataLabOrder:[
        {value:'(CBC)-Complete Blood Count'},
        {value:'(ECG)-ElectroCardioGram'},
        {value:'Dengue Duo'},
        {value: 'Urinalysis'}
      ],
      dataLabImaging:[
        {value:'Chest X-Ray (RIGHT)'},
        {value:'Chest X-Ray (LEFT)'},
        {value:'CT-Scan'},
        {value:'MRI'}
      ]
    }
  }

  addOrder=()=>{
    return()=>{
      if (_.get(this.state, 'doctors_order')) {
        let orders = this.state.orders;
        orders.push({order: this.state.doctors_order, order_id: uuid.v1(), doctype: 'custom'});

        this.setState({orders: orders, doctors_order: ''});
        this.props.updateField('doctors_order', orders);
      }
    }
  }

  removeOrder=(rowID)=>{
    return()=>{
      let orders = this.state.orders;
      orders.splice(rowID);

      this.setState({orders: orders});
      this.props.updateField('doctors_order', orders);
    }
  }

  addNote=()=>{
    return()=>{
      if (_.get(this.state, 'progress_note')) {
        let notes = this.state.notes;
        notes.push({note: this.state.progress_note});

        this.setState({notes: notes, progress_note: ''});
        this.props.updateField('progress_note', notes);
      }

    }
  }

  removeNote=(rowID)=>{
    return()=>{
      let notes = this.state.notes;
      notes.splice(rowID);

      this.setState({notes: notes});
      this.props.updateField('progress_note', notes);
    }
  }

  showDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: true});
    }
  }

  hideDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: false});
    }
  }

  setDateTime=(value, name)=>{
    return(date)=>{
      this.props.updateField([name], date);
      this.setState({[value]: false});
    }
  }

  renderOrderRow = (rowData, sectionID, rowID) => {
    return (
      <View style={{flexDirection: 'row',backgroundColor:'#F6F7FE',marginTop:4,borderBottomColor:'#CCC',borderBottomWidth:1}}>
        <Text style={{ flex: 1,marginTop:10,marginLeft:20,fontSize:HISstyle.fnFamilyRegular}}>{rowData.order}</Text>
        <Button
          onPress={this.removeOrder(rowID)}
          textStyle={{ color: 'white'}}
          style={{ backgroundColor: HISstyle.redColor,width:25,height:25,borderRadius:1,borderColor:HISstyle.redColor,marginTop:6}}>
          X
        </Button>
        <View style={{ height: 0.5,
            marginLeft: 8,
            marginRight: 8,
            backgroundColor: '#CCC'}}></View>
        </View>
      )
    }

    renderNoteRow = (rowData, sectionID, rowID) => {
      return (
        <View style={{flexDirection: 'row',backgroundColor:'#F6F7FE',marginTop:4,borderBottomColor:'#CCC',borderBottomWidth:1}}>
          <Text style={{ flex: 1,marginTop:10,marginLeft:20,fontSize:HISstyle.fnFamilyRegular}}>{rowData.note}</Text>
          <Button
            onPress={this.removeNote(rowID)}
            textStyle={{ color: 'white'}}
            style={{ backgroundColor: HISstyle.redColor,width:25,height:25,borderRadius:1,borderColor:HISstyle.redColor,marginTop:6}}>
            X
          </Button>
          <View style={{ height: 0.5,
              marginLeft: 8,
              marginRight: 8,
              backgroundColor: '#CCC'}}></View>
          </View>
        )
      }

      showPopover=(id)=>{
        return()=>{
          this.setState({[id]: true});
        }
      }

      closePopover=(id)=>{
        return()=>{
          this.setState({[id]: false});
        }
      }

      setSelectItem=(type)=>{
        return(data)=>{
          let orders = this.state.orders;
          orders.push({order: data, order_id: uuid.v1(), doctype: type});

          this.setState({orders: orders});
          this.props.updateField('doctors_order', orders);
        }
      }

      onSelectItem=(data,type)=>{
        let orders = this.state.orders;
        orders.push({order: data, order_id: uuid.v1(), doctype: type});

        this.setState({orders: orders});
        this.props.updateField('doctors_order', orders);

        this.setState({isOpen:false})
      }

      openSignatureModal = (name) => {
        this.setState({
          [name]: true
        })
      }

      closeSignatureModal = (name) => {
        this.setState({
          [name]: false
        })
      }

      saveSignature = (name, value)=>{
        this.props.updateField(name, value);
      }

      openSignaturePreviewModal = (name) => {
        name = name+'Preview';
        this.setState({
          [name]: true
        })
      }

      closeSignaturePreviewModal = (name) => {
        name = name+'Preview';
        this.setState({
          [name]: false
        })
      }

      closeModal=()=>{
        return()=>{
          this.setState({
            doctor: null,
            progress_note: '',
            doctors_order: '',
            orders: [],
            notes: [],
            PNDate: false,
            DODate: false,
            isOpen:false,
            isOpen2:false
          }, this.props.closeModal('showMPNDOModal'));
        }
      }

      submitForm=()=>{
        return()=>{
          if (this.props.mpndo.activeRecord.doctor == null) {
              Alert.alert(
                'Error',
                'Please select doctor before saving!',
                [{
                  text: 'OK',
                  onPress: ()=>{},
                }]
              );
          }else if (this.props.mpndo.activeRecord.doctor_signature == null){
            Alert.alert(
              'Error',
              'Please sign before saving!',
              [{
                text: 'OK',
                onPress: ()=>{},
              }]
            );
          }else {
            this.setState({
              doctor: null,
              progress_note: '',
              doctors_order: '',
              orders: [],
              notes: [],
              PNDate: false,
              DODate: false
            }, this.props.submitForm());
          }
        }
      }


      isOpenBox = () =>{
        this.setState({isOpen:true})
      }

      closeModalBox = () =>{
        this.setState({isOpen:false})
      }
      isOpenBox2 = () =>{
        this.setState({isOpen2:true})
      }

      closeModalBox2 = () =>{
        this.setState({isOpen2:false})
      }

      openSelectibleModal = (name) => {
        return()=>{
          this.setState({
            [name]: true
          });
        }
      }

      closeSelectibleModal = (name) => {
        this.setState({ [name]: false }, ()=> {
          this.props.onDoctorSearch('');
        })
      }

      onSelectionsChangePhysicians = (doctors, items) => {
        console.log(items.label);
        this.setState({doctor: items.label}, ()=>{
          this.props.updateField('doctor', items.value)
          this.closeSelectibleModal('isSelectibleOpen')
        });
      }

      render() {
        activeRecord = this.props.mpndo.activeRecord;

        let doctor_signatureImg = null;
        if(activeRecord.doctor_signature != null){
          doctor_signatureImg = `data:image/png;base64,`+activeRecord.doctor_signature;
        }else{
          if(activeRecord.doctor_signature){
            doctor_signatureImg = Config.getHost()+`/restapi/mpnDoes/doctor_signature/${activeRecord.id}`;
          }
        }

        let attendingPhysicians = _.map(this.props.attendingPhysicians, (doctor, idx) => {
          return {
            key: idx,
            label: doctor.label,
            value: doctor.value,
            employeeId: doctor.employeeId
          }
        });

        return (
          <Modal
            animationType={"slide"}
            transparent={false}
            visible={this.props.showModal}
            onRequestClose={()=>{}}
            >
            <KeyboardAwareScrollView ref='scroll'>
              <View style={{backgroundColor:'#ffffff'}}>
                <TitleBar title={'DOCTORS ORDER'} />

                <View style={HISstyle.putContainerPadding}>
                  <Text style={[HISstyle.formLabel,{fontWeight:'bold'}]}>DOCTOR</Text>
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={this.openSelectibleModal('isSelectibleOpen')}>
                      <Hideo
                        value={_.get(this.state, 'doctor') ? _.get(this.state, 'doctor', '') : null}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        placeholder={'Doctor'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        editable={false}
                        />
                    </TouchableOpacity>
                  </View>

                  <View style={{ flex: 1, marginTop:10 }}>
                    <Text style={[HISstyle.formLabel,{fontWeight:'bold'}]}>PROGRESS NOTES</Text>
                    <TouchableOpacity onPress={()=>{} /*this.showDatePicker('PNDate')*/}>
                      <Hideo
                        value={_.get(activeRecord, 'pn_datetime') ? moment(_.get(activeRecord, 'pn_datetime', '')).format('MMM DD, YYYY HHmm') + 'H' : moment().format('MMM DD, YYYY HHmm') + 'H'}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        editable={false}
                        />
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.PNDate}
                      onConfirm={this.setDateTime('PNDate', 'pn_datetime')}
                      onCancel={this.hideDatePicker('PNDate')}
                      mode={'datetime'}
                      />

                    <ListView
                      enableEmptySections={true}
                      dataSource={ds.cloneWithRows(this.state.notes)}
                      renderRow={this.renderNoteRow}
                      containerStyle={{ flex: 1 }}
                      />
                  </View>
                  <View style={{ flex: 1, marginTop:10 }}>
                    <TextInput
                      editable={true}
                      maxLength={1000}
                      multiline={true}
                      placeholder={'Extra notes/details...'}
                      style={[HISstyle.formInput,{height: 150,padding:10,fontSize:20}]}
                      onChangeText={(txt) => this.setState({'progress_note': txt})}
                      />
                  </View>
                  <View style={[HISstyle.formGroup, { flex: 0.4 }]}>
                    <Button
                      onPress={this.addNote()}
                      textStyle={{ color: 'white'}}
                      style={[HISstyle.addButton,HISstyle.putShadow,{flex:1,marginTop:10}]}>
                      Add
                    </Button>
                  </View>

                  <View style={{ flex: 1, marginTop:10 }}>
                    <Text style={[HISstyle.formLabel,{fontWeight:'bold'}]}>DOCTORS ORDER</Text>
                    <TouchableOpacity onPress={()=>{} /*this.showDatePicker('DODate')*/}>
                      <Hideo
                        value={_.get(activeRecord, 'do_datetime') ? moment(_.get(activeRecord, 'do_datetime', '')).format('MMM DD, YYYY HHmm') + 'H' : moment().format('MMM DD, YYYY HHmm') + 'H'}
                        iconClass={Icon}
                        iconName={'pencil'}
                        iconColor={'white'}
                        iconBackgroundColor={HISstyle.mainColor}
                        inputStyle={HISstyle.formInput}
                        editable={false}
                        />
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.DODate}
                      onConfirm={this.setDateTime('DODate', 'do_datetime')}
                      onCancel={this.hideDatePicker('DODate')}
                      mode={'datetime'}
                      />
                    <ListView
                      enableEmptySections={true}
                      dataSource={ds.cloneWithRows(this.state.orders)}
                      renderRow={this.renderOrderRow}
                      containerStyle={{ flex: 1 }}
                      />
                  </View>
                  <View style={{ flex: 1, marginTop:10 }}>
                    <TextInput
                      editable={true}
                      maxLength={1000}
                      multiline={true}
                      placeholder={'What is your order?'}
                      style={[HISstyle.formInput,{height: 150,padding:10,fontSize:20}]}
                      onChangeText={(txt) => this.setState({'doctors_order': txt})}
                      />
                  </View>
                  <View style={[HISstyle.formGroup, { flex: 0.4 }]}>
                    <Button
                      onPress={this.addOrder()}
                      textStyle={{ color: 'white'}}
                      style={[HISstyle.addButton,HISstyle.putShadow,{flex:1,marginTop:10}]}>
                      Add
                    </Button>
                  </View>



                  <View style={[style.formGroup, { flex: 0.6 }]}>
                    <View style={{flexWrap: 'wrap', flexDirection: 'row', alignItems: 'center',marginLeft:7 }}>
                      {/* <ListViewSelect
                        list={['Sample A', 'Sample B', 'Sample C']}
                        containerStyle={{}}
                        rowText={{color:HISstyle.mainColor,padding:10,fontSize:15}}
                        isVisible={this.state.labOrder}
                        onClick={this.setSelectItem('labOrder')}
                        onClose={this.closePopover('labOrder')}
                        /> */}
                        <ListViewSelect
                          list={['Sample A', 'Sample B', 'Sample C']}
                          containerStyle={{}}
                          rowText={{color:HISstyle.mainColor,padding:10,fontSize:15}}
                          isVisible={this.state.imaging}
                          onClick={this.setSelectItem('imaging')}
                          onClose={this.closePopover('imaging')}
                          />
                      </View>
                      <View style={{flexWrap: 'wrap', flexDirection: 'row', alignItems: 'center',marginLeft:7 }}>

                        <Button onPress={this.isOpenBox} textStyle={{ color: 'white'}} style={{ backgroundColor: '#2C3742', flex: 1, borderRadius: 3, marginRight: 10 }}>
                          LAB ORDER
                        </Button>
                        <Button onPress={this.isOpenBox2} textStyle={{ color: 'white'}} style={{ backgroundColor: '#2C3742', flex: 1, borderRadius: 3, marginRight: 10 }}>
                          IMAGING
                        </Button>
                        {/* <Button onPress={this.showPopover('labOrder')} textStyle={{ color: 'white'}}
                        style={{ backgroundColor: '#2C3742', flex: 1, borderRadius: 3, marginRight: 10 }}>
                        LAB ORDER
                        </Button> */}

                        {/* <Button onPress={this.showPopover('imaging')} textStyle={{ color: 'white'}}
                        style={{ backgroundColor: '#2C3742', flex: 1, borderRadius: 3, marginRight: 10 }}>
                        IMAGING
                        </Button> */}

                        {/* <Button onPress={this.showPopover('medication')} textStyle={{ color: 'white'}}
                        style={{ backgroundColor: '#2C3742', flex: 1, borderRadius: 3, marginRight: 10 }}>
                        MEDICATION
                        </Button> */}

                      </View>
                    </View>
                  </View>
                </View>
              </KeyboardAwareScrollView>
              <View style={{flexDirection: 'row',marginLeft:10,marginRight:10}}>
                <View style={{flex:0.5  , flexDirection: 'row'}}>
                  {doctor_signatureImg!=null?
                    (
                      <Button
                        onPress={()=>this.openSignaturePreviewModal(`doctor_signature`)}
                        textStyle={HISstyle.warningText}
                        style={[HISstyle.warningButton,HISstyle.putShadow,{flex:0.3,marginBottom:10  }]} >
                        <Text style={HISstyle.warningText}>Preview</Text>
                      </Button>
                    ):(
                      <Button
                        onPress={()=>this.openSignatureModal(`doctor_signature`)}
                        textStyle={HISstyle.deleteText}
                        style={[HISstyle.addButton,HISstyle.putShadow,{flex:0.3,marginBottom:10  }]} >
                        <Text style={HISstyle.addText}>Signature</Text>
                      </Button>
                    )
                  }
                </View>
                <View style={{ flex: 0.3, flexDirection: 'row',marginLeft:10,marginRight:10 }}>
                  <Button  onPress={this.closeModal()}
                    style={[HISstyle.deleteButtonStyle,HISstyle.putShadow,{flex:0.3,  justifyContent: 'center', alignItems: 'center',marginBottom:10  }]} >
                    <Text style={HISstyle.deleteText}>Close</Text>
                  </Button>
                </View>
                <View style={{ flex: 0.3 ,flexDirection:'row'}}>
                  <Button onPress={this.submitForm()}
                    style={[HISstyle.saveButtonStyle,HISstyle.putShadow,{flex:0.3,  justifyContent: 'center', alignItems: 'center',marginBottom:10  }]} >
                    <Text style={HISstyle.saveText}>Save</Text>
                  </Button>
                </View>
              </View>

              <SignatureModal
                id={"doctor_signature"}
                isSignatureOpen={this.state.doctor_signature}
                closeSignatureModal={this.closeSignatureModal}
                openSignatureModal={this.openSignatureModal}
                updateField={this.saveSignature}
                />

              <SignaturePreviewModal
                id='doctor_signature'
                imgSrc={doctor_signatureImg}
                previewSignOpen={this.state.doctor_signaturePreview}
                closeSignaturePreviewModal={this.closeSignaturePreviewModal}
                openSignaturePreviewModal={this.openSignaturePreviewModal}
                openSignatureModal={this.openSignatureModal}
                />

              <MultiSelectModal
                id='attendingPhysicians'
                items={attendingPhysicians}
                isSelectibleOpen={this.state.isSelectibleOpen}
                onSelectionsChange={this.onSelectionsChangePhysicians}
                onSearch={this.props.onDoctorSearch}
                updateField={this.props.updateField}
                closeModal={this.closeSelectibleModal}
                />

              <ListViewSelectModal
                isOpen={this.state.isOpen}
                onClosed={this.closeModalBox}
                dataLabOrder={this.state.dataLabOrder}
                typeData={'labOrder'}
                title={'Lab Order'}
                onSelectItem={this.onSelectItem}
                />

              <ListViewSelectModal
                isOpen={this.state.isOpen2}
                onClosed={this.closeModalBox2}
                dataLabOrder={this.state.dataLabImaging}
                typeData={'imaging'}
                title={'Imaging'}
                onSelectItem={this.onSelectItem}
                />

            </Modal>
          )
        }
      }
