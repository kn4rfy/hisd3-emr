import React, { Component } from 'react';
import { Dimensions, View, Alert, StyleSheet, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';

import ConsentForm from './ConsentForm';
import {requireAuthentication} from '../../../utils/RoleComponent';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class ConsentContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  rowRenderer = (rowData, sectionID, rowID) => {

    let rowNo = parseInt(rowID) + 1;

    return (
      <View style={{flexDirection: 'row'}}>
        <Item style={{flex: 0.8, borderBottomWidth: 0}}>
          <Input
            value={rowData.consent_title}
            editable={false}
            />
        </Item>

        <Button primary block onPress={this.props.viewConsentRecord(rowData)}
          style={{flex: 0.2}}>
          <Text>Preview</Text>
        </Button>
      </View>
    )
  }

  render() {
    return (
      <Container>
        <Content padder>
          <List
            dataArray={this.props.pdscs.activeRecord.consents || []}
            renderRow={(item) => this.rowRenderer(item)
            }>
          </List>

          {
            requireAuthentication((
              <Button primary block onPress={this.props.openConsentModal}>
                <Text>Add Consent</Text>
              </Button>
            ), this.props.auth.account, "ROLE_ER_NURSE")
          }
        </Content>

        {this.props.consentShowModal ? (
          <ConsentForm
            {...this.props}
            />
        ): null}
      </Container>
    );
  }
}
