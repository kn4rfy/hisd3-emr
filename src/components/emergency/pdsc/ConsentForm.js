import React, { Component } from 'react';
import { Dimensions, View, Modal, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';
import _ from 'lodash';

import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal';
import * as Config from '../../../config/Config';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class ConsentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dpam: false,
      otherProcedure: 'No Description',
      consenting: '',
      witness: '',
      consent_title: ''
    }

    this.relations = ['HUSBAND', 'WIFE', 'CHILD', 'RELATIVE', 'OTHERS'];
    this.consentTitles = ['DIAGNOSTIC PROCEDURES', 'ADMINISTRATION OF MEDICATION AND TREATMENT'];
  }

  componentDidMount(){
    this.syncPropsToState(this.props);
  }

  syncPropsToState=(props)=>{
    this.setState({
      consent_title: props.activeRecord.consent_title,
      name: props.activeRecord.name,
      relation_to_patient: props.activeRecord.relation_to_patient,
      reason_for_non_give: props.activeRecord.reason_for_non_give,
      witnessName: props.activeRecord.witnessName
    });
  }

  relationChange = (value) => {
    return()=>{
      let relPatient = value == 5 ? '' : this.relations[value];
      this.props.updateField('relation_to_patient', relPatient)
    }
  }

  openSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: true
    })
  }

  closeSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: false
    })
  }

  render() {
    let activeRecord = this.props.activeRecord;
    let isTitleNotOthers = this.consentTitles.indexOf(activeRecord.consent_title) > -1 ? true:false;
    let isPatientConsenting = _.toUpper(this.props.patients.patientInfo.fullname) == _.toUpper(activeRecord.name) ? true:false;
    let isRelationNotOthers = (this.relations.indexOf(activeRecord.relation_to_patient) > -1||activeRecord.relation_to_patient==null) ? true: false;

    let signatureImg = null;
    if(activeRecord.signature != null){
      signatureImg = `data:image/png;base64,`+activeRecord.signature;
    }else{
      if(activeRecord.hassignature){
        signatureImg = Config.getHost()+`/restapi/pdscConsentRecords/signature/${activeRecord.id}`;
      }
    }

    let witnessSignatureImg = null;
    if(activeRecord.witnessSignature != null){
      witnessSignatureImg = `data:image/png;base64,`+activeRecord.witnessSignature;
    }else{
      if(activeRecord.haswitnessSignature){
        witnessSignatureImg = Config.getHost()+`/restapi/pdscConsentRecords/witnessSignature/${activeRecord.id}`;
      }
    }

    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.consentShowModal||false}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={this.props.closeConsentModal}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title>Consent Form</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
            <Item stackedLabel>
              <Label>CONSENT TITLE</Label>
              {_.get(activeRecord, 'consent_title') && _.get(this.state, 'consent_title') ? null :
                (_.map(this.consentTitles, (value, idx) => {
                  return(
                    <ListItem key={idx} style={{flex: 1}}
                      selected={activeRecord.consent_title == value ? true:false}
                      onPress={() => {
                        let swValue = activeRecord.consent_title == value ? '' : value;
                        this.props.updateField('consent_title', swValue)}
                      }
                      disabled={activeRecord.consent_title == value && this.state.consent_title == value}>
                      <Text>{value}</Text>
                      <Right>
                        <Switch
                          onValueChange={() => {
                            let swValue = activeRecord.consent_title == value ? '' : value;
                            this.props.updateField('consent_title', swValue)}
                          }
                          value={activeRecord.consent_title == value ? true:false}
                          disabled={activeRecord.consent_title == value && this.state.consent_title == value}
                          />
                      </Right>
                    </ListItem>
                  );
                })
              )}

              {isTitleNotOthers ? _.get(activeRecord, 'consent_title') && _.get(this.state, 'consent_title') ?
                (_.map(this.consentTitles, (value, idx) => {
                  return(
                    <ListItem key={idx} style={{borderBottomWidth: 0}}
                      selected={activeRecord.consent_title == value ? true:false}
                      onPress={() => {
                        let swValue = activeRecord.consent_title == value ? '' : value;
                        this.props.updateField('consent_title', swValue)}
                      }
                      disabled={_.get(activeRecord, 'consent_title') && _.get(this.state, 'consent_title') ? true : false}>
                      <Text>{value}</Text>
                      <Right>
                        <Switch
                          onValueChange={() => {
                            let swValue = activeRecord.consent_title == value ? '' : value;
                            this.props.updateField('consent_title', swValue)}
                          }
                          value={activeRecord.consent_title == value ? true:false}
                          disabled={_.get(activeRecord, 'consent_title') && _.get(this.state, 'consent_title') ? true : false}
                          />
                      </Right>
                    </ListItem>
                  );
                })) : null : (
                  <Item style={{borderBottomWidth: 0}}>
                    <Input
                      onChangeText={(value) => this.props.updateField("consent_title", value)}
                      value={this.consentTitles.indexOf(activeRecord.consent_title) > -1?'':activeRecord.consent_title}
                      placeholder={'Other Procedure'}
                      editable={_.get(activeRecord, 'consent_title') && _.get(this.state, 'consent_title') ? false : true}
                      />
                  </Item>
                )
              }
            </Item>

            <Item stackedLabel>
              <Label>CONSENTING PERSON</Label>
              <ListItem style={{flex: 0.3, borderBottomWidth: 0}}
                selected={isPatientConsenting}
                onPress={() => {
                  let consentNameVal = isPatientConsenting ? "" : this.props.patients.patientInfo.fullname
                  this.props.updateField("name", consentNameVal)
                  this.props.updateField("signature", null)
                  this.props.updateField("relation_to_patient", null)
                  this.props.updateField("reason_for_non_give", null)}
                }
                disabled={_.get(activeRecord, 'name', '') != '' && activeRecord.name == this.state.name}>
                <Text>Same as patient?</Text>
                <Right>
                  <Switch
                    onValueChange={() => {
                      let consentNameVal = isPatientConsenting ? "" : this.props.patients.patientInfo.fullname
                      this.props.updateField("name", consentNameVal)
                      this.props.updateField("signature", null)
                      this.props.updateField("relation_to_patient", null)
                      this.props.updateField("reason_for_non_give", null)}
                    }
                    value={isPatientConsenting}
                    disabled={_.get(activeRecord, 'name', '') != '' && activeRecord.name == this.state.name}
                    />
                </Right>
              </ListItem>

              <View style={{flexDirection: 'row'}}>
                <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                  <Input
                    onChangeText={(value) => this.props.updateField("name", value)}
                    value={activeRecord.name}
                    placeholder={'Consenting Person'}
                    editable={_.get(activeRecord, 'name') && _.get(this.state, 'name') ? false : true}
                    />
                </Item>

                {signatureImg!=null?
                  (
                    <Button warning block onPress={()=>this.openSignaturePreviewModal('signature')}
                      style={{flex: 0.3}}>
                      <Text>Preview</Text>
                    </Button>
                  ):(
                    <Button danger block onPress={()=>this.props.openSignatureModal('signature')}
                      style={{flex: 0.3}}>
                      <Text>Signature</Text>
                    </Button>
                  )
                }
              </View>
            </Item>

            {isPatientConsenting? null :(
              <View>
                <Item stackedLabel>
                  <Label>RELATION TO PATIENT</Label>

                  <Item style={{flexDirection: 'column', borderBottomWidth: 0}}>
                    <View style={{flexDirection: 'row'}}>
                      <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                        selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 1 ? true:false}
                        onPress={this.relationChange(1)}
                        disabled={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? true : false}>
                        <Text>HUSBAND</Text>
                        <Right>
                          <Radio selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 1 ? true:false} />
                        </Right>
                      </ListItem>

                      <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                        selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 2 ? true:false}
                        onPress={this.relationChange(2)}
                        disabled={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? true : false}>
                        <Text>WIFE</Text>
                        <Right>
                          <Radio selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 2 ? true:false} />
                        </Right>
                      </ListItem>

                      <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                        selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 3 ? true:false}
                        onPress={this.relationChange(3)}
                        disabled={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? true : false}>
                        <Text>CHILD</Text>
                        <Right>
                          <Radio selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 3 ? true:false} />
                        </Right>
                      </ListItem>

                      <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                        selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 4 ? true:false}
                        onPress={this.relationChange(4)}
                        disabled={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? true : false}>
                        <Text>RELATIVE</Text>
                        <Right>
                          <Radio selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 4 ? true:false} />
                        </Right>
                      </ListItem>

                      <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                        selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 5 ? true:false}
                        onPress={this.relationChange(5)}
                        disabled={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? true : false}>
                        <Text>OTHERS</Text>
                        <Right>
                          <Radio selected={(activeRecord.relation_to_patient != null ? (this.relations.indexOf(activeRecord.relation_to_patient) > 0 ? this.relations.indexOf(activeRecord.relation_to_patient):5):0) == 5 ? true:false} />
                        </Right>
                      </ListItem>
                    </View>
                  </Item>
                </Item>

                {isRelationNotOthers? null : (
                  <Item>
                    <Input
                      onChangeText={(value) => this.props.updateField('relation_to_patient', value)}
                      value={activeRecord.relation_to_patient || ''}
                      placeholder={'Relation to Patient'}
                      editable={_.get(activeRecord, 'relation_to_patient') && _.get(this.state, 'relation_to_patient') ? false : true}
                      />
                  </Item>
                )}

                <Item stackedLabel>
                  <Label>REASON WHY PATIENT CANNOT GIVE CONSENT:</Label>
                  <View style={{ flexDirection: 'row' }}>
                    <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                      <Input
                        onChangeText={(value) => this.props.updateField('reason_for_non_give', value)}
                        value={activeRecord.reason_for_non_give || ''}
                        placeholder={'Reason'}
                        editable={_.get(activeRecord, 'reason_for_non_give') && _.get(this.state, 'reason_for_non_give') ? false : true}
                        />
                    </Item>
                  </View>
                </Item>
              </View>
            )
          }

          <Item stackedLabel style={{borderBottomWidth: 0}}>
            <Label>WITNESS</Label>
            <View style={{ flexDirection: 'row' }}>
              <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                <Input
                  onChangeText={(value) => this.props.updateField("witnessName", value)}
                  value={activeRecord.witnessName}
                  placeholder={'Witness'}
                  editable={_.get(activeRecord, 'witnessName') && _.get(this.state, 'witnessName') ? false : true}
                  />
              </Item>

              {witnessSignatureImg != null?
                (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('witnessSignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ):(
                  <Button danger block onPress={()=>this.props.openSignatureModal('witnessSignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )
              }
            </View>
          </Item>

          <View style={{flexDirection: 'row'}}>
            <Button danger block onPress={this.props.closeConsentModal}
              style={{flex: 0.4}}>
              <Text>Close</Text>
            </Button>
            <View style={{flex: 0.2}}></View>
            <Button success block onPress={this.props.saveConsentChanges}
              style={{flex: 0.4}}>
              <Text>Submit</Text>
            </Button>
          </View>

        </Content>

        <SignatureModal
          id='signature'
          isSignatureOpen={this.props.signature}
          closeSignatureModal={this.props.closeSignatureModal}
          openSignatureModal={this.props.openSignatureModal}
          updateField={this.props.updateField}
          signatureOf="Consenting Person"
          />
        <SignatureModal
          id='witnessSignature'
          isSignatureOpen={this.props.witnessSignature}
          closeSignatureModal={this.props.closeSignatureModal}
          openSignatureModal={this.props.openSignatureModal}
          updateField={this.props.updateField}
          signatureOf="Witness"
          />
        <SignaturePreviewModal
          id='signature'
          imgSrc={signatureImg}
          previewSignOpen={this.state.signaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <SignaturePreviewModal
          id='witnessSignature'
          imgSrc={witnessSignatureImg}
          previewSignOpen={this.state.witnessSignaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
      </Container>
    </Modal>
  );
}
}
