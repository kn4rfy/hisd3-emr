import React, { Component } from 'react';
import { Dimensions, View, Modal } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';
import moment from 'moment';
import _ from 'lodash';
import ModalPicker from 'react-native-modal-picker';
import Spinner from 'react-native-loading-spinner-overlay';

import {Actions} from 'react-native-router-flux';
import {post,get} from '../../../utils/RestClient';
import {requireAuthentication} from '../../../utils/RoleComponent';

import * as Config from '../../../config/Config';
import MultiSelectModal from '../../general/MultiSelectModal';
import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class DischargeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signatureShowModal: false
    }
  }

  openSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: true
    })
  }

  closeSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: false
    })
  }

  render() {
    let activeRecord = this.props.pdscs.discharge.activeRecord;
    let doctors = _.map(this.props.doctors, (doctor, idx) => {
      return {
        key: idx,
        label: doctor.label,
        value: doctor.value
      }
    });
    let selDiscIdx = _.findIndex(this.props.doctors, {
      value: activeRecord.discharge_recommended_by
    });
    let selDiscName = !_.isEmpty(this.props.doctors[selDiscIdx]) ? this.props.doctors[selDiscIdx].label : '';

    let nurses = _.map(this.props.nurses, (nurse, idx) => {
      return {
        key: idx,
        label: nurse.label,
        value: nurse.value
      }
    });
    let selHmneIdx = _.findIndex(this.props.nurses, {
      value: activeRecord.home_medication_nurse_explainer
    });
    let selHmneName = !_.isEmpty(this.props.nurses[selHmneIdx]) ? this.props.nurses[selHmneIdx].label : '';

    let discharge_recommended_bySignatureImg = null;
    if(activeRecord.discharge_recommended_bySignature != null){
      discharge_recommended_bySignatureImg = `data:image/png;base64,`+activeRecord.discharge_recommended_bySignature;
    }else{
      if(activeRecord.hasdischarge_recommended_bySignature){
        //discharge_recommended_bySignatureImg = Config.getHost()+`/restapi/pdsc/hasdischarge_recommended_bySignature/${activeRecord.id}`;
      }
    }

    let instructionsBySignatureImg = null;
    if(activeRecord.instructionsBySignature != null){
      instructionsBySignatureImg = `data:image/png;base64,`+activeRecord.instructionsBySignature;
    }else{
      if(activeRecord.hasinstructionsBySignature){
        //instructionsBySignatureImg = Config.getHost()+`/restapi/pdsc/instructionsBySignature/${activeRecord.id}`;
      }
    }

    let acknowledged_bySignatureImg = null;
    if(activeRecord.acknowledged_bySignature != null){
      acknowledged_bySignatureImg = `data:image/png;base64,`+activeRecord.acknowledged_bySignature;
    }else{
      if(activeRecord.hasacknowledged_bySignature){
        //acknowledged_bySignatureImg = Config.getHost()+`/restapi/pdsc/acknowledged_bySignature/${activeRecord.id}`;
      }
    }

    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.showDischargeModal||false}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={this.props.closeDischargeModal}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title>ER Discharge Notice</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>CLINICAL DIAGNOSIS</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('clinical_diagnosis', value)}
                    value={activeRecord.clinical_diagnosis || ''}
                    placeholder={'Clinical Diagnosis'}
                    multiline
                    onContentSizeChange={(event) => {
                        this.setState({clinical_diagnosis_height: event.nativeEvent.contentSize.height});
                    }}
                    style={{height: Math.max(80, this.state.clinical_diagnosis_height)}}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>HOME MEDICATION INSTRUCTIONS</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('home_medication_instructions', value)}
                    value={activeRecord.home_medication_instructions || ''}
                    placeholder={'Home Medication Instructions'}
                    multiline
                    onContentSizeChange={(event) => {
                        this.setState({home_medication_instructions_height: event.nativeEvent.contentSize.height});
                    }}
                    style={{height: Math.max(80, this.state.home_medication_instructions_height)}}
                    />
                </Item>
              </View>
            </Item>

            <ListItem>
              <Title>SPECIFIC INSTRUCTIONS</Title>
            </ListItem>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>HEAD INJURY</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('head_injury_instructions', value)}
                    value={activeRecord.head_injury_instructions || ''}
                    placeholder={'Specific Instructions for Head Injury'}
                    multiline
                    onContentSizeChange={(event) => {
                        this.setState({head_injury_instructions_height: event.nativeEvent.contentSize.height});
                    }}
                    style={{height: Math.max(80, this.state.head_injury_instructions_height)}}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>LACERATED SATURED WOUND</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('lac_sut_wound_instructions', value)}
                    value={activeRecord.lac_sut_wound_instructions || ''}
                    placeholder={'Specific Instructions for Lacerated Sutured Wound'}
                    multiline
                    onContentSizeChange={(event) => {
                        this.setState({lac_sut_wound_instructions_height: event.nativeEvent.contentSize.height});
                    }}
                    style={{height: Math.max(80, this.state.lac_sut_wound_instructions_height)}}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>OTHERS</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('other_instructions', value)}
                    value={activeRecord.other_instructions || ''}
                    placeholder={'Other Instructions (Diet, lab results, special precautions, daily wound care, arm sling, etc)'}
                    multiline
                    onContentSizeChange={(event) => {
                        this.setState({other_instructions_height: event.nativeEvent.contentSize.height});
                    }}
                    style={{height: Math.max(80, this.state.other_instructions_height)}}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>FOLLOW UP SCHEDULE</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('follow_up_schedule', value)}
                    value={activeRecord.follow_up_schedule || ''}
                    placeholder={'Follow up schedule'}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>PHYSICIAN OF CHOICE</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item regular style={{flex: 1}}>
                  <Input
                    onChangeText={(value) => this.props.updateField('physcian_of_choice', value)}
                    value={activeRecord.physcian_of_choice || ''}
                    placeholder={'Physician of Choice'}
                    />
                </Item>
              </View>
            </Item>

            <Item stackedLabel>
              <Label>RECOMMENED FOR DISCHARGE BY</Label>
              <View style={{ flexDirection: 'row' }}>
                <ModalPicker
                  data={doctors}
                  initValue="Discharge by"
                  onChange={(doctorSel)=>{ this.props.updateField('discharge_recommended_by', doctorSel.value)}}
                  style={{flex: 1}}>
                  <Input
                    value={selDiscName}
                    placeholder={'Recommeded for Discharge by'}
                    editable={false}
                    />
                </ModalPicker>

                {discharge_recommended_bySignatureImg != null? (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('discharge_recommended_bySignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ) : (
                  <Button danger block onPress={()=>this.props.openSignatureModal('discharge_recommended_bySignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )}
              </View>
            </Item>

            <Item stackedLabel>
              <Label>HOME MEDICATION INSTRUCTIONS EXPLAINED BY</Label>
              <View style={{ flexDirection: 'row' }}>
                <ModalPicker
                  data={nurses}
                  initValue="Explained by"
                  onChange={(nurses)=>{ this.props.updateField('home_medication_nurse_explainer', nurses.value)}}
                  style={{flex: 1}}>
                  <Input
                    value={selHmneName}
                    placeholder={'Home Medication Instructions Explained by'}
                    editable={false}
                    />
                </ModalPicker>

                {instructionsBySignatureImg != null? (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('instructionsBySignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ) : (
                  <Button danger block onPress={()=>this.props.openSignatureModal('instructionsBySignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )}
              </View>
            </Item>

            <Item stackedLabel style={{borderBottomWidth: 0}}>
              <Label>ACKNOWLEDGED BY (PATIENT)</Label>
              <View style={{ flexDirection: 'row' }}>
                <Item style={{flex: 1, borderBottomWidth: 0}}>
                  <Input
                    value={this.props.patients.patientInfo.fullname}
                    placeholder={'Acknowledged by (Patient)'}
                    editable={false}
                    />
                </Item>

                {acknowledged_bySignatureImg != null? (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('acknowledged_bySignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ) : (
                  <Button danger block onPress={()=>this.props.openSignatureModal('acknowledged_bySignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )}
              </View>
            </Item>

            <View style={{flexDirection: 'row'}}>
              <Button success block onPress={()=>this.props.submitDischargeForm(activeRecord)}
                style={{flex: 0.5}}>
                <Text>Submit</Text>
              </Button>
              <Button danger block onPress={this.props.closeDischargeModal}
                style={{flex: 0.5}}>
                <Text>Close</Text>
              </Button>
            </View>

          </Content>

          <SignatureModal
            id='discharge_recommended_bySignature'
            isSignatureOpen={this.props.discharge_recommended_bySignature}
            closeSignatureModal={this.props.closeSignatureModal}
            openSignatureModal={this.props.openSignatureModal}
            updateField={this.props.updateField}
            signatureOf="Discharge Recommended By"
            />
          <SignatureModal
            id='instructionsBySignature'
            isSignatureOpen={this.props.instructionsBySignature}
            closeSignatureModal={this.props.closeSignatureModal}
            openSignatureModal={this.props.openSignatureModal}
            updateField={this.props.updateField}
            signatureOf="Instructions by"
            />
          <SignatureModal
            id='acknowledged_bySignature'
            isSignatureOpen={this.props.acknowledged_bySignature}
            closeSignatureModal={this.props.closeSignatureModal}
            openSignatureModal={this.props.openSignatureModal}
            updateField={this.props.updateField}
            signatureOf="Acknowledged by"
            />

          {/* {this.props.createSignaturePreviewModal('discharge_recommended_bySignature', 'discharge')}
          {this.props.createSignaturePreviewModal('instructionsBySignature', 'discharge')}
          {this.props.createSignaturePreviewModal('acknowledged_bySignature', 'discharge')} */}

          <SignaturePreviewModal
            id='discharge_recommended_bySignature'
            imgSrc={discharge_recommended_bySignatureImg}
            previewSignOpen={this.state.discharge_recommended_bySignaturePreview}
            closeSignaturePreviewModal={this.closeSignaturePreviewModal}
            openSignaturePreviewModal={this.openSignaturePreviewModal}
            openSignatureModal={this.props.openSignatureModal}
            />

          <SignaturePreviewModal
            id='instructionsBySignature'
            imgSrc={instructionsBySignatureImg}
            previewSignOpen={this.state.instructionsBySignaturePreview}
            closeSignaturePreviewModal={this.closeSignaturePreviewModal}
            openSignaturePreviewModal={this.openSignaturePreviewModal}
            openSignatureModal={this.props.openSignatureModal}
            />

          <SignaturePreviewModal
            id='acknowledged_bySignature'
            imgSrc={acknowledged_bySignatureImg}
            previewSignOpen={this.state.acknowledged_bySignaturePreview}
            closeSignaturePreviewModal={this.closeSignaturePreviewModal}
            openSignaturePreviewModal={this.openSignaturePreviewModal}
            openSignatureModal={this.props.openSignatureModal}
            />
        </Container>
      </Modal>
    );
  }
}
