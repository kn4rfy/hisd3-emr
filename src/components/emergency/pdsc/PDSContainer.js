import React, { Component } from 'react';
import { Dimensions, View, Alert, StyleSheet, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';
import moment from 'moment';
import _ from 'lodash';

import {connect} from 'react-redux';
import {Actions, Scene, ActionConst} from 'react-native-router-flux';
import {bindActionCreators} from 'redux';

import * as authActions from '../../../actions/authActions';
import * as healthchecks from '../../../actions/healthchecks';
import * as pdscActions from '../../../actions/pdscactions';
import {get, post, put, patch, _delete} from '../../../utils/RestClient';
import * as Config from '../../../config/Config';

import PatientInfo from '../../PatientInfo';
import PatientDataSheet from './PatientDataSheet';
import ConsentContainer from './ConsentContainer';
import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal'

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class PDSContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      page:'second',
      employees: [],
      erConsultants: [],
      attendingPhysicians: [],
      erDoctors: [],
      nurses: [],
      showDischargeModal: false,
      consentShowModal: false
    };
  }

  componentDidMount() {
    this.getDoctorByFilter('');
    this.getNurseByFilter('');
    this.getAttendingPhysByFilter('');
    this.getEmployeesByFilter('');
  }

  getDoctorByFilter = (filter) => {
    get(Config.getHost()+'/restapi/employees/search/findAllByFilterAndTypeAndDoc',{
      params:{
        filter,
        careProviderType: "doctor",
        erDoctor: true
      }
    }).then((response) => {
      if(response.data._embedded && response.data._embedded.employees){

        var erConsultants = response.data._embedded.employees.map((item) => {
          return {
            label: item.fullname,
            value: item._links.self.href,
            employeeId: item.id
          }
        });

        this.setState({
          erConsultants
        });
      }

    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getDoctorByFilter', error);
      }
    });
  }

  getAttendingPhysByFilter = (filter) => {
    get(Config.getHost()+'/restapi/employees/search/findAllByFilterAndTypeAndDoc',{
      params:{
        filter,
        careProviderType: "doctor",
        attendingPhysician: true
      }
    }).then((response) => {
      if(response.data._embedded && response.data._embedded.employees){

        var attendingPhysicians = response.data._embedded.employees.map((item) => {
          return {
            label: item.fullname,
            value: item._links.self.href,
            employeeId: item.id
          }
        });

        this.setState({
          attendingPhysicians
        });
      }

    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getAttendingPhysByFilter', error);
      }
    });
  }

  getNurseByFilter = (filter) => {
    get(Config.getHost()+'/restapi/employees/search/findAllByFilterAndType',{
      params:{
        filter,
        careProviderType: "nurse"
      }
    }).then((response) => {
      if(response.data._embedded && response.data._embedded.employees){
        var nurses = response.data._embedded.employees.map((item) => {
          return {
            label: item.fullname,
            value: item._links.self.href
          }
        });

        this.setState({
          nurses
        });

      }
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getNurseByFilter', error);
      }
    });
  }

  getEmployeesByFilter = (filter) => {
    get(Config.getHost()+'/restapi/employees/search/findAllByPositionAndDepartment',{
      params:{
        filter,
        position: "",
        department: ""
      }
    }).then((response) => {
      if(response.data._embedded && response.data._embedded.employees){
        var employees = response.data._embedded.employees.map((item) => {
          return {
            label: item.fullname,
            value: item._links.self.href
          }
        });

        this.setState({
          employees
        });

      }
    }).catch((error)=>{
      if(__DEV__) {
        console.log('error getEmployeesByFilter', error);
      }
    });
  }

  onDoctorSearch = (term) => {
    if(this.timeoutHandle)
    clearTimeout(this.timeoutHandle);

    this.timeoutHandle = setTimeout(()=>{
      // this.getAttendingPhysByFilter(term);
      this.getDoctorByFilter(term);
      this.timeoutHandle = undefined;
    }, 500);
  }

  updateField = (name, value)=>{
    var payload = {};
    if(name == 'timeEntry'){
      let dateEntry = this.props.pdscs.activeRecord.datetimeEntry ? moment(this.props.pdscs.activeRecord.datetimeEntry).format("YYYY-MM-DD") : moment().format("YYYY-MM-DD");

      payload["datetimeEntry"] = dateEntry+" "+value;
    }else{
      payload[name]=value;
    }
    this.props.pdscActions.updatePDSCInput(payload);
  }

  removeBase64Prefix = (strVal) => {
    return _.replace(strVal, "data:image/png;base64,", "");
  }

  updateConsentInput = (name, value) => {
    var payload = {};
    payload[name] = value;
    this.props.pdscActions.updateConsentInput(payload);
  }

  onFormSubmit = () => {
    let data = this.props.pdscs.activeRecord;
    data.status = "ER";
    if(data.dischargeNoticeNo == "") data.dischargeNoticeNo = null;
    if(_.get(data, 'datetimeEntry')){
      data.datetimeEntry = moment(data.datetimeEntry).format('YYYY-MM-DDTHH:mm:ss');
    } else {
      data.datetimeEntry = moment().format('YYYY-MM-DDTHH:mm:ss');
    }

    data.patient = this.props.patients.patientInfo._links.self.href;
    data.fromPath = this.fromPath;
    if(data.triagelevel == null){
      data.triagelevel = this.props.activeTriage? this.props.activeTriage+1:null;
    }else{
      _.replace(data.triagelevel, "Level ", "");
    }

    let currInsuranceIds = [];
    let activeInsuranceIds = []
    if(data.insurances){
      activeInsuranceIds = data.insurances.map((insurance) => {
        return insurance.patientInsuranceId
      });
    }

    if(this.props.pdscs.pdscInfo.insurances){
      currInsuranceIds = this.props.pdscs.pdscInfo.insurances.map((insurance) => {
        return insurance.patientInsuranceId
      });
    }
    let idsToDelete = _.difference(currInsuranceIds, activeInsuranceIds);
    let idsToAdd = _.difference(activeInsuranceIds, currInsuranceIds);
    idsToDelete = idsToDelete.map(id => {
      let idx = _.findIndex(this.props.pdscs.pdscInfo.insurances, {patientInsuranceId: id});
      return this.props.pdscs.pdscInfo.insurances[idx].id
    });
    data.insuranceUpdate = {
      toAdd: idsToAdd,
      toDelete: idsToDelete
    };

    let currAttendingPhyIds = [];
    let activeAttendingPhyIds = [];

    if(data.attendingPhysicians){
      activeAttendingPhyIds = _.map(data.attendingPhysicians, (physician) => {
        let physIdx = _.findIndex(this.state.attendingPhysicians, {value:physician.value?physician.value:`${Config.getHost()}${physician.attendingPhysicianLink}`});
        return this.state.attendingPhysicians[physIdx].employeeId;
      });
    }
    if(this.props.pdscs.pdscInfo.attendingPhysicians){
      currAttendingPhyIds = _.map(this.props.pdscs.pdscInfo.attendingPhysicians, (physician) => {
        let physIdx = _.findIndex(this.state.attendingPhysicians, {value:`${Config.getHost()}${physician.attendingPhysicianLink}`});
        return this.state.attendingPhysicians[physIdx].employeeId;
      });
    }

    let apIdsToDelete = _.difference(currAttendingPhyIds, activeAttendingPhyIds);
    apIdsToDelete = apIdsToDelete.map(id => {
      let idx = _.findIndex(this.props.pdscs.pdscInfo.attendingPhysicians, {employeeId: id});
      return this.props.pdscs.pdscInfo.attendingPhysicians[idx].id
    });
    let apIdsToAdd = _.difference(activeAttendingPhyIds, currAttendingPhyIds);
    data.attendingPhysicianUpdate = {
      toAdd: apIdsToAdd,
      toDelete: apIdsToDelete
    }

    let currErDoctorIds = [];
    let activeErDoctorIds = [];

    if(data.erDoctors){
      activeErDoctorIds = _.map(data.erDoctors, (doctor) => {
        let docIdx = _.findIndex(this.state.erConsultants, {value:doctor.value?doctor.value:`${Config.getHost()}${doctor.erDoctorLink}`});
        return this.state.erConsultants[docIdx].employeeId;
      });
    }

    if(this.props.pdscs.pdscInfo.erDoctors){
      currErDoctorIds = _.map(this.props.pdscs.pdscInfo.erDoctors, (doctor) => {
        let docIdx = _.findIndex(this.state.erConsultants, {value:`${Config.getHost()}${doctor.erDoctorLink}`});
        return this.state.erConsultants[docIdx].employeeId;
      });
    }

    let edIdsToDelete = _.difference(currErDoctorIds, activeErDoctorIds);
    edIdsToDelete = edIdsToDelete.map(id => {
      let idx = _.findIndex(this.props.pdscs.pdscInfo.erDoctors, {employeeId: id});
      return this.props.pdscs.pdscInfo.erDoctors[idx].id
    });
    let edIdsToAdd = _.difference(activeErDoctorIds, currErDoctorIds);

    let signatureData = {};

    _.map(this.activeErDoctors, (doctor, idx)=>{
      let docIdx = _.findIndex(this.state.erConsultants, {value:doctor.value?doctor.value:`${Config.getHost()}${doctor.erDoctorLink}`});
      let docEmpId = this.state.erConsultants[docIdx].employeeId;

      let recidx = _.findIndex(this.props.pdscs.pdscInfo.erDoctors, {employeeId: docEmpId});
      let recordId = recidx > -1 ? this.props.pdscs.pdscInfo.erDoctors[recidx].id:null;

      let signature = null;
      if(data[`erConsultant${docEmpId}Signature`]){
        signature = this.removeBase64Prefix(data[`erConsultant${docEmpId}Signature`]);
      }else{
        if(recidx > -1){
          if(this.props.pdscs.pdscInfo.erDoctors[recidx].hassignature == true){
            signature = 'skip';
          }
        }
      }

      signatureData[docEmpId] = {
        recordId,
        signature
      }
    })

    data.erDoctorUpdate = {
      toAdd: edIdsToAdd,
      toDelete: edIdsToDelete,
      signatureData
    }

    this.props.pdscActions.upsertPDSC(data, this.onUpsert);
  }

  onUpsert = () => {
    let textVal = this.props.pdscs.activeRecord.id ? "Updated":"Inserted";
    Alert.alert(
      'Success!',
      `Data ${textVal} Successfully`,
      [
        {text: 'OK', onPress: ()=>{
          this.props.pdscActions.getPatientAndPDSCInfo(this.props.patients.patientInfo.id);
          Actions.patientmenu({patientId: this.props.patients.patientInfo.id, type: ActionConst.POP_AND_REPLACE});
        }}
      ]
    )
  }

  onDeleteRecord = (pdscId) => {
    return () => {
      Alert.alert(
        'Confirm Deletion',
        'Please confirm to delete record',
        [
          {text: 'Yes', onPress: () => this.deleteRecord(pdscId)},
          {text: 'No', onPress: () => {}},
        ]
      )
    }
  }

  deleteRecord = (pdscId) => {
    this.props.pdscActions.deletePDSC(pdscId, this.onDeletePdsc);
  }

  onDeletePdsc = () => {
    Alert.alert(
      'Success!',
      `Record Removed Successfully`,
      [
        {text: 'OK', onPress: ()=>{
          // this.getPatientAndPdscsInfo();
          this.props.pdscActions.getPatientAndPDSCInfo(this.props.patients.patientInfo.id);
          Actions.patientmenu({patientId: this.props.patients.patientInfo.id, type: ActionConst.POP_AND_REPLACE});
        }}
      ]
    )

  }

  submitDischargeForm = (dischargeData) => {
    Alert.alert(
      'Confirm Discharge',
      'Please confirm to discharge',
      [
        {text: 'Yes', onPress: () => {
          dischargeData.pdsc = `${Config.getHost()}/restapi/pdscs/${this.props.pdscs.activeRecord.id}`;
          dischargeData.acknowledged_by = `${Config.getHost()}/restapi/patients/${this.props.patients.patientInfo.id}`;
          dischargeData.discharge_datetime = moment(dischargeData.discharge_datetime).format('YYYY-MM-DDTHH:mm:ss');
          this.props.pdscActions.upsertDischargeForm(dischargeData, this.dischargePdsc);
        }},
        {text: 'No', onPress: () => {}},
      ]
    )
  }

  dischargePdsc = () => {
    this.props.pdscActions.dischargePdscById(this.props.pdscs.activeRecord.id, this.onPdscDischarge);
  }

  onPdscDischarge = () => {
    this.props.pdscActions.updatePDSCInput('clear');
    this.props.pdscActions.updateDischargeInput('clear');
    Alert.alert(
      'Success!',
      `Patient Discharged Successfully`,
      [
        {text: 'OK', onPress: ()=>{
          this.setState({
            showDischargeModal: false
          }, () => {
            this.props.pdscActions.getPatientAndPDSCInfo(this.props.patients.patientInfo.id);
            Actions.patientmenu({patientId: this.props.patients.patientInfo.id, type: ActionConst.POP_AND_REPLACE});
          });
        }}
      ]
    )
  }

  openDischargeModal = () => {
    this.setState({
      showDischargeModal: true
    })
  }

  closeDischargeModal = () => {
    this.setState({
      showDischargeModal: false
    })
  }

  getPatientAndPdscsInfo = () => {
    let pdscId = this.props.pdscs.pdscInfo.id || this.props.pdscId;
    this.props.pdscActions.getPatientAndPDSCInfo(this.state.patientId, pdscId);
  }

  updateDischargeInput = (name, value) => {
    var payload = {};
    payload[name]=value;
    this.props.pdscActions.updateDischargeInput(payload);
  }

  saveConsentChanges = () => {
    let payload = {};

    let consentData = this.state.editedConsent;
    if(_.isEmpty(consentData)){
      let newConsentData = this.props.pdscs.consent.activeRecord;
      payload['consents'] = _.isEmpty(this.props.pdscs.activeRecord.consents) ? [newConsentData] : _.concat(this.props.pdscs.activeRecord.consents, newConsentData);
    }else{
      var consNewList = _.assign([],this.props.pdscs.activeRecord.consents);
      let consNewIdx =  _.findIndex(this.props.pdscs.activeRecord.consents, (c)=>{
        return c.consent_title == this.state.editedConsent.consent_title;
      });
      if(consNewIdx != -1){
        consNewList[consNewIdx] = {...this.props.pdscs.consent.activeRecord};
        payload['consents'] = consNewList;
      }
    }

    this.props.pdscActions.updatePDSCInput(payload);
    this.closeConsentModal();
  }

  openConsentModal = () => {
    this.setState({
      consentShowModal: true
    })
  }

  closeConsentModal = () => {
    this.setState({
      consentShowModal:false,
      editedConsent: {}
    },()=>{
      this.props.pdscActions.updateConsentInput('clear');
    });
  }

  viewConsentRecord = (consentData) => {
    return () => {
      this.setState({
        editedConsent: consentData
      }, () => {
        this.props.pdscActions.updateConsentInput(consentData);
        this.openConsentModal();
      });
    }
  }

  deleteConsentRecord = (consentData) => {
    return () => {
      Alert.alert(
        'Confirm Deletion!',
        `Please confirm to delete record`,
        [
          {text: 'Yes', onPress: () => {
            _.remove(this.props.pdscs.activeRecord.consents, consentData);
            let payload = {};
            payload['consents'] = this.props.pdscs.activeRecord.consents;

            if(consentData.id){
              payload["consentsToDelete"] = this.props.pdscs.activeRecord.consentsToDelete ? _.concat(this.props.pdscs.activeRecord.consentsToDelete, consentData) : [consentData];
            }
            this.props.pdscActions.updatePDSCInput(payload);
          }},
          {text: 'No', onPress: () => {}},
        ]
      )
    }
  }

  openSignatureModal = (name) => {
    this.setState({
      [name]: true
    })
  }

  closeSignatureModal = (name) => {
    this.setState({
      [name]: false
    })
  }

  createSignatureModal = (id, signatureOf=null) => {
    return (
      <SignatureModal
        id={id}
        isSignatureOpen={this.state[id]}
        closeSignatureModal={this.closeSignatureModal}
        openSignatureModal={this.openSignatureModal}
        updateField={this.updateField}
        signatureOf={signatureOf}
        />
    )
  }

  openSignaturePreviewModal = (name, imgSrc=null) => {
    name = name+'Preview';
    this.setState({
      [name]: true,
      [name+'Image']: imgSrc
    })
  }

  closeSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: false
    })
  }

  createSignaturePreviewModal = (id) => {
    return (
      <SignaturePreviewModal
        id={id}
        imgSrc={this.props.imgSrc}
        previewSignOpen={this.state[id+'Preview']}
        closeSignaturePreviewModal={this.closeSignaturePreviewModal}
        openSignaturePreviewModal={this.openSignaturePreviewModal}
        openSignatureModal={this.openSignatureModal}
        />
    )
  }

  render() {
    this.activeInsurances = [];

    if(this.props.pdscs.activeRecord.insurances){
      this.activeInsurances = _.map(this.props.pdscs.activeRecord.insurances, (insurance, idx) => {
        if(!insurance.label){
          return {
            label: `${insurance.companyName}(${insurance.companyNameAbbv}): ${insurance.insuranceIdNo}`,
            value: insurance.patientInsuranceLink,
            patientInsuranceId: insurance.patientInsuranceId
          }
        }else{
          return insurance;
        }
      });
    }

    this.activeAttendingPhysicians = [];

    if(this.props.pdscs.activeRecord.attendingPhysicians){
      this.activeAttendingPhysicians = _.map(this.props.pdscs.activeRecord.attendingPhysicians, (physician, idx) => {
        if(!physician.label){
          return {
            label: physician.attendingPhysicianName,
            value: `${Config.getHost()}${physician.attendingPhysicianLink}`,
            employeeId: physician.employeeId
          }
        }else{
          return physician;
        }
      });
    }

    this.activeErDoctors = [];

    if(this.props.pdscs.activeRecord.erDoctors){
      this.activeErDoctors = _.map(this.props.pdscs.activeRecord.erDoctors, (doctor, idx) => {
        if(!doctor.label){
          return {
            label: doctor.erDoctorName,
            value: `${Config.getHost()}${doctor.erDoctorLink}`,
            employeeId: doctor.employeeId
          }
        }else{
          return doctor;
        }
      });
    }

    return (
      <Container>
        <Header hasTabs>
          <Left>
            <Button transparent onPress={()=>Actions.pop()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Patient Data Sheet and Consents</Title>
          </Body>
          <Right />
        </Header>
        <Content scrollEnabled={false} contentContainerStyle={{flex: 1}}>
          <PatientInfo {...this.props} />
          <Tabs>
            <Tab heading="Data Sheet" style={{flex: 1}}>
              <PatientDataSheet {...this.props}
                updateField={this.updateField}
                doctors={this.state.erConsultants}
                attendingPhysicians={this.state.attendingPhysicians}
                nurses={this.state.nurses}
                employees={this.state.employees}
                onDoctorSearch={this.onDoctorSearch}
                onFormSubmit={this.onFormSubmit}
                onDeleteRecord={this.onDeleteRecord}
                onPdscDischarge={this.onPdscDischarge}
                submitDischargeForm={this.submitDischargeForm}
                showDischargeModal={this.state.showDischargeModal}
                openDischargeModal={this.openDischargeModal}
                closeDischargeModal={this.closeDischargeModal}
                closeSignatureModal={this.closeSignatureModal}
                openSignatureModal={this.openSignatureModal}
                createSignatureModal={this.createSignatureModal}
                openSignaturePreviewModal={this.openSignaturePreviewModal}
                createSignaturePreviewModal={this.createSignaturePreviewModal}
                activeTriage={this.props.activeTriage}
                activeInsurances={this.activeInsurances}
                activeErDoctors={this.activeErDoctors}
                activeAttendingPhysicians={this.activeAttendingPhysicians}
                {...this.state}
                />
            </Tab>
            <Tab heading="Consents" style={{flex: 1}}>
              <ConsentContainer
                {...this.props}
                updateField={this.updateConsentInput}
                activeRecord={this.props.pdscs.consent.activeRecord}
                updateDischargeInput={this.updateDischargeInput}
                saveConsentChanges={this.saveConsentChanges}
                consentShowModal={this.state.consentShowModal}
                openConsentModal={this.openConsentModal}
                closeConsentModal={this.closeConsentModal}
                viewConsentRecord={this.viewConsentRecord}
                deleteConsentRecord={this.deleteConsentRecord}
                closeSignatureModal={this.closeSignatureModal}
                openSignatureModal={this.openSignatureModal}
                createSignatureModal={this.createSignatureModal}
                openSignaturePreviewModal={this.openSignaturePreviewModal}
                createSignaturePreviewModal={this.createSignaturePreviewModal}
                {...this.state}
                />
            </Tab>
          </Tabs>
        </Content>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth:state.auth,
    routes:state.routes,
    healthchecks:state.healthchecks,
    patients:state.patients,
    pdscs:state.pdscs
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    healthchecks: bindActionCreators(healthchecks, dispatch),
    pdscActions: bindActionCreators(pdscActions, dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PDSContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
