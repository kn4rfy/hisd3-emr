import React, { Component } from 'react';
import { Dimensions, View, Alert, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, List, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Tabs, Tab } from 'native-base';
import moment from 'moment';
import _ from 'lodash';
import ModalPicker from 'react-native-modal-picker';
import Spinner from 'react-native-loading-spinner-overlay';

import {Actions} from 'react-native-router-flux';
import {post,get} from '../../../utils/RestClient';
import {requireAuthentication} from '../../../utils/RoleComponent';

import * as Config from '../../../config/Config';
import DischargeForm from './DischargeForm';
import MultiSelectModal from '../../general/MultiSelectModal';
import SignatureModal from '../../general/SignatureModal';
import SignaturePreviewModal from '../../general/SignaturePreviewModal';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class PatientDataSheet extends Component {
  constructor(props) {
    super(props);

    this.state = {
      previousPatient: false,
      dayReturned: false,
      informant: 'Ace Jordan',
      selectedDoctor: '',
      selectedNurse: '',
      selectedPhysician: '',
      guarantor: '',
      witness: '',
      manner: '',
      isSelectibleOpen: false,
      isSelectibleOpenEr: false,
      doctorsList: [],
      activeErDoctors: []
    }

    this.triageLevels = ['Level 1','Level 2','Level 3', 'Level 4', 'Level 5' ];
    this.relations = ['HUSBAND', 'WIFE', 'CHILD', 'RELATIVE', 'OTHERS'];
    this.mannerSettlementOpt = ["PERSONAL CASH", "PERSONAL CHECK", "COMPANY ACCOUNT", "HMO","PN"];
  }

  componentDidMount(){
    this.showSpinner(true);
    this.syncPropsToState(this.props);
  }

  syncPropsToState=(props)=>{
    this.setState({
      triagelevel: props.pdscs.activeRecord.triagelevel,
      previousPatient: props.pdscs.activeRecord.previousPatient,
      returnWithin24Hr: props.pdscs.activeRecord.broughtBy,
      personGiveInfoName: props.pdscs.activeRecord.personGiveInfoName,
      personGiveInfoRelationToPatient: props.pdscs.activeRecord.personGiveInfoRelationToPatient,
      activeErDoctors: props.activeErDoctors,
      nurseData: props.pdscs.activeRecord.nurseData,
      guarantor_name: props.pdscs.activeRecord.guarantor_name,
      witness_name: props.pdscs.activeRecord.witness_name
    }, ()=>{
      this.showSpinner(false);
    });
  }

  showSpinner = (boolVal) => {
    this.setState({ spinVisible: boolVal });
  }

  openSelectibleModal = (name) => {
    this.setState({
      [name]: true
    });
  }

  triageChange = (level) => {
    return()=>{
      this.props.updateField('triagelevel', level);
    }
  }

  relationChange = (value) => {
    return()=>{
      let relPatient = value == 5 ? '' : this.relations[value];
      this.props.updateField('personGiveInfoRelationToPatient', relPatient);
    }
  }

  onSelectionsChangePhysicians = (doctors, items) => {
    this.props.updateField('attendingPhysicians', doctors);
  }

  onSelectionsChangeDoctors = (doctors, items) => {
    this.props.updateField('erDoctors', doctors);
  }

  onSearchAttPhy = (text) => {
    let filterResult = _.filter(this.doctorsMulti, (item) => {
      return item.label.indexOf(text) > -1;
    })

    this.setState({ items: filterResult })
  }

  closeSelectibleModal = (name) => {
    this.setState({ [name]: false }, ()=> {
      this.props.onDoctorSearch('');
    })
  }

  updateDischargeInput = (name, value) => {
    var payload = {};
    payload[name]=value;
    this.props.pdscActions.updateDischargeInput(payload);
  }

  openSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: true
    })
  }

  closeSignaturePreviewModal = (name) => {
    name = name+'Preview';
    this.setState({
      [name]: false
    })
  }

  renderPhyRow = (rowData) => {
    return (
      <View style={[HISstyle.putShadow, HISstyle.patientListItem]}>
        <Text style={{ fontSize: 15 }}>{rowData.label}</Text>
      </View>
    )
  }

  renderErDocRow = (rowData) => {
    let docIdx = _.findIndex(this.props.doctors, {value:rowData.value?rowData.value:`${Config.getHost()}${rowData.erDoctorLink}`});
    let docEmpId = docIdx > -1 ? this.props.doctors[docIdx].employeeId : null;

    let recidx = _.findIndex(this.props.pdscs.pdscInfo.erDoctors, {employeeId: docEmpId});
    let recordId = recidx > -1 ? this.props.pdscs.pdscInfo.erDoctors[recidx].id:null;

    let hasSignature = recidx > -1 ? this.props.pdscs.pdscInfo.erDoctors[recidx].hassignature:false;

    let signatureImg = null;

    if(this.props.pdscs.activeRecord[`erConsultant${docEmpId}Signature`] != null){
      signatureImg = `data:image/png;base64,`+this.props.pdscs.activeRecord[`erConsultant${docEmpId}Signature`];
    }else{
      if(recordId){
        if(hasSignature){
          signatureImg = Config.getHost()+`/restapi/pdscErDoctors/signature/${recordId}`;
        }
      }
    }

    return (
      <View style={{flexDirection: 'row'}}>
        {
          this.props[`erConsultant${docEmpId}Signature`] == true ?
          this.props.createSignatureModal(`erConsultant${docEmpId}Signature`,'Doctor')
          : null
        }
        <Item style={{flex: 0.7, borderBottomWidth: 0}}>
          <Input
            value={rowData.label}
            placeholder={'ER Doctor'}
            editable={false}
            />
        </Item>

        {signatureImg!=null ? (
          <Button warning block onPress={()=>this.openSignaturePreviewModal(`erConsultant${docEmpId}Signature`)}
            style={{flex: 0.3}}>
            <Text>Preview</Text>
          </Button>
        ):(
          <Button danger block onPress={()=>this.props.openSignatureModal(`erConsultant${docEmpId}Signature`)}
            style={{flex: 0.3}}>
            <Text>Signature</Text>
          </Button>
        )}

        <SignaturePreviewModal
          id={`erConsultant${docEmpId}Signature`}
          imgSrc={signatureImg}
          previewSignOpen={this.state[`erConsultant${docEmpId}SignaturePreview`]}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
      </View>
    )
  }

  render() {

    let index = 0;

    const physicians = [
      { key: index++, section: true, label: 'Your Attending Physician' },
      { key: index++, label: 'Julius Castillo, Ortho' },
      { key: index++, label: 'Ronnie Ramiro, MD' },
      { key: index++, label: 'Jane Ramiro, PhD' },
    ];

    const review = [
      { key: 'LetterA', value: 'LetterA' },
      { key: 'LetterA', value: 'LetterA' },
      { key: 'LetterA', value: 'LetterA' },
      { key: 'LetterA', value: 'LetterA' },
      { key: 'LetterA', value: 'LetterA' },
    ]

    let activeRecord = this.props.pdscs.activeRecord;

    let doctors = _.map(this.props.doctors, (doctor, idx) => {
      return {
        key: idx,
        label: doctor.label,
        value: doctor.value
      }
    });

    let selDocIdx = _.findIndex(this.props.doctors, {
      value: activeRecord.erConsultant
    });

    this.doctorsMulti = _.clone(doctors);

    let selDocName = !_.isEmpty(this.props.doctors[selDocIdx]) ? this.props.doctors[selDocIdx].label : '';

    let nurses = _.map(this.props.nurses, (nurse, idx) => {
      return {
        key: idx,
        label: nurse.label,
        value: nurse.value
      }
    });

    let selNurseIdx = _.findIndex(this.props.nurses, {
      value: activeRecord.nurse
    });

    let selNurseName = !_.isEmpty(this.props.nurses[selNurseIdx]) ? this.props.nurses[selNurseIdx].label : '';

    let attendingPhysicians = _.map(this.props.attendingPhysicians, (doctor, idx) => {
      return {
        key: idx,
        label: doctor.label,
        value: doctor.value,
        employeeId: doctor.employeeId
      }
    });

    let erDoctors = _.map(this.props.doctors, (doctor, idx) => {
      return {
        key: idx,
        label: doctor.label,
        value: doctor.value,
        employeeId: doctor.employeeId
      }
    });

    let activeMannerSets = activeRecord.mannerSettlement ? activeRecord.mannerSettlement.split(','):[];

    let personGiveInfoSignatureImg = null;
    if(activeRecord.personGiveInfoSignature != null){
      personGiveInfoSignatureImg = `data:image/png;base64,`+activeRecord.personGiveInfoSignature;
    }else{
      if(activeRecord.haspersonGiveInfoSignature){
        personGiveInfoSignatureImg = Config.getHost()+`/restapi/pdsc/personGiveInfoSignature/${activeRecord.id}`;
      }
    }

    let erConsultantSignatureImg = null;
    if(activeRecord.erConsultantSignature != null){
      erConsultantSignatureImg = `data:image/png;base64,`+activeRecord.erConsultantSignature;
    }else{
      if(activeRecord.haserConsultantSignature){
        erConsultantSignatureImg = Config.getHost()+`/restapi/pdsc/erConsultantSignature/${activeRecord.id}`;
      }
    }

    let nurseSignatureImg = null;
    if(activeRecord.nurseSignature != null){
      nurseSignatureImg = `data:image/png;base64,`+activeRecord.nurseSignature;
    }else{
      if(activeRecord.hasnurseSignature){
        nurseSignatureImg = Config.getHost()+`/restapi/pdsc/nurseSignature/${activeRecord.id}`;
      }
    }

    let guarantor_signatureImg = null;
    if(activeRecord.guarantor_signature != null){
      guarantor_signatureImg = `data:image/png;base64,`+activeRecord.guarantor_signature;
    }else{
      if(activeRecord.hasguarantor_signature){
        guarantor_signatureImg = Config.getHost()+`/restapi/pdsc/guarantor_signature/${activeRecord.id}`;
      }
    }

    let witnessSignatureImg = null;
    if(activeRecord.witnessSignature != null){
      witnessSignatureImg = `data:image/png;base64,`+activeRecord.witnessSignature;
    }else{
      if(activeRecord.haswitnessSignature){
        witnessSignatureImg = Config.getHost()+`/restapi/pdsc/witnessSignature/${activeRecord.id}`;
      }
    }

    let isDisabled = activeRecord.dischargeNoticeDate ? true : false;
    let saveDisabled = false;
    if(activeRecord.id){
      if(_.isEqual(this.props.pdscs.pdscInfo, activeRecord)){
        saveDisabled = true;
      }else{
        saveDisabled = false;
      }
    }

    return (
      <Container>
        <Content padder>
          {this.props.personGiveInfoSignature == true? this.props.createSignatureModal('personGiveInfoSignature', 'Informant'):null}
          {this.props.erConsultantSignature == true? this.props.createSignatureModal('erConsultantSignature','Resident Doctor on Duty'):null}
          {this.props.nurseSignature == true? this.props.createSignatureModal('nurseSignature','Nurse on Duty'):null}
          {this.props.guarantor_signature == true? this.props.createSignatureModal('guarantor_signature','Guarantor'):null}
          {this.props.witnessSignature == true? this.props.createSignatureModal('witnessSignature','Witness'):null}

          <Title>Emergency Information</Title>

          <Item stackedLabel>
            <Label>TRIAGE LEVEL</Label>
            <View style={{flexDirection: 'row'}}>
              <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 1 ? true:false}
                onPress={this.triageChange(1)}
                disabled={_.get(activeRecord, 'triagelevel') && _.get(this.state, 'triagelevel') ? true : false}>
                <Text>Level 1</Text>
                <Right>
                  <Radio selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 1 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 2 ? true:false}
                onPress={this.triageChange(2)}
                disabled={_.get(activeRecord, 'triagelevel') && _.get(this.state, 'triagelevel') ? true : false}>
                <Text>Level 2</Text>
                <Right>
                  <Radio selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 2 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 3 ? true:false}
                onPress={this.triageChange(3)}
                disabled={_.get(activeRecord, 'triagelevel') && _.get(this.state, 'triagelevel') ? true : false}>
                <Text>Level 3</Text>
                <Right>
                  <Radio selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 3 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 4 ? true:false}
                onPress={this.triageChange(4)}
                disabled={_.get(activeRecord, 'triagelevel') && _.get(this.state, 'triagelevel') ? true : false}>
                <Text>Level 4</Text>
                <Right>
                  <Radio selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 4 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 5 ? true:false}
                onPress={this.triageChange(5)}
                disabled={_.get(activeRecord, 'triagelevel') && _.get(this.state, 'triagelevel') ? true : false}>
                <Text>Level 5</Text>
                <Right>
                  <Radio selected={(activeRecord.triagelevel == null ? (this.props.activeTriage ==  null ? 0 : this.props.activeTriage) : activeRecord.triagelevel) == 5 ? true:false} />
                </Right>
              </ListItem>
            </View>
          </Item>

          <Item style={{flexDirection: 'row'}}>
            <ListItem style={{flex: 0.5, borderBottomWidth: 0}}
              selected={activeRecord.previousPatient || false}
              onPress={()=>this.props.updateField('previousPatient', !activeRecord.previousPatient)}
              disabled={_.get(activeRecord, 'previousPatient') && _.get(this.state, 'previousPatient')}>
              <Text>PREVIOUS PATIENT?</Text>
              <Right>
                <Switch
                  onValueChange={()=>this.props.updateField('previousPatient', !activeRecord.previousPatient)}
                  value={activeRecord.previousPatient || false}
                  disabled={_.get(activeRecord, 'previousPatient') && _.get(this.state, 'previousPatient')}
                  />
              </Right>
            </ListItem>

            <ListItem
              style={{flex: 0.5, borderBottomWidth: 0}}
              selected={activeRecord.returnWithin24Hr || false}
              onPress={()=>this.props.updateField('returnWithin24Hr', !activeRecord.returnWithin24Hr)}
              disabled={_.get(activeRecord, 'previousPatient') && _.get(this.state, 'previousPatient')}>
              <Text>RETURNED W/ 24 HOURS?</Text>
              <Right>
                <Switch
                  onValueChange={()=>this.props.updateField('returnWithin24Hr', !activeRecord.returnWithin24Hr)}
                  value={activeRecord.returnWithin24Hr || false}
                  disabled={_.get(activeRecord, 'previousPatient') && _.get(this.state, 'previousPatient')}
                  />
              </Right>
            </ListItem>
          </Item>

          <Item stackedLabel>
            <Label>INFORMANT</Label>
            <View style={{ flexDirection: 'row' }}>
              <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                <Input
                  onChangeText={(value) => this.props.updateField('personGiveInfoName', value)}
                  value={activeRecord.personGiveInfoName}
                  placeholder={'Informant'}
                  editable={_.get(activeRecord, 'personGiveInfoName') && _.get(this.state, 'personGiveInfoName') ? false : true}
                  />
              </Item>

              {personGiveInfoSignatureImg!=null?
                (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('personGiveInfoSignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ):(
                  <Button danger block onPress={()=>this.props.openSignatureModal('personGiveInfoSignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )
              }
            </View>
          </Item>

          <Item stackedLabel>
            <Label>RELATION TO PATIENT</Label>

            <Item style={{flexDirection: 'column', borderBottomWidth: 0}}>
              {this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 || activeRecord.personGiveInfoRelationToPatient==null ? (null) : (
                <Item>
                  <Input
                    onChangeText={(value) => this.props.updateField('personGiveInfoRelationToPatient', value)}
                    value={activeRecord.personGiveInfoRelationToPatient || ""}
                    placeholder={'Relation to Patient'}
                    editable={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? false : true}
                    />
                </Item>
              )}
              <View style={{flexDirection: 'row'}}>
                <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                  selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 1 ? true:false}
                  onPress={this.relationChange(1)}
                  disabled={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? true : false}>
                  <Text>HUSBAND</Text>
                  <Right>
                    <Radio selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 1 ? true:false} />
                  </Right>
                </ListItem>

                <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                  selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 2 ? true:false}
                  onPress={this.relationChange(2)}
                  disabled={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? true : false}>
                  <Text>WIFE</Text>
                  <Right>
                    <Radio selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 2 ? true:false} />
                  </Right>
                </ListItem>

                <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                  selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 3 ? true:false}
                  onPress={this.relationChange(3)}
                  disabled={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? true : false}>
                  <Text>CHILD</Text>
                  <Right>
                    <Radio selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 3 ? true:false} />
                  </Right>
                </ListItem>

                <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                  selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 4 ? true:false}
                  onPress={this.relationChange(4)}
                  disabled={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? true : false}>
                  <Text>RELATIVE</Text>
                  <Right>
                    <Radio selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 4 ? true:false} />
                  </Right>
                </ListItem>

                <ListItem style={{flex: 0.2, borderBottomWidth: 0}}
                  selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 5 ? true:false}
                  onPress={this.relationChange(5)}
                  disabled={_.get(activeRecord, 'personGiveInfoRelationToPatient') && _.get(this.state, 'personGiveInfoRelationToPatient') ? true : false}>
                  <Text>OTHERS</Text>
                  <Right>
                    <Radio selected={(activeRecord.personGiveInfoRelationToPatient != null ? (this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient) > 0 ?this.relations.indexOf(activeRecord.personGiveInfoRelationToPatient):5):0) == 5 ? true:false} />
                  </Right>
                </ListItem>
              </View>
            </Item>
          </Item>

          <Title>Emergency Personnel</Title>

          <Item stackedLabel
            style={{borderBottomWidth: this.props.activeErDoctors.length != 0 && this.state.activeErDoctors.length != 0 ? 1 : 0}}>
            <Label>ER Doctor/s</Label>
            <Item style={{borderBottomWidth: 0}}>
              <List
                dataArray={this.props.activeErDoctors}
                renderRow={(item) => this.renderErDocRow(item)
                }>
              </List>
            </Item>
          </Item>

          {this.props.activeErDoctors.length != 0 && this.state.activeErDoctors.length != 0 ? (
            null
          ) : (
            <Button danger block onPress={()=>this.openSelectibleModal('isSelectibleOpenEr')}>
              <Text>Select ER Doctor/s</Text>
            </Button>
          )}

          <Item stackedLabel>
            <Label>ER NURSE</Label>
            <View style={{ flexDirection: 'row' }}>
              {_.get(activeRecord, 'nurseData.fullname') && _.get(this.state, 'nurseData.fullname') ? (
                <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                  <Input
                    value={activeRecord.nurseData.fullname}
                    placeholder={'Select ER Nurse'}
                    editable={false}
                    />
                </Item>
              ) : (
                <ModalPicker
                  data={nurses}
                  initValue="Select ER Nurse"
                  onChange={(nurseSel)=>{ this.props.updateField('nurse', nurseSel.value)}}
                  style={{flex: 0.7}}>
                  <Input
                    value={selNurseName}
                    placeholder={'Select ER Nurse'}
                    editable={false}
                    />
                </ModalPicker>
              )}

              {selNurseName? nurseSignatureImg!=null? (
                <Button warning block onPress={()=>this.openSignaturePreviewModal('nurseSignature')}
                  style={{flex: 0.3}}>
                  <Text>Preview</Text>
                </Button>
              ) : (
                <Button danger block onPress={()=>this.props.openSignatureModal('nurseSignature')}
                  style={{flex: 0.3}}>
                  <Text>Signature</Text>
                </Button>
              ) : null}
            </View>
          </Item>

          <Title>Hospital Bills Responsibility</Title>

          <Item stackedLabel>
            <Label>GUARANTOR</Label>
            <View style={{ flexDirection: 'row' }}>
              <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                <Input
                  onChangeText={(value) => this.props.updateField('guarantor_name', value)}
                  value={activeRecord.guarantor_name}
                  placeholder={'Guarantor'}
                  editable={_.get(activeRecord, 'guarantor_name') && _.get(this.state, 'guarantor_name') ? false : true}
                  />
              </Item>

              {guarantor_signatureImg!=null?
                (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('guarantor_signature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ):(
                  <Button danger block onPress={()=>this.props.openSignatureModal('guarantor_signature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )
              }
            </View>
          </Item>

          <Item stackedLabel>
            <Label>WITNESS</Label>
            <View style={{ flexDirection: 'row' }}>
              <Item style={{flex: 0.7, borderBottomWidth: 0}}>
                <Input
                  onChangeText={(value) => this.props.updateField('witness_name', value)}
                  value={activeRecord.witness_name}
                  placeholder={'Witness'}
                  editable={_.get(activeRecord, 'witness_name') && _.get(this.state, 'witness_name') ? false : true}
                  />
              </Item>

              {witnessSignatureImg!=null?
                (
                  <Button warning block onPress={()=>this.openSignaturePreviewModal('witnessSignature')}
                    style={{flex: 0.3}}>
                    <Text>Preview</Text>
                  </Button>
                ) : (
                  <Button danger block onPress={()=>this.props.openSignatureModal('witnessSignature')}
                    style={{flex: 0.3}}>
                    <Text>Signature</Text>
                  </Button>
                )
              }
            </View>
          </Item>

          <Title>MANNER OF SETTLEMENT</Title>

          {_.map(this.mannerSettlementOpt, (opt, idx)=>{
            return (
              <ListItem key={opt} style={{borderBottomWidth: 0}}
                selected={activeMannerSets.indexOf(opt) > -1 ? true:false}
                onPress={() => {
                  let arrManSet = activeRecord.mannerSettlement ? activeRecord.mannerSettlement.split(','):[];
                  if(activeMannerSets.indexOf(opt) > -1 == false){
                    arrManSet.push(opt);
                  }else{
                    _.remove(arrManSet, (data)=>{
                      return data == "" || data == opt
                    });
                  }
                  let swValue = arrManSet.join(',');
                  this.props.updateField('mannerSettlement', swValue)}
                }>
                <Text>{opt}</Text>
                <Right>
                  <Switch
                    onValueChange={() => {
                      let arrManSet = activeRecord.mannerSettlement ? activeRecord.mannerSettlement.split(','):[];
                      if(activeMannerSets.indexOf(opt) > -1 == false){
                        arrManSet.push(opt);
                      }else{
                        _.remove(arrManSet, (data)=>{
                          return data == "" || data == opt
                        });
                      }
                      let swValue = arrManSet.join(',');
                      this.props.updateField('mannerSettlement', swValue)}
                    }
                    value={activeMannerSets.indexOf(opt) > -1 ? true:false}
                    />
                </Right>
              </ListItem>
            )}
          )}

          {
            requireAuthentication((
              <View style={{ flexDirection: 'row' }}>
                <Button success block onPress={this.props.onFormSubmit} isDisabled={isDisabled||saveDisabled}
                  style={{flex: 0.4}}>
                  <Text>Save</Text>
                </Button>

                <View style={{flex: 0.2}}></View>

                {activeRecord.id && isDisabled == false ?
                  <Button primary block onPress={this.props.openDischargeModal}
                    style={{flex: 0.4}}>
                    <Text>Discharge</Text>
                  </Button>
                  :
                  <View style={{flex: 0.4}}></View>
                }
              </View>
            ), this.props.auth.account, "ROLE_ER_NURSE")
          }

        </Content>
        <SignaturePreviewModal
          id='personGiveInfoSignature'
          imgSrc={personGiveInfoSignatureImg}
          previewSignOpen={this.state.personGiveInfoSignaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <SignaturePreviewModal
          id='erConsultantSignature'
          imgSrc={erConsultantSignatureImg}
          previewSignOpen={this.state.erConsultantSignaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <SignaturePreviewModal
          id='nurseSignature'
          imgSrc={nurseSignatureImg}
          previewSignOpen={this.state.nurseSignaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <SignaturePreviewModal
          id='guarantor_signature'
          imgSrc={guarantor_signatureImg}
          previewSignOpen={this.state.guarantor_signaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <SignaturePreviewModal
          id='witnessSignature'
          imgSrc={witnessSignatureImg}
          previewSignOpen={this.state.witnessSignaturePreview}
          closeSignaturePreviewModal={this.closeSignaturePreviewModal}
          openSignaturePreviewModal={this.openSignaturePreviewModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <DischargeForm
          {...this.props}
          doctors={this.props.doctors}
          nurses={this.props.nurses}
          updateField={this.updateDischargeInput}
          closeSignatureModal={this.props.closeSignatureModal}
          openSignatureModal={this.props.openSignatureModal}
          />
        <MultiSelectModal
          id='attendingPhysicians'
          items={attendingPhysicians}
          selectedItems={this.props.activeAttendingPhysicians}
          isSelectibleOpen={this.state.isSelectibleOpen}
          onSelectionsChange={this.onSelectionsChangePhysicians}
          onSearch={this.props.onDoctorSearch}
          updateField={this.props.updateField}
          closeModal={this.closeSelectibleModal}
          />
        <MultiSelectModal
          id='erDoctors'
          items={erDoctors}
          selectedItems={this.props.activeErDoctors}
          isSelectibleOpen={this.state.isSelectibleOpenEr}
          onSelectionsChange={this.onSelectionsChangeDoctors}
          onSearch={this.props.onDoctorSearch}
          updateField={this.props.updateField}
          closeModal={this.closeSelectibleModal}
          />
        <Spinner visible={this.state.spinVisible} />
      </Container>
    )
  }
}
