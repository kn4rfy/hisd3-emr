import React, { Component } from 'react';
import { Dimensions, View } from 'react-native';
import { Container, Header, Content, Title, InputGroup, Input, Icon, List, ListItem, Thumbnail, H3, Text, Button, Item, Left, Body, Right, Label } from 'native-base';
import moment from 'moment'

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux'
import {bindActionCreators} from 'redux';

import * as HISstyle from '../styles/HISstyle';
import * as patientActions from '../actions/patientactions';
import {requireAuthentication} from '../utils/RoleComponent';
import * as Config from '../config/Config';

const { width, height, scale } = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class PatientList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: '',
      filterDob: '',
      activePage: 1,
      pageSize: 99999,
      showFab: false
    }

    this.fetchPatientList();
  }

  selectPatient=(patientId)=>{
    return()=>{
      Actions.patientmenu({patientId});
    }
  }

  newCase = () => {
    Actions.newcase();
  }

  fetchPatientList = () => {
    let filter = this.getFilter();
    let filterDob = this.getFilterDob();
    this.props.patientActions.getPatientList(this.state.activePage, filter.toUpperCase(), filterDob, this.state.pageSize);
  }

  getFilter = () => {
    return this.props.patients.activeSearch != '' ? this.props.patients.activeSearch : this.state.filter;
    // return this.state.filter;
  }

  getFilterDob = () => {
    return this.props.patients.activeSearchDob != '' ? this.props.patients.activeSearchDob : this.state.filterDob;
    // return this.state.filter;
  }

  renderRow=(rowData)=>{

    const defImage = (rowData.gender.toLowerCase() == 'male') ? require('../assets/male.png') : require('../assets/female.png');

    return(
      <View>
        <ListItem thumbnail onPress={this.selectPatient(rowData.id)} style={{paddingVertical: 10}}>
          <Left>
            <Thumbnail
              size={80}
              source={rowData.hasPicture ? { uri: Config.getHost()+ "/restapi/patient/picture/" + rowData.id} : defImage}
              />
          </Left>
          <Body style={{borderBottomWidth: 0}}>
            <Label>{rowData.fullname}</Label>
            <Text note>{rowData.home_address || 'No Address Specified'}</Text>
            <Text note>{moment(rowData.dob).format("MMMM DD, YYYY")}</Text>
          </Body>
          <Right style={{borderBottomWidth: 0}}>
            <Icon name="arrow-forward" />
          </Right>
        </ListItem>
        <View style={{height: 1, backgroundColor: '#e2e2e2'}}></View>
      </View>
    );
  }

  onSearchChange = (term) => {
    this.setState({
      filter: term
    })

    this.props.patientActions.updateSearch(term);

    if(this.timeoutHandle)
    clearTimeout(this.timeoutHandle);

    this.timeoutHandle = setTimeout(()=>{
      this.fetchPatientList();
      this.timeoutHandle = undefined;
    }, 500);
  }

  render() {
    let filter = this.getFilter();

    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="search" />
            <Input placeholder="Search patient..." onChangeText={this.onSearchChange}/>
            <Icon active name="people" />
          </Item>
          {requireAuthentication((
            <Button
              danger
              small
              rounded
              onPress={this.newCase}
              >
              <Text>NEW CASE</Text>
            </Button>
          ), this.props.auth.account, 'ROLE_ER_NURSE')}
        </Header>
        <Content padder>
          <List
            dataArray={this.props.patients.patientList}
            renderRow={(item) => this.renderRow(item)
            }>
          </List>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    patients: state.patients,
    pdscs: state.pdscs
  }
}

function mapDispatchToProps(dispatch) {
  return {
    patientActions: bindActionCreators(patientActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PatientList);
