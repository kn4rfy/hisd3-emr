import React, { Component } from 'react';
import { Dimensions, Image, Alert } from 'react-native';
import { Container, Header, Content, Body, Title, Item, Thumbnail, Form, Input, Icon, Button, Text } from 'native-base';
import _ from 'lodash';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Actions} from 'react-native-router-flux';

import * as authActions from '../actions/authActions';
import * as erActions from '../actions/erActions';
import * as healthchecks from '../actions/healthchecks';

import * as style from '../styles/styles';
import * as HISstyle from '../styles/HISstyle';
import {post,get} from '../utils/RestClient';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state={
      j_username: 'kn4rfy',
      j_password: 'sansastarkgirl'
    };
  }

  onChange=(fieldName)=>{
    return (value)=>{
      var payload = {};
      payload[fieldName] = value;

      this.setState(payload);
    }
  }

  login=()=>{
    if(!this.state.j_username || !this.state.j_password){
      Alert.alert(
        'Error Credentials', 'Please enter username and password',
        [{
          text: 'OK',
          onPress: () => {}
        }]
      );
      return;
    }

    this.props.healthchecks.ping(()=>{
      this.props.authActions.login(this.state, (result,error)=>{
        if(result){
          Actions.patients();
        }
        else {
          Alert.alert(
            'Error Credentials', error || 'Wrong username and password',
            [{
              text: 'OK',
              onPress: () => {}
            }]
          );
        }
      });
    });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>Patient List</Title>
          </Body>
        </Header>
        <Content padder>
          <Form>
            <Image
              style={{ resizeMode: 'contain', width: 100 * vw, height: 70 *vh }}
              source={require('../assets/logo.png')}
              />
            <Item>
              <Icon name='person' style={{color: '#384850'}}/>
              <Input
                autoCapitalize={'none'}
                autoCorrect={false}
                placeholder='Username'
                onChangeText={this.onChange('j_username')}
                />
            </Item>

            <Item>
              <Icon name='lock' style={{color: '#384850'}}/>
              <Input
                placeholder='Password'
                secureTextEntry={true}
                onChangeText={this.onChange('j_password')}
                />
            </Item>

            <Button
              success
              full
              disabled={_.get(this.state, 'j_username') && _.get(this.state, 'j_password') ? false : true}
              onPress={this.login}
              >
                <Text>Login</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth:state.auth,
    routes:state.routes,
    er:state.er,
    healthchecks:state.healthchecks
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    erActions: bindActionCreators(erActions, dispatch),
    healthchecks: bindActionCreators(healthchecks, dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Auth);
