import React, { Component } from 'react';
import { Dimensions, View, Image } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Left, Right, Radio, Icon, Text, Thumbnail } from 'native-base';
import moment from 'moment';
import _ from 'lodash';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {bindActionCreators} from 'redux';

import * as patientActions from '../actions/patientactions';
import * as pdscActions from '../actions/pdscactions';
import * as erActions from '../actions/erActions';

import * as Config from '../config/Config';

export default class PatientInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      patient: []
    }
  }

  render() {
    let triageLevel = null;
    if(!_.isEmpty(this.props.patients.patientInfo.activePdsc)){
      triageLevel = this.props.patients.patientInfo.activePdsc.triagelevel!=null?this.props.patients.patientInfo.activePdsc.triagelevel: null;
    }else{
      if(this.props.activeTriage != null){
        triageLevel = this.props.activeTriage + 1;
      }
    }

    const defImage = (this.props.patients.patientInfo.gender.toLowerCase() == 'male') ? require('../assets/boy.png') : require('../assets/girl.png');

    return (
      <View>
        <ListItem style={{flexDirection: 'row', borderBottomWidth: 0}}>
          <Thumbnail size={100}
            source={this.props.patients.patientInfo.hasPicture ? { uri: Config.getHost()+ "/restapi/patient/picture/" + this.props.patients.patientInfo.id}
            : defImage }
            />

          <ListItem style={{borderBottomWidth: 0}}>
            <Body style={{borderBottomWidth: 0}}>
              <Label>{this.props.patients.patientInfo.fullname}</Label>
              <Text note>{this.props.patients.patientInfo.home_address || 'No Address Specified'}</Text>
              <Text note>{moment(this.props.patients.patientInfo.dob).format("MMMM DD, YYYY")}</Text>
            </Body>
          </ListItem>
        </ListItem>

          <View style={{flexDirection: 'row', padding: 10}}>
            <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
              <View>
                <Label>PATIENT NO</Label>
                <Text>{this.props.patients.patientInfo.id ? this.props.patients.patientInfo.patientNo:'NO DATA'}</Text>
              </View>
            </Item>

            <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

            <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
              <View>
                <Label>CASE NO</Label>
                <Text>{this.props.patients.patientInfo.activePdsc != null ? this.props.patients.patientInfo.activePdsc.casenum:'NO DATA'}</Text>
              </View>
            </Item>

            <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

            <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
              <View>
                <Label>TRIAGE</Label>
                <Text>{triageLevel != null ? `LEVEL ${triageLevel}` : `NO DATA` } </Text>
              </View>
            </Item>

            <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

            <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
              <View>
                <Label>AGE</Label>
                <Text>{Math.floor(moment(new Date()).diff(moment(this.props.patients.patientInfo.dob),'years',true))}</Text>
              </View>
            </Item>
          </View>
        </View>
      )
    }
  }
