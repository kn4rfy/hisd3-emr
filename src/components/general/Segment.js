import React, { Component } from 'react'
import {
  View,
  Text,
  Switch
} from 'react-native'

import _ from 'lodash';
import {CustomSegmentedControl} from 'react-native-custom-segmented-control'

export default class Segment extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ height: 60 }}>
        <CustomSegmentedControl
          style={{
            flex:1,
            backgroundColor: 'white',
            marginVertical: 8
          }}
          textValues={this.props.values}
          selected={this.props.selected || 0}
          segmentedStyle={{
            selectedLineHeight: 2,
            fontSize:17,
            fontWeight: 'bold', // bold, italic, regular (default)
            segmentBackgroundColor: 'transparent',
            segmentTextColor: '#7a92a5',
            segmentHighlightTextColor: '#7a92a599',
            selectedLineColor: '#00adf5',
            selectedLineAlign: 'bottom', // top/bottom/text
            selectedLineMode: 'text', // full/text
            selectedTextColor: 'black',
            selectedLinePaddingWidth: 30,
            segmentFontFamily: 'system-font-bold'
          }}
          animation={{
            duration: 0.7,
            damping: 0.5,
            animationType: 'middle-line',
            initialDampingVelocity: 0.4
          }}
          onSelectedWillChange={(event)=> {
          }}
          onSelectedDidChange={this.props.onSelectedDidChange}
          />
      </View>
    )
  }
}
