import React, { Component } from 'react';
import { Dimensions, View, Modal, Image } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card, CardItem } from 'native-base';
import SelectMultiple from 'react-native-select-multiple';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class MultiSelectModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    }
  }

  onSearch = (text) => {
    this.setState({ text }, () => {
      this.props.onSearch(this.state.text);
    });
  }

  onSaveSelection = () => {
    this.props.updateField(this.props.id, this.props.selectedItems);
    this.closeModal();
  }

  closeModal = () => {
    this.setState({
      text: ''
    }, () => {
      let modalStatusName = this.props.id == 'attendingPhysicians' ? 'isSelectibleOpen':'isSelectibleOpenEr';
      this.props.closeModal(modalStatusName);
    })
  }

  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.isSelectibleOpen||false}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header searchBar rounded>
            <Left style={{flex: 0}}>
              <Button transparent onPress={this.closeModal}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Item style={{flex: 0.8}}>
              <Icon name="search" />
              <Input placeholder="Search Doctor..." onChangeText={(text) => this.onSearch(text)}/>
              <Icon active name="people" />
            </Item>
          </Header>
          <Content padder>
            <Card>
              <CardItem cardBody>
                <SelectMultiple
                  items={this.props.items}
                  selectedItems={this.props.selectedItems}
                  onSelectionsChange={this.props.onSelectionsChange}
                  />
              </CardItem>
              <CardItem>
                <Left>
                  <Button danger block onPress={this.closeModal}>
                    <Text>Close</Text>
                  </Button>
                </Left>
                <Right>
                  <Button success block onPress={this.onSaveSelection}>
                    <Text>Save</Text>
                  </Button>
                </Right>
              </CardItem>
            </Card>
          </Content>
        </Container>
      </Modal>
    )
  }
}
