import React, { Component } from 'react';
import { Dimensions, View, Modal } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card, CardItem } from 'native-base';
import SignatureCapture from 'react-native-signature-capture';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class SignatureModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    }
  }

  saveSign = () => {
    // this.refs[this.props.id+"sign"].saveImage();
    this.signatureComponentRef.saveImage();
    // this.props.closeSignatureModal(this.props.id);
  }

  resetSign = () => {
    // this.refs[this.props.id+"sign"].resetImage();
    this.signatureComponentRef.resetImage();
  }

  _onSaveEvent = (result) => {
    //result.encoded - for the base64 encoded png
    //result.pathName - for the file path name
    this.props.updateField(this.props.id, result.encoded);
    this.props.closeSignatureModal(this.props.id);

    // this.refs[this.props.id+"sign"].close();
  }
  _onDragEvent = (result) => {
    // This callback will be called when the user enters signature
  }

  _signaturePadError = (error) => {
    console.error(error);
  };

  _signaturePadChange = ({base64DataUrl}) => {
    base64DataUrl = this.removeBase64Prefix(base64DataUrl);
    this.props.updateField(this.props.id, base64DataUrl);
  };

  removeBase64Prefix = (strVal) => {
    return _.replace(strVal, "data:image/png;base64,", "");
  }

  show = (display) => {
    this.setState({visible: display});
  }

  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.isSignatureOpen||false}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={()=>this.props.closeSignatureModal(this.props.id)}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title>Signature {this.props.signatureOf ? this.props.signatureOf : ''}</Title>
            </Body>
            <Right />
          </Header>
          <Card>
            {this.props.isSignatureOpen ? (
              <View>
                <SignatureCapture
                  ref={(ref)=>this.signatureComponentRef = ref}
                  onSaveEvent={this.props.isSignatureOpen ? this._onSaveEvent:null}
                  onDragEvent={this._onDragEvent}
                  saveImageFileInExtStorage={false}
                  showNativeButtons={false}
                  viewMode={"portrait"}
                  style={{height: 500}}
                  />
              </View>
            ) : null }
            <CardItem>
              <Left>
                <Button danger block onPress={()=>this.props.closeSignatureModal(this.props.id)}>
                  <Text>Close</Text>
                </Button>
              </Left>
              <Body>
                <Button primary block onPress={() => this.resetSign()}>
                  <Text>Reset</Text>
                </Button>
              </Body>
              <Right>
                <Button success block onPress={() => this.saveSign()}>
                  <Text>Save</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Container>
      </Modal>
    )
  }
}
