import React, { Component } from 'react'
import {
  View,
  Text,
  Switch,
  Image
} from 'react-native'

import _ from 'lodash';
import * as style from '../../styles/styles';
import * as HISstyle from '../../styles/HISstyle'

export default class TitleBar extends Component {
    constructor(props) {
      super(props);
    }

    render() {
      return (
        <View>
          <Text style={[HISstyle.putShadow, { position: 'absolute', fontSize: 30, marginTop: 20, marginLeft: 10, zIndex: 100, backgroundColor: 'transparent', color: 'white',fontFamily:HISstyle.fnFamilyRegular }]}>{this.props.title}</Text>
          <Text style={[HISstyle.putShadow, { position: 'absolute', fontSize: 18, marginTop: 20, right:10, bottom:15, zIndex: 100, backgroundColor: 'transparent', color: 'white',fontFamily:HISstyle.fnFamilyRegular }]}> {_.  isEmpty(this.props.moduleName) ? null : this.props.moduleName} </Text>

          {/* <Image style={{ height: 70, zIndex: 90 }} source={require('../../assets/titlebar-bg.png')} /> */}
          <View style={{height:70,zIndex:90,backgroundColor:HISstyle.mainColor}}></View>
        </View>
      )
    }
}
