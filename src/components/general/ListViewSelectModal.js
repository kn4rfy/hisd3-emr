import React, { Component } from 'react';
import {
  View,
  Text,
  Switch,
  TextInput,
  Image,
  ScrollView,
  Dimensions,
  ListView,
  TouchableOpacity
} from 'react-native'

import Icon from 'react-native-vector-icons/EvilIcons';
import { Fumi, Hoshi, Hideo } from 'react-native-textinput-effects'
import Button from 'apsl-react-native-button'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';
import _ from 'lodash';
import moment from 'moment';
import uuid from 'uuid';

import ModalBox from 'react-native-modalbox';


import * as HISstyle from '../../styles/HISstyle';

const { width, height, scale } = Dimensions.get('window')

import ModalPicker from 'react-native-modal-picker'

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

export default class ListViewSelectModal extends Component {
  constructor(props) {
    super(props);

    // this.state={
    //   data:[{value:'Sample1'},
    //         {value:'Sample2'}
    //         ]
    // }

  }

  onSelectItem = (value) =>{
    return ()=>{

      this.props.onSelectItem(value,this.props.typeData)
      this.props.onClosed();
    }
  }

  renderNoteRow = (rowData, sectionID, rowID) => {
    return (
      <TouchableOpacity onPress={this.onSelectItem(rowData.value)} style={{marginLeft:10,marginRight:10,marginBottom:10,marginTop:10}}>
          <Text style={{fontFamily:HISstyle.fnFamilyThin,fontSize:15}}>{rowData.value}</Text>

      </TouchableOpacity>
    )
  }

  render(){
    return(
      <ModalBox  swipeToClose={false} backdropPressToClose={false} isOpen={this.props.isOpen} style={HISstyle.HISstyleModal} position={'center'}>
        {/* <View style={{flexDirection:'row'}}>
          <Text style={[HISstyle.formLabel,{fontWeight:'bold',marginTop:10}]}>Lab Order</Text>

        </View> */}

        <View style={{ flexDirection: 'row',backgroundColor:HISstyle.backColor,padding:10,marginBottom:5,marginTop:-210 }}>
          <View style={{ flex: 0.3 }}><Text onPress={()=>{this.props.onClosed()}} style={{fontFamily:HISstyle.fnFamilyThin,fontSize:17}}>Cancel</Text></View>
          <View style={{ flex: 0.3}}><Text style={{alignSelf:'center',fontFamily:HISstyle.fnFamilyRegular,fontWeight:'bold',fontSize:18}}>{this.props.title}</Text></View>
          <View style={{ flex: 0.3 }}><Text style={{alignSelf:'flex-end',opacity:0}} >Save</Text></View>
        </View>

        <View style={{flexDirection:'row'}}>
            <ListView
              enableEmptySections={true}
              dataSource={ds.cloneWithRows(this.props.dataLabOrder)}
              renderRow={this.renderNoteRow}
              renderSeparator={(sectionID, rowID) =>
                <View key={`${sectionID}-${rowID}`} style={{height: 1,backgroundColor: HISstyle.backColor,marginLeft:10,marginRight:10}} />
              }

            />
        </View>

      </ModalBox>
    )
  }
}
