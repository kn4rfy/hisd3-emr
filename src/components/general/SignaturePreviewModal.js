import React, { Component } from 'react';
import { Dimensions, View, Modal, Image } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon, Card, CardItem } from 'native-base';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

export default class SignaturePreviewModal extends React.Component {
  constructor(props){
    super(props);
  }

  editSignature = () => {
    this.props.closeSignaturePreviewModal(this.props.id)
    this.props.openSignatureModal(this.props.id);
  }

  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.previewSignOpen||false}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={()=>this.props.closeSignaturePreviewModal(this.props.id)}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title>Signature</Title>
            </Body>
            <Right />
          </Header>
          <Card>
            {this.props.previewSignOpen? (
              <Image
                resizeMode="contain"
                style={{height:500}}
                source={{uri:this.props.imgSrc}}
                />
            ) : null}
            <CardItem>
              <Body>
                <Button danger block onPress={()=>this.props.closeSignaturePreviewModal(this.props.id)}>
                  <Text>Close</Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
        </Container>
      </Modal>
    );
  }
}
