import React, { Component } from 'react';
import { Dimensions, View, Modal } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon } from 'native-base';
import {Actions} from 'react-native-router-flux';

export default class TriageModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.showTriageModal}
        onRequestClose={()=>{}}
        >
        <Container>
          <Header>
            <Left>
              <Button transparent onPress={()=>this.props.closeTriageModal('backToPatientList')}>
                <Icon name='arrow-back' />
              </Button>
            </Left>
            <Body>
              <Title>Select triage level</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
            <View style={{flexDirection: 'row'}}>
              <ListItem style={{flex: 0.2}} selected={this.props.activeTriage == 1 ? true:false} onPress={this.props.triageChange(1)}>
                <Text>Level 1</Text>
                <Right>
                  <Radio selected={this.props.activeTriage == 1 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2}} selected={this.props.activeTriage == 2 ? true:false} onPress={this.props.triageChange(2)}>
                <Text>Level 2</Text>
                <Right>
                  <Radio selected={this.props.activeTriage == 2 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2}} selected={this.props.activeTriage == 3 ? true:false} onPress={this.props.triageChange(3)}>
                <Text>Level 3</Text>
                <Right>
                  <Radio selected={this.props.activeTriage == 3 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2}} selected={this.props.activeTriage == 4 ? true:false} onPress={this.props.triageChange(4)}>
                <Text>Level 4</Text>
                <Right>
                  <Radio selected={this.props.activeTriage == 4 ? true:false} />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.2}} selected={this.props.activeTriage == 5 ? true:false} onPress={this.props.triageChange(5)}>
                <Text>Level 5</Text>
                <Right>
                  <Radio selected={this.props.activeTriage == 5 ? true:false} />
                </Right>
              </ListItem>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.3}}>
                <Button
                  danger
                  block
                  onPress={()=>this.props.closeTriageModal('backToPatientList')}
                  >
                  <Text>Close</Text>
                </Button>
              </View>
              <View style={{flex: 0.7}}>
                <Button
                  success
                  block
                  onPress={()=>this.props.saveSelectedTriage()}
                  disabled={this.props.activeTriage == -1?true:false}
                  >
                  <Text>Save and Continue</Text>
                </Button>
              </View>
            </View>
          </Content>
        </Container>
      </Modal>
    );
  }
}
