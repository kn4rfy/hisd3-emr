import React, { Component } from 'react';
import { Dimensions, View, Image, Alert } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Left, Right, Radio, Icon, Card, CardItem } from 'native-base';
import _ from 'lodash';
import Spinner from 'react-native-loading-spinner-overlay';

import {connect} from 'react-redux';
import {Actions, Scene} from 'react-native-router-flux'
import {bindActionCreators} from 'redux';

import * as patientActions from '../actions/patientactions';
import * as pdscActions from '../actions/pdscactions';
import * as erActions from '../actions/erActions';

import * as Config from '../config/Config';
import {requireAuthentication} from '../utils/RoleComponent';
import PatientInfo from './PatientInfo';
import PMRInfo from './PMRInfo';

class PatientMenu extends Component {
  constructor(props) {
    super(props);

    this.state = { spinVisible: false }
  }

  componentDidMount() {
    this.showSpinner(true);
    this.props.pdscActions.updatePDSCInput('clear');
    this.props.pdscActions.getPatientAndPDSCInfo(this.props.patientId);
    this.props.erActions.getPMRByPatientId(this.props.patientId);
    this.props.erActions.getVitalSignsByPatientId(this.props.patientId);
  }

  componentWillReceiveProps (nextProps) {
    if(nextProps.patients.patientInfo.id == this.props.patientId);
    this.showSpinner(false);
  }

  showSpinner = (boolVal) => {
    this.setState({ spinVisible: boolVal });
  }

  pageSelect = (page) => {

    return()=>{

      if (!_.isEmpty(this.props.patients.patientInfo.activePdsc)) {
        switch(page)
        {
          case 'pdsc':
          Actions.pdsc({ patientInfo: this.props.patients  });
          break;
          case 'pmr':
          Actions.pmr({ patientId: this.props.patientId });
          break;
          case 'mpndo':
          Actions.mpndo({ patientId: this.props.patientId, patientInfo: this.props.patients, pmr: this.props.pdscs.pdscInfo.activePmr.id });
          break;
          case 'iap':
          if (this.props.patients.patientInfo.activePdsc.triagelevel <= 3 ) {
            Actions.iap({patientId:this.props.patientId})
          }else{
            this.iapWarning();
          }
        }
      }
      else{
        if (page == 'pdsc') {
          Actions.pdsc();
        }else{
          this.successSubmit();
        }
      }

    }
  }

  successSubmit = () =>{
    Alert.alert(
      'Alert!',
      `Create the Patient Data Sheet Consent First!`,
      [
        {text: 'OK', onPress: ()=>{

        }}
      ]
    )
  }

  iapWarning = () =>{
    Alert.alert(
      'Alert!',
      `This feature is available for triage 1, 2, and 3`,
      [
        {text: 'OK', onPress: ()=>{

        }}
      ]
    )
  }

  render() {

    let btnOpacity;

    if (!_.isEmpty(this.props.patients.patientInfo.activePdsc)) {
      btnOpacity = 1;
    }else{
      btnOpacity = 0.2;
    }

    let btnOpacityIap = this.props.pdscs.activeRecord.triagelevel > 3 ? 0.2:btnOpacity;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=>Actions.pop()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>New case</Title>
          </Body>
          <Right />
        </Header>
        <PatientInfo {...this.props} />
        <Content padder>
          <PMRInfo patientId={this.props.patientId} />

          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.5 }}>
              {
                requireAuthentication((
                  <Card>
                    <ListItem style={{borderBottomWidth: 0}} onPress={this.pageSelect('pdsc')}>
                      <Body>
                        <Image
                          style={{ width: 60, height: 60, borderRadius: 10 }}
                          source={require('../assets/pdsc.png')}
                          />
                        <Label>
                          Patient Data Sheet
                        </Label>
                      </Body>
                    </ListItem>
                  </Card>
                ), this.props.auth.account, ["ROLE_ER_NURSE", "ROLE_ER_DOCTOR"])
              }
            </View>
            <View style={{ flex: 0.5,opacity:btnOpacity }}>
              {
                requireAuthentication((
                  <Card>
                    <ListItem style={{borderBottomWidth: 0}} onPress={this.pageSelect('pmr')}>
                      <Body>
                        <Image
                          style={{ width: 60, height: 60, borderRadius: 10 }}
                          source={require('../assets/pmr.png')}
                          />
                        <Label>
                          Patient Medical Record
                        </Label>
                      </Body>
                    </ListItem>
                  </Card>
                ), this.props.auth.account, ["ROLE_ER_NURSE", "ROLE_ER_DOCTOR"])
              }
            </View>
          </View>

          <View style={{ flexDirection: 'row'}}>
            <View style={{ flex: 0.5,opacity:btnOpacityIap }}>

              {
                requireAuthentication((
                  <Card>
                    <ListItem style={{borderBottomWidth: 0}} onPress={this.pageSelect('iap')}>
                      <Body>
                        <Image
                          style={{ width: 60, height: 60, borderRadius: 10 }}
                          source={require('../assets/iap.png')}
                          />
                        <Label>
                          Personal Effects
                        </Label>
                      </Body>
                    </ListItem>
                  </Card>
                ), this.props.auth.account, "ROLE_ER_STAFF")
              }

            </View>
            <View style={{flex: 0.5, opacity: btnOpacity}}>
              {
                requireAuthentication((
                  <Card>
                    <ListItem style={{borderBottomWidth: 0}} onPress={this.pageSelect('mpndo')}>
                      <Body>
                        <Image
                          style={{ width: 60, height: 60, borderRadius: 10 }}
                          source={require('../assets/mpndo.png')}
                          />
                        <Label>
                          Doctors Order and Progress Notes
                        </Label>
                      </Body>
                    </ListItem>
                  </Card>
                ), this.props.auth.account, ["ROLE_ER_NURSE", "ROLE_ER_DOCTOR"])
              }
            </View>
          </View>
        </Content>

        <Spinner visible={this.state.spinVisible} />
      </Container>
    )
  }
}

function mapStateToProps(state){
  return {
    auth: state.auth,
    patients: state.patients,
    pdscs: state.pdscs,
    er: state.er,
    pmr: state.er.pmr
  }
}

function mapDispatchToProps(dispatch) {
  return {
    patientActions: bindActionCreators(patientActions, dispatch),
    pdscActions: bindActionCreators(pdscActions, dispatch),
    erActions: bindActionCreators(erActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PatientMenu);
