import React, { Component } from 'react';
import { Dimensions, View, Image, Alert } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Left, Right, Radio, Icon, Text } from 'native-base';
import moment from 'moment';
import _ from 'lodash';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import * as erActions from '../actions/erActions';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class PMRInfo extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let latestVitalSignsIndex = this.props.er.vitalSigns.records.length -1;
    let latestDT;
    if (!_.isEmpty(this.props.er.vitalSigns.records)) {
      latestDT = moment(_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].dateTime', '')).format('MMM DD, YYYY HHmm') + 'H';
    }else{
      latestDT = "No Data Yet";
    }

    return (
      <View>

        <ListItem style={{borderBottomWidth: 0}}>
          <Title>LATEST VITAL SIGNS</Title>
          <Right>
            <Label>{latestDT}</Label>
          </Right>
        </ListItem>

        <View style={{height: 1, backgroundColor: '#e2e2e2'}}></View>

        <View style={{flexDirection: 'row', paddingVertical:20}}>
          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>BLOOD PRESSURE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.bloodPressureSystolic', '') && _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.bloodPressureDiastolic', '')? (
                  _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.bloodPressureSystolic', '')
                  + ' / ' +
                  _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.bloodPressureDiastolic', '')
                ) + ' mmHg' : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>HEART RATE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.heartRate', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.heartRate', '') + ' bpm' : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>PULSE RATE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.pulseRate', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.pulseRate', '') + ' bpm' : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>RESPIRATORY RATE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.respiratoryRate', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.respiratoryRate', '') + ' bpm' : 'NO DATA'}
              </Text>
            </View>
          </Item>
        </View>

        <View style={{height: 1, backgroundColor: '#e2e2e2'}}></View>

        <View style={{flexDirection: 'row', paddingVertical:20}}>
          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>TEMPERATURE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.temperature', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.temperature', '') + ' °C' : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>O2 SAT</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.oxygenSaturation', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.oxygenSaturation', '') + ' %' : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>PAIN SCORE</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.painScore', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.painScore', '') : 'NO DATA'}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>GLASGOW COMA</Label>
              <Text>
                {_.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.glasgowComaScale', '') ? _.get(this.props.er.vitalSigns, 'records['+latestVitalSignsIndex+'].records.glasgowComaScale', '') : 'NO DATA'}
              </Text>
            </View>
          </Item>
        </View>

        <View style={{height: 1, backgroundColor: '#e2e2e2'}}></View>

        <View style={{flexDirection: 'row', paddingVertical:20}}>
          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>HEIGHT</Label>
              <Text>
                {_.get(this.props.er.pmr.pmrInfo, 'height', '') ? _.get(this.props.er.pmr.pmrInfo, 'height', '') + ' cm' : "NO DATA"}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>WEIGHT</Label>
              <Text>
                {_.get(this.props.er.pmr.pmrInfo, 'weight', '') ? _.get(this.props.er.pmr.pmrInfo, 'weight', '') + ' kg' :  "NO DATA"}
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>B.M.I</Label>
              <Text>
                {_.get(this.props.er.pmr.pmrInfo, 'height', '') && _.get(this.props.er.pmr.pmrInfo, 'weight', '')
                  ?(Number(this.props.er.pmr.activeRecord.weight) / Math.pow((Number(this.props.er.pmr.activeRecord.height) / 100), 2)).toFixed(2) : "NO DATA"
                }
              </Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>B.S.A</Label>
              <Text>
                {_.get(this.props.er.pmr.pmrInfo, 'height', '') && _.get(this.props.er.pmr.pmrInfo, 'weight', '') ?
                  (Math.sqrt((Number(this.props.er.pmr.activeRecord.height) * Number(this.props.er.pmr.activeRecord.weight)) / 3600, 2)).toFixed(2) + ' m²' : "NO DATA"
                }
              </Text>
            </View>
          </Item>
        </View>

        <ListItem style={{borderBottomWidth: 0}}>
          <Title>MEDICAL INFORMATION</Title>
        </ListItem>

        <View style={{height: 1, backgroundColor: '#e2e2e2'}}></View>

        <View style={{flexDirection: 'row', paddingVertical:20}}>
          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>CHIEF COMPLAINTS</Label>
              <Text>{this.props.er.pmr.pmrInfo.chiefComplaints || "NO DATA"}</Text>
            </View>
          </Item>

          <View style={{width: 1, backgroundColor: '#e2e2e2'}}></View>

          <Item stackedLabel style={{flex: 0.25, borderBottomWidth: 0}}>
            <View>
              <Label>CLINICAL IMPRESSIONS</Label>
              <Text>{this.props.er.pmr.pmrInfo.clinicalImpressions || "NO DATA"}</Text>
            </View>
          </Item>
        </View>
      </View>
    )
  }
}

function mapStateToProps(state){
  return {
    er: state.er
  }
}

function mapDispatchToProps(dispatch) {
  return {
    erActions: bindActionCreators(erActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PMRInfo);
