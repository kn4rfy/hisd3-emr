import React, { Component } from 'react';
import { Dimensions, View, Alert, Switch } from 'react-native';
import { Container, Header, Content, Body, Title, Form, Input, Item, ListItem, Button, Label, Text, Left, Right, Radio, Icon } from 'native-base';
import moment from 'moment';
import _ from 'lodash';

import {connect} from 'react-redux';
import {Actions, Scene} from 'react-native-router-flux'
import {bindActionCreators} from 'redux';

import * as authActions from '../actions/authActions';
import * as healthchecks from '../actions/healthchecks';
import * as patientActions from '../actions/patientactions';
import * as pdscActions from '../actions/pdscactions';

import DateTimePicker from 'react-native-modal-datetime-picker';

import * as style from '../styles/styles';
import * as HISstyle from '../styles/HISstyle';

import TriageModal from './general/TriageModal';

const {width, height, scale} = Dimensions.get("window"),
vw = width / 100,
vh = height / 100,
vmin = Math.min(vw, vh),
vmax = Math.max(vw, vh);

class NewCase extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTriageModal: true,
      activeTriage: -1
    }
  }

  componentWillMount(){
    this.props.patientActions.loadPatientActiveSuccess('clear');
    this.props.pdscActions.updatePDSCInput('clear');
  }

  updatePatientField=(id)=>{
    return(data)=>{
      var payload = {};
      payload[id] = data;
      this.props.patientActions.updatePatientActive(payload);
    }
  }

  updatePatientFieldWithVal = (name, value) => {
    var payload = {};
    payload[name] = value;
    this.props.patientActions.updatePatientActive(payload);
  }

  showDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: true});
    }
  }

  hideDatePicker=(value)=>{
    return()=>{
      this.setState({[value]: false});
    }
  }

  setDateTime=(date)=>{
    var payload = {};
    payload['dob'] = moment(date).format("YYYY-MM-DD");
    this.props.patientActions.updatePatientActive(payload);

    let filter = `${this.props.patients.activePatient.lastname}, ${this.props.patients.activePatient.firstname} ${this.props.patients.activePatient.middlename}`;
    let filterDob = payload['dob'];
    this.props.patientActions.getPatientInfoByFilter(1, filter.toUpperCase(), filterDob, 9999);
    this.setState({dob: false});
  }

  onPressSaveConfirm=()=>{
    if(_.get(this.props.patients.activePatient, 'lastname') && _.get(this.props.patients.activePatient, 'firstname') && _.get(this.props.patients.activePatient, 'middlename') && _.get(this.props.patients.activePatient, 'dob') &&
    _.get(this.props.patients.activePatient, 'gender') && _.get(this.props.patients.activePatient, 'home_address') && _.get(this.props.patients.activePatient, 'patientType')){
      Alert.alert(
        'Confirm',
        'Do you really want to complete the input and save?',
        [{
          text: 'Yes',
          onPress: () => {this.onPressSave()},
        },
        {
          text: 'No',
          onPress: () => {},
        }]
      );

    } else {
      Alert.alert(
        'Error',
        'Some fields are empty! Please fill it up to continue.',
        [{
          text: 'OK',
          onPress: () => {},
        }]
      );
    }
  }

  onPressSave=()=>{
    if (this.props.patients.activePatient.dob == null) {
      var date = moment().format("YYYY-MM-DD");
      this.setDateTime(date);
    }

    if(this.props.patients.activePatient.id == null){
      this.props.patientActions.addPatient(this.props.patients.activePatient, (result, message)=>{
        if(result){
          Alert.alert(
            'Success',
            message,
            [{
              text: 'OK',
              onPress: () => {Actions.pdsc({activeTriage: this.state.activeTriage})},
            }]
          );
        }
        else {
          Alert.alert(
            'Error',
            message,
            [{
              text: 'OK',
              onPress: () => {},
            }]
          );
        }
      });
    }else{
      Actions.pdsc({activeTriage: this.state.activeTriage});
    }
  }

  openTriageModal = () => {
    this.setState({
      showTriageModal:true
    })
  }

  closeTriageModal = (act) => {
    this.setState({
      showTriageModal:false
    }, () => {
      if(act == 'backToPatientList'){
        Actions.patients()
      }
    })
  }

  triageChange = (level) => {
    return()=>{
      this.setState({
        activeTriage: level
      })
    }
  }

  saveSelectedTriage = () => {
    this.closeTriageModal();
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={()=>Actions.pop()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>New case</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Form>
            <Item stackedLabel>
              <Label>Last name</Label>
              <Input
                onChangeText={this.updatePatientField('lastname')}
                value={this.props.patients.activePatient.lastname || ""}
                />
            </Item>

            <Item stackedLabel>
              <Label>First name</Label>
              <Input
                onChangeText={this.updatePatientField('firstname')}
                value={this.props.patients.activePatient.firstname || ""}
                />
            </Item>

            <Item stackedLabel>
              <Label>Middle name</Label>
              <Input
                onChangeText={this.updatePatientField('middlename')}
                value={this.props.patients.activePatient.middlename || ""}
                />
            </Item>

            <Item stackedLabel onPress={this.showDatePicker('dob')}>
              <Label>Date of birth</Label>
              <Input
                value={_.get(this.props.patients.activePatient, 'dob') ? String(moment(this.props.patients.activePatient.dob).format('LL')) : ""}
                editable={false}
                />
            </Item>

            <Item style={{flexDirection: 'row'}}>
              <Label>Gender</Label>
              <ListItem style={{flex: 0.5, borderBottomWidth: 0}}
                selected={this.props.patients.activePatient.gender == 'MALE' ? true:false}
                onPress={()=>this.updatePatientFieldWithVal('gender', this.props.patients.activePatient.gender == 'MALE' ? 'FEMALE' : 'MALE')}>
                <Text>MALE</Text>
                <Right>
                  <Switch
                    onValueChange={()=>this.updatePatientFieldWithVal('gender', this.props.patients.activePatient.gender == 'MALE' ? 'FEMALE' : 'MALE')}
                    value={this.props.patients.activePatient.gender == 'MALE' ? true:false}
                    />
                </Right>
              </ListItem>

              <ListItem style={{flex: 0.5, borderBottomWidth: 0}}
                selected={this.props.patients.activePatient.gender == 'FEMALE' ? true:false}
                onPress={()=>this.updatePatientFieldWithVal('gender', this.props.patients.activePatient.gender == 'FEMALE' ? 'MALE' : 'FEMALE')}>
                <Text>FEMALE</Text>
                <Right>
                  <Switch
                    onValueChange={()=>this.updatePatientFieldWithVal('gender', this.props.patients.activePatient.gender == 'FEMALE' ? 'MALE' : 'FEMALE')}
                    value={this.props.patients.activePatient.gender == 'FEMALE' ? true:false}
                    />
                </Right>
              </ListItem>
            </Item>

            <Item>
              <Label>Home address</Label>
              <Input
                onChangeText={this.updatePatientField('home_address')}
                value={this.props.patients.activePatient.home_address || ""}
                />
            </Item>

            {
              this.props.patients.activePatient.id?(
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ flex: 0.5, flexDirection: 'row' }}>
                    <Text>{this.props.patients.activePatient.patientType}</Text>
                  </View>
                  <View style={{ flex: 0.5, flexDirection: 'row' }}>
                    <Text>{this.props.patients.activePatient.memberId?this.props.patients.activePatient.memberId:(this.props.patients.activePatient.pin?this.props.patients.activePatient.pin:null)}</Text>
                  </View>
                </View>
              )
              :(
                <Item style={{flexDirection: 'row'}}>
                  <Label>Patient type</Label>
                  <ListItem style={{flex: 0.5, borderBottomWidth: 0}}
                    selected={this.props.patients.activePatient.patientType == 'ACE-MC INVESTOR' ? true:false}
                    onPress={()=>this.updatePatientFieldWithVal('patientType', this.props.patients.activePatient.patientType == 'ACE-MC INVESTOR' ? 'REGULAR PATIENT' : 'ACE-MC INVESTOR')}>
                    <Text>ACE-MC INVESTOR</Text>
                    <Right>
                      <Switch
                        onValueChange={()=>this.updatePatientFieldWithVal('patientType', this.props.patients.activePatient.patientType == 'ACE-MC INVESTOR' ? 'REGULAR PATIENT' : 'ACE-MC INVESTOR')}
                        value={this.props.patients.activePatient.patientType == 'ACE-MC INVESTOR' ? true:false}
                        />
                    </Right>
                  </ListItem>

                  <ListItem style={{flex: 0.5, borderBottomWidth: 0}}
                    selected={this.props.patients.activePatient.patientType == 'REGULAR PATIENT' ? true:false}
                    onPress={()=>this.updatePatientFieldWithVal('patientType', this.props.patients.activePatient.patientType == 'REGULAR PATIENT' ? 'ACE-MC INVESTOR' : 'REGULAR PATIENT')}>
                    <Text>REGULAR PATIENT</Text>
                    <Right>
                      <Switch
                        onValueChange={()=>this.updatePatientFieldWithVal('patientType', this.props.patients.activePatient.patientType == 'REGULAR PATIENT' ? 'ACE-MC INVESTOR' : 'REGULAR PATIENT')}
                        value={this.props.patients.activePatient.patientType == 'REGULAR PATIENT' ? true:false}
                        />
                    </Right>
                  </ListItem>
                </Item>
              )
            }

            <Button success block onPress={this.onPressSaveConfirm}>
              <Text>{this.props.patients.activePatient.id == null?'Save':'Proceed'}</Text>
            </Button>

          </Form>

          {
            this.state.showTriageModal ? (
              <TriageModal
                showTriageModal={this.state.showTriageModal}
                openTriageModal={this.openTriageModal}
                closeTriageModal={this.closeTriageModal}
                activeTriage={this.state.activeTriage}
                triageChange={this.triageChange}
                saveSelectedTriage={this.saveSelectedTriage}
                />
            ) : null
          }

          <DateTimePicker
            isVisible={this.state.dob}
            onConfirm={(date)=>this.setDateTime(date)}
            onCancel={this.hideDatePicker('dob')}
            mode={'date'}
            maximumDate={new Date()}
            />

        </Content>
      </Container>
    );
  }
}


function mapStateToProps(state) {
  return {
    auth:state.auth,
    routes:state.routes,
    healthchecks:state.healthchecks,
    patients:state.patients
  }
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch),
    healthchecks: bindActionCreators(healthchecks, dispatch),
    patientActions: bindActionCreators(patientActions, dispatch),
    pdscActions: bindActionCreators(pdscActions, dispatch)
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(NewCase);
