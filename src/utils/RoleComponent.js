import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';
import {AsyncStorage,View,Alert,Dimensions} from 'react-native'
import * as style from '../styles/styles';
import * as authActions from '../actions/authActions';

export function isInRole(role,rolesRepo){
    return _.includes(rolesRepo || [], role);
}

export function isInAnyRole(roles,rolesRepo){
    roles = _.isArray(roles) ? roles : [];
    var found = false;
    roles.forEach(function(i){
        if(isInRole(i,rolesRepo)){
          found = true;
        }
    });

    return found;
}

export function requireAuthentication(Component, account, roles) {
  let rolesArray = [];
  if(typeof roles === 'string'){
      rolesArray.push(roles);
  }

  if(roles instanceof Array){
      rolesArray = roles;
  }

  let userRoles = account.roles;

  if (userRoles == null || userRoles.length == 0) {
    return null;
  }

  if (rolesArray.length > 0) {
      if (rolesArray.length == 1) {

          if (!isInRole(rolesArray[0], userRoles)){
            return null;
          }
          else {
            return Component;
          }
      }
      else {
          if (!isInAnyRole(rolesArray, userRoles)){
            return null
          } else {
            return Component;
          }
      }
  }
}
