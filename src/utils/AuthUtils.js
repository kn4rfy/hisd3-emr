/**
* Created by albertoclarit on 1/13/16.
*/
import React from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';
import {AsyncStorage,View,Alert,Dimensions} from 'react-native'
import * as style from '../styles/styles';
import * as authActions from '../actions/authActions';

export function isInRole(role,rolesRepo){
  return _.includes(rolesRepo || [], role);
}

export function isInAnyRole(roles,rolesRepo){

  roles = _.isArray(roles) ? roles : [];
  var found = false;

  roles.forEach(function(i){
    if(isInRole(i,rolesRepo))
    {
      found = true;
    }
  });

  return found;
}

export function requireAuthentication(Component,roles) {

  var rolesArray = [];
  if(typeof roles === 'string'){
    rolesArray.push(roles);
  }

  if(roles instanceof Array){
    rolesArray = roles;
  }

  class AuthenticatedComponent extends React.Component{
    constructor(props){
      super(props);

      this.state={
        allowed:false,
        mounted:true
      };
    }

    componentDidMount(){
      this.setState({
        mounted:true
      },()=>{

        var {dispatch} = this.props;
        AsyncStorage.getItem('login',(error,token)=>{
          if(token){
            if(!this.props.auth.account){
              // attempt to get account details based on existing session cookie and csrf
              dispatch(authActions.loginSuccess((result)=>{
                if(result)
                this.checkRoles(this.props.auth.account);
              }));
            }
            else
            this.checkRoles(this.props.auth.account);
          }
          else {
            Actions.auth();
          }
        });
      });
    }

    componentWillUnmount(){
      this.setState({
        mounted:false
      });
    }

    checkRoles=(account)=>{

      if(account){
        var roles = account.roles;

        if (roles == null || roles.length == 0) {
          Actions.auth();
          Alert.alert(
            'Error Credentials', 'User is forbidden to access this resource',
            [{
              text: 'OK',
              onPress: () => {}
            }]
          );
          return;
        }

        if (rolesArray.length > 0) {
          if (rolesArray.length == 1) {
            if (!isInRole(rolesArray[0], roles)){
              setTimeout(()=>{
                Actions.auth();
                Alert.alert(
                  'Error Credentials', 'User is forbidden to access this resource',
                  [{
                    text: 'OK',
                    onPress: () => {}
                  }]
                );
              },100);
            }
            else {
              setTimeout(()=>{
                if(this.state.mounted)
                this.setState({
                  allowed:true
                });
              },100);
            }
          }
          else {
            if (!isInAnyRole(rolesArray, roles)){
              setTimeout(()=>{
                Actions.auth();
                Alert.alert(
                  'Error Credentials', 'User is forbidden to access this resource',
                  [{
                    text: 'OK',
                    onPress: () => {}
                  }]
                );
              },100);
            } else {
              setTimeout(()=>{
                if(this.state.mounted)
                this.setState({
                  allowed:true
                });
              },100);
            }
          }
        }
      }
      else
      Actions.auth();
    };

    render(){
      var renderMe=null;
      const {width, height, scale} = Dimensions.get("window"),
      vw = width / 100,
      vh = height / 100,
      vmin = Math.min(vw, vh),
      vmax = Math.max(vw, vh);

      if(this.props.auth.account &&  this.props.auth.isAuthenticated && this.state.allowed)
      renderMe=(<Component {...this.props}/>);
      return (
        <View style={{ flex: 1, backgroundColor: style.darkBlueColor }}>
          {renderMe}
        </View>
      );
    }
  }

  const mapStateToProps = (state) => ({
    auth: state.auth
  });

  return connect(mapStateToProps)(AuthenticatedComponent);
}
