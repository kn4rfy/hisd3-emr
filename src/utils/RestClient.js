/**
 * Created by albertoclarit on 9/2/16.
 */

import axios from 'axios'
import _ from 'lodash'
import Promise from 'bluebird'
import {AsyncStorage} from 'react-native'
// see https://github.com/mzabriskie/axios

import cookie  from 'cookie';


const defaultConfig = {
    headers: {'X-Requested-With': 'XMLHttpRequest'},
    xsrfCookieName: 'CSRF-TOKEN',
    xsrfHeaderName: 'X-CSRF-TOKEN'
};





// Add a request interceptor
axios.interceptors.request.use(function (config) {

    return config;
}, function (error) {

});

// Add a response interceptor
axios.interceptors.response.use(function (response) {

    var cookieLine= response.headers['set-cookie']||'';

    var cookies = cookie.parse(cookieLine);

    if(cookieLine != ''){
      AsyncStorage.setItem('cookieCache', JSON.stringify(cookies));
    }

    return response;
}, function (error) {
    return Promise.reject(error);
});


async function getCookieCache() {
        const value = await AsyncStorage.getItem('cookieCache');
        if (value !== null){
             return JSON.parse(value);
        } else {
          return {}
        }
}


export let get  = (path,config)=>{

       if(!config)
       config = {};

     config = _.assign({},defaultConfig,config);

     let _config = config;

    return new Promise(function (resolve, reject) {

        getCookieCache().then((cookieCache)=>{

            _config.headers['JSESSIONID'] = cookieCache['JSESSIONID'] || '';

            axios.get(path,_config).then((response)=>{
                resolve(response);
            }).catch((error)=>{
                reject(error);
            });

        }).catch((error)=>{
            reject(error)
        });
    });

};

export let post  = (path,body,config)=>{

    if(!config)
        config = {};

    config = _.assign({},defaultConfig,config);
    config.headers = config.headers || {};
    let _config = config;
    return new Promise(function (resolve, reject) {

        getCookieCache().then((cookieCache)=>{

            _config.headers['JSESSIONID'] = cookieCache['JSESSIONID'] || '';
            _config.headers['X-CSRF-TOKEN'] = cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || '';

            axios.post(path,body || {}, _config).then((response)=>{
                resolve(response);
            }).catch((error)=>{
                reject(error);
            });

        }).catch((error)=>{
            reject(error)
        });

    });

};

export let put  = (path,body,config)=>{

    if(!config)
        config = {};

    config = _.assign({},defaultConfig,config);
    config.headers = config.headers || {};


    let _config = config;

    return new Promise(function (resolve, reject) {

        getCookieCache().then((cookieCache)=>{

            _config.headers['JSESSIONID'] = cookieCache['JSESSIONID'] || '';
            _config.headers['X-CSRF-TOKEN'] = cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || '';

            axios.put(path,body || {}, _config).then((response)=>{
                resolve(response);
            }).catch((error)=>{
                reject(error);
            });

        }).catch((error)=>{
            reject(error)
        });
    });


};

export let patch  = (path,body,config)=>{

    return new Promise(function (resolve, reject) {


        getCookieCache().then((cookieCache)=>{
          if(!config)
              config = {};

          config = _.assign({},defaultConfig,config);
          config.headers = config.headers || {};
          let _config = config;

            _config.headers['JSESSIONID'] = cookieCache['JSESSIONID'] || '';
            _config.headers['X-CSRF-TOKEN'] = cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || '';

            axios.patch(path,body || {}, _config).then((response)=>{
                resolve(response);
            }).catch((error)=>{
                reject(error);
            });

        }).catch((error)=>{
            reject(error)
        });

    });


};


export let _delete  = (path,config)=>{

    if(!config)
        config = {};

    config = _.assign({},defaultConfig,config);
    config.headers = config.headers || {};
    let _config = config;
    return new Promise(function (resolve, reject) {

        getCookieCache().then((cookieCache)=>{

            _config.headers['JSESSIONID'] = cookieCache['JSESSIONID'] || '';
            _config.headers['X-CSRF-TOKEN'] = cookieCache['HttpOnly, CSRF-TOKEN'] || cookieCache['CSRF-TOKEN'] || '';

            axios.delete(path || {}, _config).then((response)=>{
                resolve(response);
            }).catch((error)=>{
                reject(error);
            });

        }).catch((error)=>{
            reject(error)
        });

    });

};
