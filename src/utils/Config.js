const host = 'localhost';
const port = '8080';
const url = host + ':' + port;

export { host, port, url }
