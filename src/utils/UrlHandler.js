/**
 * Created by albertoclarit on 10/7/16.
 */
import React from 'react';

import { routerActions } from 'react-router-redux'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {getDefaultRoutesFromRoles} from '~/src/components/shared/RouteRules'

class UrlHandler extends React.Component{

    constructor(props){
        super(props);

    }


    componentDidMount(){


        var account  = this.props.auth.account;

        if(account){
            if(!account.active){
                this.props.routerActions.push("/user/forchangepassword");
            }
            else {
                var targetPath=this.props.location.pathname+this.props.location.search;
                var goto = getDefaultRoutesFromRoles(account.roles,false,targetPath);
                this.props.routerActions.push(goto);
            }
        }

    }

    render(){
    return (
        <div></div>
    )
    }
}

function mapStateToProps(state) {
    return {
        auth:state.auth
    }
}


function mapDispatchToProps(dispatch) {
    return {
        routerActions: bindActionCreators(routerActions, dispatch)
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(UrlHandler);

