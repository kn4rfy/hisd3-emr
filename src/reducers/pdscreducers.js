import * as pdscTypes from '../constants/PDSCActionTypes';
import update from 'react-addons-update';

const INITIAL_STATE = {
    pdscList: [],
    pageInfo: [],
    pdscInfo: {},
    activeRecord: {},
    activeSearch: '',
    discharge: {
        activeRecord: {}
    },
    consent: {
        activeRecord: {}
    },
    random:0
};

export default function pdscreducers(state = INITIAL_STATE, action = {}) {
    switch(action.type) {
        case pdscTypes.GET_PDSC_LIST:
            return update(state, {
                pdscList: {
                    $set: action.data._embedded.pdscs
                },
                pageInfo: {
                    $set: action.data.page
                },
                random: {
                    $set: Math.random()
                }
            });

        case pdscTypes.GET_PDSC_INFO:
            return update(state, {
                pdscInfo: {
                    $set: action.data
                }
            });

        case pdscTypes.UPDATE_PDSC_INPUT:
            if(action.data == 'clear'){
                return update(state,{
                    activeRecord:{
                        $set:{}
                    },
                    pdscInfo:{
                        $set:{}
                    }
                });
            }else{
                return update(state,{
                    activeRecord:{
                        $merge:action.data
                    }
                });
            }

        case pdscTypes.UPDATE_SEARCH_INPUT:
            return update(state,{
                activeSearch:{
                    $set:action.data
                }
            });

        case pdscTypes.UPDATE_DISCHARGE_INPUT:
            if(action.data == 'clear'){
                var newemp = update(state.discharge,{
                    activeRecord:{
                        $set:{}
                    }
                });
            }else{
                var newemp = update(state.discharge,{
                    activeRecord:{
                        $merge:action.data
                    }
                });
            }

            return update(state,{
                discharge:{
                    $set:newemp
                }
            });

        case pdscTypes.UPDATE_CONSENT_INPUT:
            if(action.data == 'clear'){
                var newemp = update(state.consent,{
                    activeRecord:{
                        $set:{}
                    }
                });
            }else{
                var newemp = update(state.consent,{
                    activeRecord:{
                        $merge:action.data
                    }
                });
            }

            return update(state,{
                consent:{
                    $set:newemp
                }
            });

        case pdscTypes.GET_PDSC_DATA:
            return update(state,{
                activeRecord:{
                    $set:action.data
                },
                pdscInfo:{
                    $set:action.data
                }
            });
        default:
            return state;
    }
}
