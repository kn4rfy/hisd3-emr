import * as Types from '../constants/ERActionTypes';
import update from 'react-addons-update';

const INITIAL_STATE = {
    triageLevel: null,
    activeTab:1,
    pmr: {
        pmrInfo: {
          allergies:{}
        },
        activeRecord: {
          allergies:{}
        }
    },
    folio: {
        folioInfo: {},
        activeRecord: {}
    },
    vitalSigns: {
        records:[],
        activeRecord: {
            records: {}
        }
    },
    pastMedicalHistories: {
        records:[],
        activeRecord: {
            records: []
        }
    },
    presentMedicalHistories: {
        records:[],
        activeRecord: {
            records: []
        }
    },
    reviewSystems:{
        records:[],
        activeRecord: {
            records: {}
        }
    },
    physicalExaminations:{
        records:[],
        activeRecord: {
            records: {}
        }
    },
    iap:{
      records:[],
      activeRecord:{},
      activePatient:{}
    },
    iapDetails:{
      records:[],
      activeRecord:{}

    },
    mpndo: {
      activeRecord: {},
      records: []
    },
    npn:{
      activeRecord:{},
      records:[]
    }
};

export default function erreducers(state = INITIAL_STATE, action = {}) {
    switch(action.type) {

        case Types.SET_TRIAGE:
            return update(state,{
                triageLevel:{
                    $set:action.data
                }
            });

        case Types.SET_ACTIVE_TAB:
            return update(state,{
                activeTab:{
                    $set:action.data
                }
            });

        case Types.PMR_ADD_SUCCESS:
            var data = update(state.pmr,{
                pmrInfo:{
                    $set:action.data
                },
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state,{
                pmr:{
                    $set:data
                }
            });

        case Types.PMR_UPDATE_SUCCESS:
            var data = update(state.pmr,{
                pmrInfo:{
                    $set:action.data
                },
                activeRecord:{
                    $set:action.data
                }
            });
            return update(state,{
                pmr:{
                    $set:data
                }
            });

        case Types.PMR_UPDATE_INPUT:
            var data = update(state.pmr,{
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state,{
                pmr:{
                    $set:data
                }
            });

        case Types.PMR_ALLERGY_UPDATE_INPUT:
            var data = update(state.pmr,{
                activeRecord:{
                    allergies: {
                        [action.id]: {
                            $set: action.data
                        }
                    }
                }
            });
            return update(state,{
                pmr:{
                    $set:data
                }
            });

        case Types.PMR_LOAD_SUCCESS:
            if(action.data == 'clear'){
                var data = update(state.pmr,{
                    pmrInfo:{
                      $set:{
                        allergies:{}
                      }
                    },
                    activeRecord:{
                      $set:{
                        allergies:{}
                      }
                    }
                });
                return update(state,{
                    pmr:{
                        $set:data
                    }
                });
            }else{
                var data = update(state.pmr,{
                    pmrInfo:{
                        $set:action.data
                    },
                    activeRecord:{
                        $merge:action.data
                    }
                });
                return update(state,{
                    pmr:{
                        $set:data
                    }
                });
            }

        case Types.FOLIO_ADD_SUCCESS:
            var data = update(state.folio,{
                folioInfo:{
                    $set:action.data
                },
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state,{
                folio:{
                    $set:data
                }
            });

        case Types.FOLIO_UPDATE_SUCCESS:
            var data = update(state.folio,{
                folioInfo:{
                    $set:action.data
                },
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state,{
                folio:{
                    $set:data
                }
            });

        case Types.FOLIO_UPDATE_INPUT:
            var data = update(state.folio,{
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state,{
                folio:{
                    $set:data
                }
            });

        case Types.FOLIO_LOAD_SUCCESS:
            if(action.data == 'clear'){
                var data = update(state.folio,{
                    folioInfo:{
                        $set:{}
                    },
                    activeRecord:{
                        $set:{}
                    }
                });
                return update(state,{
                    folio:{
                        $set:data
                    }
                });
            }else{
                var data = update(state.folio,{
                    folioInfo:{
                        $set:action.data
                    },
                    activeRecord:{
                        $merge:action.data
                    }
                });
                return update(state,{
                    folio:{
                        $set:data
                    }
                });
            }

        case Types.VITAL_SIGNS_ADD_SUCCESS:
            return update(state, {
                vitalSigns:{
                    $merge: action.data
                }
            });

        case Types.VITAL_SIGNS_UPDATE_SUCCESS:
            return update(state, {
                vitalSigns:{
                    $merge: action.data
                }
            });

        case Types.VITAL_SIGNS_UPDATE_INPUT:
            var data = update(state.vitalSigns, {
                activeRecord: {
                    records: {
                        [action.id]: {
                            $set: action.data
                        }
                    }
                }
            });
            return update(state, {
                vitalSigns:{
                    $set: data
                }
            });

        case Types.VITAL_SIGNS_LOAD_SUCCESS:
            if(action.records == 'clear'){
                var data = update(state.vitalSigns,{
                    records:{
                        $set:[]
                    },
                    activeRecord:{
                        $set:{
                            records:{}
                        }
                    }
                });
                return update(state, {
                    vitalSigns:{
                        $set: data
                    }
                });
            }else{
                var data = update(state.vitalSigns,{
                    records:{
                        $set:action.records._embedded.vitalSigns
                    },
                    activeRecord:{
                        $merge:action.activeRecord
                    }
                });
                return update(state, {
                    vitalSigns:{
                        $set: data
                    }
                });
            }

        case Types.PAST_MEDICAL_HISTORY_ADD_SUCCESS:
            return update(state, {
                pastMedicalHistories:{
                    $merge: action.data
                }
            });

        case Types.PAST_MEDICAL_HISTORY_UPDATE_SUCCESS:
            return update(state, {
                pastMedicalHistories:{
                    $merge: action.data
                }
            });

        case Types.PAST_MEDICAL_HISTORY_UPDATE_INPUT:
            var data = update(state.pastMedicalHistories,{
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state, {
                pastMedicalHistories:{
                    $set: data
                }
            });

        case Types.PAST_MEDICAL_HISTORY_LOAD_SUCCESS:
            if(action.records == 'clear'){
                var data = update(state.pastMedicalHistories,{
                    records:{
                        $set:[]
                    },
                    activeRecord:{
                        $set:{
                            records:[]
                        }
                    }
                });
                return update(state, {
                    pastMedicalHistories:{
                        $set: data
                    }
                });
            }else{
                var data = update(state.pastMedicalHistories,{
                    records:{
                        $set:action.records._embedded.pastMedicalHistories
                    },
                    activeRecord:{
                        $merge:action.activeRecord
                    }
                });
                return update(state, {
                    pastMedicalHistories:{
                        $set: data
                    }
                });
            }

        case Types.PRESENT_MEDICAL_HISTORY_ADD_SUCCESS:
            return update(state, {
                presentMedicalHistories:{
                    $merge: action.data
                }
            });

        case Types.PRESENT_MEDICAL_HISTORY_UPDATE_SUCCESS:
            return update(state, {
                presentMedicalHistories:{
                    $merge: action.data
                }
            });

        case Types.PRESENT_MEDICAL_HISTORY_UPDATE_INPUT:
            var data = update(state.presentMedicalHistories,{
                activeRecord:{
                    $merge:action.data
                }
            });
            return update(state, {
                presentMedicalHistories:{
                    $set: data
                }
            });

        case Types.PRESENT_MEDICAL_HISTORY_LOAD_SUCCESS:
            if(action.records == 'clear'){
                var data = update(state.presentMedicalHistories,{
                    records:{
                        $set:[]
                    },
                    activeRecord:{
                        $set:{
                            records:[]
                        }
                    }
                });
                return update(state, {
                    presentMedicalHistories:{
                        $set: data
                    }
                });
            }else{
                var data = update(state.presentMedicalHistories,{
                    records:{
                        $set:action.records._embedded.presentMedicalHistories
                    },
                    activeRecord:{
                        $merge:action.activeRecord
                    }
                });
                return update(state, {
                    presentMedicalHistories:{
                        $set: data
                    }
                });
            }

        case Types.REVIEW_SYSTEM_ADD_SUCCESS:
            return update(state, {
                reviewSystems:{
                    $merge: action.data
                }
            });

        case Types.REVIEW_SYSTEM_UPDATE_SUCCESS:
            return update(state, {
                reviewSystems:{
                    $merge: action.data
                }
            });

        case Types.REVIEW_SYSTEM_UPDATE_INPUT:
            var data = update(state.reviewSystems,{
                activeRecord:{
                    records:{
                        [action.id]:{
                            $set:action.data
                        }
                    }
                }
            });
            return update(state, {
                reviewSystems:{
                    $set: data
                }
            });

        case Types.REVIEW_SYSTEM_LOAD_SUCCESS:
            if(action.records == 'clear'){
                var data = update(state.reviewSystems,{
                    records:{
                        $set:[]
                    },
                    activeRecord:{
                        $set:{
                            records:{}
                        }
                    }
                });
                return update(state, {
                    reviewSystems:{
                        $set: data
                    }
                });
            }else{
                var data = update(state.reviewSystems,{
                    records:{
                        $set:action.records._embedded.reviewSystems
                    },
                    activeRecord:{
                        $merge:action.activeRecord
                    }
                });
                return update(state, {
                    reviewSystems:{
                        $set: data
                    }
                });
            }

        case Types.PHYSICAL_EXAMINATION_ADD_SUCCESS:
            return update(state, {
                physicalExaminations:{
                    $merge: action.data
                }
            });

        case Types.PHYSICAL_EXAMINATION_UPDATE_SUCCESS:
            return update(state, {
                physicalExaminations:{
                    $merge: action.data
                }
            });

        case Types.PHYSICAL_EXAMINATION_UPDATE_INPUT:
            var data = update(state.physicalExaminations,{
                activeRecord:{
                    records:{
                        [action.id]:{
                            $set:action.data
                        }
                    }
                }
            });
            return update(state, {
                physicalExaminations:{
                    $set: data
                }
            });

        case Types.PHYSICAL_EXAMINATION_LOAD_SUCCESS:
            if(action.records == 'clear'){
                var data = update(state.physicalExaminations,{
                    records:{
                        $set:[]
                    }
                    ,
                    activeRecord:{
                        $set:{
                            records:{}
                        }
                    }
                });
                return update(state, {
                    physicalExaminations:{
                        $set: data
                    }
                });
            }else{
                var data = update(state.physicalExaminations,{
                    records:{
                        $set:action.records._embedded.physicalExaminations
                    },
                    activeRecord:{
                        $merge:action.activeRecord
                    }
                });
                return update(state, {
                    physicalExaminations:{
                        $set: data
                    }
                });
            }

        case Types.IAP_UPDATE:
        {

          var newiap = update(state.iap,{
              activeRecord:{
                  $merge:action.data
              }
          });

          return update(state,{
              iap:{
                  $set:newiap
              }
          });
        }
        case Types.IAP_RESET:
        {

          var newiap = update(state.iap,{
              activeRecord:{
                  $set:{patient:action.data}
              }
          });

          return update(state,{
              iap:{
                  $set:newiap
              }
          });
        }
        case Types.IAP_LOAD:
        {
          if (action.data == 'clear') {
              var newiap = update(state.iap,{
                  activeRecord:{
                      $set:{}
                  }
              });

              return update(state,{
                  iap:{
                      $set:newiap
                  }
              });
          }else{
            var newiap = update(state.iap,{
                activeRecord:{
                    $merge:action.data
                }
            });


            return update(state,{
                iap:{
                    $set:newiap
                }
            });
          }

        }
        case Types.IAP_LOAD_PATIENT:
        {
          if (action.data == 'clear') {
              var newiap = update(state.iap,{
                  activePatient:{
                      $set:{}
                  }
              });

              return update(state,{
                  iap:{
                      $set:newiap
                  }
              });
          }else{
          var pdscID = action.data.activePdsc ? action.data.activePdsc.id : null;
            var newiap = update(state.iap,{
                activePatient:{
                    $set:action.data
                },
                activeRecord:{
                    $merge:{"pdsc":"/restapi/pdscs/"+pdscID}
                }
            });

            return update(state,{
                iap:{
                    $set:newiap
                }
            });
          }

        }
        case Types.ADD_IAP_DETAILS:
        {
            var newiapDetails = update(state.iapDetails,{
                records:{
                    $push:[action.data]
                }
            });
            return update(state,{
                iapDetails:{
                    $set:newiapDetails
                }
            });
        }
        case Types.LOAD_IAP_DETAILS:
        {

            var newiapDetails = update(state.iapDetails,{
                records:{
                    $push:action.data
                }
            });

            return update(state,{
                iapDetails:{
                    $set:newiapDetails
                }
            });
        }
        case Types.SET_IAP_DETAILS:
        {

            if (action.data == 'clear') {
              var newiapDetails = update(state.iapDetails,{
                  records:{
                      $set:[]
                  }
              });

              return update(state,{
                  iapDetails:{
                      $set:newiapDetails
                  }
              });
            }else {
              var newiapDetails = update(state.iapDetails,{
                  records:{
                      $set:action.data
                  }
              });

              return update(state,{
                  iapDetails:{
                      $set:newiapDetails
                  }
              });
            }
        }
        case Types.SAVE_IAP_DETAILS:
        {

           var oldRecords = state.iapDetails.records;
	         var newRecords = [];

           for(var i=0;i<oldRecords.length;i++){
               if(oldRecords[i].id === action.data.id)
                {
                  newRecords.push(action.data);
                }
               else
               {
                 newRecords.push(oldRecords[i]);
               }

           	}

           	return update(state.iapDetails,{
           	   records: {
           	      $set:newRecords
           	   }
           	});
        }
        case Types.UPDATE_IAP_DETAILS:
        {

          var newiapDetails = update(state.iapDetails,{
              activeRecord:{
                  $merge:action.data
              }
          });

          return update(state,{
              iapDetails:{
                  $set:newiapDetails
              }
          });
        }
        case Types.GET_ACTIVE_IAP_DETAILS:
        {

          if (action.data == 'clear') {
            var newiapDetails = update(state.iapDetails,{
                activeRecord:{
                    $set:{}
                }
            });

            return update(state,{
                iapDetails:{
                    $set:newiapDetails
                }
            });
          }else{
            var newiapDetails = update(state.iapDetails,{
                activeRecord:{
                    $merge:action.data
                }
            });

            return update(state,{
                iapDetails:{
                    $set:newiapDetails
                }
            });
          }
        }
        case Types.REMOVE_KEY_IAP_DETAILS:
        {

          var removeIapdetails = update(state.iapDetails,{
            records:{
                $splice: [[state.iapDetails.records.indexOf(action.selected), 1]]
            }
          });

          return update(state, {

              iapDetails:{
                  $merge: removeIapdetails
              }
          });
        }

        case 'UPDATE_MPNDO_INPUT':
        {
          if(action.data == 'clear'){
              var newemp = update(state.mpndo,{
                  activeRecord:{
                      $set:{}
                  }
              });
          }else{
              var newemp = update(state.mpndo,{
                  activeRecord:{
                      $merge:action.data
                  }
              });
          }

          return update(state,{
              mpndo:{
                  $set:newemp
              }
          });
        }

        case 'UPDATE_MPNDO':
        {
          if(action.data == 'clear'){
              var data = update(state.mpndo,{
                  records:{
                      $set:{}
                  }
              });
          }else{
              var data = update(state.mpndo,{
                  records:{
                      $set:action.data
                  }
              });
          }

          return update(state,{
              mpndo:{
                  $set:data
              }
          });
        }

        case 'UPDATE_NPN_INPUT':
        {
          if(action.data == 'clear'){
              var data = update(state.npn,{
                  activeRecord:{
                      $set:{}
                  }
              });
          }else{
              var data = update(state.npn,{
                  activeRecord:{
                      $merge:action.data
                  }
              });
          }

          return update(state,{
              npn:{
                  $set:data
              }
          });
        }

        case 'UPDATE_NPN':
        {
          if(action.data == 'clear'){
              var data = update(state.npn,{
                  records:{
                      $set:{}
                  }
              });
          }else{
              var data = update(state.npn,{
                  records:{
                      $set:action.data
                  }
              });
          }

          return update(state,{
              npn:{
                  $set:data
              }
          });
        }

        default:
            return state;
    }
}
