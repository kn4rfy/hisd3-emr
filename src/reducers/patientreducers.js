import * as pTypes from '../constants/PatientActionTypes';
import update from 'react-addons-update';

const INITIAL_STATE = {
    patientList: [],
    pageInfo: [],
    patientInfo: [],
    activePatient:{},
    activeSearch: '',
    activeSearchDob: '',
    random: 0,
    selectedPatient: ''
};

export default function patientreducers(state = INITIAL_STATE, action = {}) {

    switch (action.type) {
        case pTypes.GET_PATIENT_LIST:
            return update(state, {
                patientList: {
                    $set: action.data._embedded.patients
                },
                pageInfo: {
                    $set: action.data.page
                },
                random: {
                    $set: Math.random()
                }
            });
        case pTypes.GET_PATIENT_INFO_BY_ID:
            if(action.data == 'clear'){
                return update(state,{
                    patientInfo:{
                        $set:{}
                    }
                });
            }else{
                return update(state, {
                    patientInfo: {
                        $set: action.data
                    }
                });
            }
            case pTypes.GET_PATIENT_ACTIVE:
                if(action.data == 'clear'){
                    return update(state,{
                        activePatient:{
                            $set:{}
                        }
                    });
                }else{
                    return update(state, {
                        activePatient: {
                            $set: action.data
                        }
                    });
                }
        case pTypes.ADD_PATIENT:
            return update(state, {
                activePatient: {
                    $set: action.data
                }
            });

        case pTypes.UPDATE_SEARCH_INPUT:
            return update(state,{
                activeSearch:{
                    $set:action.data
                }
            });

        case pTypes.UPDATE_SEARCH_DOB:
            return update(state,{
                activeSearchDob:{
                    $set:action.data
                }
            });

        case pTypes.UPDATE_PATIENTACTIVE:
        {
          return update(state,{
              activePatient: {
                  $merge: action.data
              }
          });
        }

        case pTypes.SET_SELECTED_PATIENT_ON_VIRTUALIZE:
            return update(state, {
              selectedPatient: {
                $set: action.data
              },
              random: {
                  $set: Math.random()
              }
            })
        default:
            return state;
    }
}
