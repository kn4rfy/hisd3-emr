import { combineReducers } from 'redux';

import auth from './authreducers';
import routes from './routes';
import healthchecks from './healthchecks';
import er from './erreducers';
import patients from './patientreducers';
import pdscs from './pdscreducers';

const rootReducer = combineReducers({
  auth,
  routes,
  healthchecks,
  er,
  patients,
  pdscs
});

export default rootReducer;
